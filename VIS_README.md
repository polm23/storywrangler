# README for visualization code

For ease of reference, we will use 
`*_plot.py` to refer to scripts that provide command-line interfaces that users can run to get plots,
while `*_analytics.py` will include any analytical tools needed for a given figure 
and `*_vis.py` will have the visualization code to produce plots.  

## Languages

- [`languages_plot.py`](src/languages_plot.py): Command-line interface 
- [`languages_analytics.py`](src/languages_analytics.py) Analytical scripts
- [`languages_vis.py`](src/languages_vis.py) Visualization scripts 

```shell
usage: languages_plot.py [-h] [-r RESOLUTION] [-t TARGETS] [-o OUTDIR] [-c] [-x]
{all,summary,story,rank,compare,ratio,zipf, stats,timeline,shift,div,contagiograms,lang_usage}
...

positional arguments:
  {all,summary,story,rank,compare,ratio,zipf,stats,timeline,shift,div,contagiograms,lang_usage}
                        Arguments for specific action.
    all                 Run all available plots
    summary             Plot a summary figure of language evolution of Twitter
    story               Plot total number of languages captured by Twitter and
                        FastText along with the volume of tweets for top
                        languages
    rank                Plot top-15 ranked languages over time
    compare             Compare top-30 ranked language time series by twitter
                        and FastText
    ratio               Compare the ratio of tweets to retweets of the top-30
                        ranked languages on Twitter
    zipf                Plot zipf distribution for all supported languages by
                        twitter/FastText
    stats               Plot a table of total number of messages captured by
                        language
    timeline            Plot a timeline comparing the frequency of languages
                        on twitter
    shift               Plot language shifts comparing the normalized usage of
                        languages on twitter
    div                 Compare Twitter to FastText language labels
    contagiograms       Plot a grid of contagiograms: rate of usage timeseries
                        + contagion fractions
    lang_usage          Plot a timeseries of language usage

optional arguments:
  -c, --compressed      compressed days (default: False)
  -h, --help            show this help message and exit
  -o OUTDIR, --outdir OUTDIR
                        absolute Path to save figures (default:
                        ../storywrangler/plots/languages)
  -r RESOLUTION, --resolution RESOLUTION
                        group days based on a given timescale [i.e. 'D', 'W',
                        'M', '6M', 'Y'] (default: W)
  -t TARGETS, --targets TARGETS
                        list of languages to use (i.e. "en es ja") (default:
                        ['en', 'ja', 'es', 'pt', 'ar', 'th', 'ko', 'fr', 'id',
                        'tr', 'und', 'other', 'unknown'])
  -x, --compute         option to re-compute dataframes (default: False)
```


## $n$-grams

- [`ngrams_plot.py`](src/ngrams_plot.py): Command-line interface 
- [`ngrams_analytics.py`](src/ngrams_analytics.py) Analytical scripts
- [`ngrams_vis.py`](src/ngrams_vis.py) Visualization scripts 

```shell
usage: ngrams_plot.py [-h] [-n NGRAMS] [--date DATE] [-r RESOLUTION]
[-t TARGETS] [-o OUTDIR] [-c CACHEDIR] [-e] [-l] [--target_date TARGET_DATE]
{zipf,freqzipf,collect_stats,stats,risk,flipbook,studies,pantheon_figures,pantheon_age,grid,chart}
...

positional arguments:
  {zipf,freqzipf,collect_stats,stats,risk,flipbook,studies,pantheon_figures,pantheon_age,grid,chart}
                        Arguments for specific action.
    zipf                Plot zipf distribution for [N]-grams for a given
                        language
    freqzipf            Plot zipf distribution for [N]-grams for a given
                        language
    collect_stats       generate a dataframe of ngram statistics overtime
    stats               plot ngram statistics overtime
    risk                run risk models
    flipbook            combine PDFs into a single flipbook
    studies             plot median age of famous figures in the last century
    pantheon_figures    plot rank of famous individuals on twitter
    pantheon_age        plot KDE of rank and age of famous individuals on
                        twitter
    grid                plot a grid of ngram timeseries
    chart               Plot a grid of ngrams timeseries

optional arguments:
  --date DATE           date to insert into (default: None)
  --target_date TARGET_DATE
                        target date for comparison plot (default: None)
  -c CACHEDIR, --cachedir CACHEDIR
                        absolute Path to save figures (default:
                        ../storywrangler/datacache/ngrams)
  -e, --emoji           include emoji component (when available) (default:
                        False)
  -h, --help            show this help message and exit
  -l, --lite            run the lite version of a given function, generally
                        reducing space complexity (default: False)
  -n NGRAMS, --ngrams NGRAMS
                        n-grams scheme to use (default: 1)
  -o OUTDIR, --outdir OUTDIR
                        absolute Path to save figures (default:
                        ../storywrangler/plots/ngrams)
  -r RESOLUTION, --resolution RESOLUTION
                        group days based on a given timescale [i.e. 'D', 'W',
                        'M', '6M', 'Y'] (default: D)
  -t TARGETS, --targets TARGETS
                        list of languages to use (i.e. "en es ja") (default:
                        ['en', 'ja', 'und', 'es', 'pt', 'ar', 'th', 'ko',
                        'fr', 'id', 'tr', 'de', 'nl', 'ru'])
```


**Example:** To plot Zipf distributions for [1-3]grams: 
`python ngrams_plot.py -e freqzipf 
~/Data/twitter_lid/new_zipfs/1grams/2019-11-11.tar.gz 
~/Data/twitter_lid/new_zipfs/2grams/2019-11-11.tar.gz 
~/Data/twitter_lid/new_zipfs/3grams/2019-11-11.tar.gz`


## Contagion Ratio/Gain

- [`gain_plot.py`](src/gain_plot.py): Command-line interface 
- [`gain_analytics.py`](src/gain_analytics.py) Analytical scripts
- [`gain_vis.py`](src/gain_vis.py) Visualization scripts 


```shell
usage: gain_plot.py [-h] [-r RESOLUTION] [-s] [-o OUTDIR] [-m MODELS]
{gain_complex,ratio_complex,gain,ratio,gain_heatmap,ratio_heatmap,gaints,ratiots,pred,contagion,diff,ngrams}
...

positional arguments:
  {gain_complex,ratio_complex,gain,ratio,gain_heatmap,ratio_heatmap,gaints,ratiots,pred,contagion,diff,ngrams}
                        Arguments for specific action.
    gain_complex        Compare growth of sharing among languages
    ratio_complex       Compare growth of sharing among languages
    gain                Compare gain among languages
    ratio               Compare ratio among languages
    gain_heatmap        Compare gain among languages
    ratio_heatmap       Compare ratio among languages
    gaints              Plot a time series of gain
    ratiots             Plot a time series of ratio
    pred                Compare growth of sharing among languages
    contagion           Plot a timeline comparing comparing contagion ratios
                        among languages on twitter
    diff                Plot growth of retweet-ing by language
    ngrams              Compare growth of sharing among ngrams

optional arguments:
  -h, --help            show this help message and exit
  -m MODELS, --models MODELS
                        absolute Path to bayesian fit models (default:
                        ../storywrangler/models)
  -o OUTDIR, --outdir OUTDIR
                        absolute Path to save figures (default:
                        ../storywrangler/plots/gain)
  -r RESOLUTION, --resolution RESOLUTION
                        group days based on a given timescale [i.e. 'D', 'W',
                        'M', '6M', 'Y'] (default: W)
  -s, --sample          get a random sample of ngrams and save it to a json
                        file (default: False)
```