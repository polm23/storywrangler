"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""

import sys
import time
from pathlib import Path

import cli
import regexr
import ujson
from utils import predict


def parse_args(args, config):
    parser = cli.parser()

    # optional subparsers
    subparsers = parser.add_subparsers(help='Arguments for specific data types.', dest='dtype')
    subparsers.required = True

    text_parser = subparsers.add_parser(
        'text',
        help='run ngrams parser and fastText LID on short text'
    )
    text_parser.add_argument(
        'str',
        help='short text (ie. "What language is this?")'
    )

    doc_parser = subparsers.add_parser(
        'doc',
        help='run ngrams parser and fastText LID on .txt document'
    )
    doc_parser.add_argument(
        'path',
        help='path to a (.txt) file'
    )

    # optional args
    parser.add_argument(
        '-n', '--ngrams',
        default=1,
        type=int,
        help='n-grams scheme to use'
    )

    parser.add_argument(
        '-s', '--score',
        default=float(config['model_threshold']),
        type=float,
        help='confidence score threshold for the language identifier'
    )

    parser.add_argument(
        '-m', '--model',
        default=config['model'],
        help='absolute Path to FastText pre-trained model'
    )

    parser.add_argument(
        '-e', '--emoji',
        action='store_true',
        help='download new codes for emojis from (https://www.unicode.org/) and nre-compile regex to parse out ngrams'
    )

    return parser.parse_args(args)


def main(args=None):

    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    for p in Path(sys.argv[0]).resolve().parents:
        if str(p).endswith('tlid'):
            with open(p/'config.json', 'r') as cfg:
                config = ujson.load(cfg)
                break

    args = parse_args(args, config)

    if args.emoji:
        regexr.update_parsers(config['twitterlid'])

    eparser = regexr.get_emojis_parser(config['emoji_parser'])
    nparser = regexr.get_ngrams_parser(config['ngrams_parser'])

    if args.dtype == 'text':
        text = regexr.remove_whitespaces(args.str)
        emoji_freezone = eparser.sub(r'', regexr.filter_text(text))
        emoji_freezone = regexr.remove_whitespaces(emoji_freezone)

        print('-'*50)
        print(f'Text: "{args.str}"')
        print(f'Language: {predict(emoji_freezone, str(args.model), args.score)}')

        if args.ngrams > 0:
            print(f'N-grams: {regexr.ngrams(text, parser=nparser, n=int(args.ngrams))}')
        print('-' * 50)

    else:
        print('Error: unknown data type!')

    print(f'Total time elapsed: {time.time() - timeit:.2f} sec.')


if __name__ == "__main__":
    main()
