"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""

import sys
import time
import ujson
import logging
import pandas as pd
from pathlib import Path
from datetime import datetime

import cli
import languages_analytics


def parse_args(args, config):
    parser = cli.parser()

    parser.add_argument(
        'date',
        help='a date string (eg, 2020-01-01)'
    )

    # optional args
    parser.add_argument(
        '-o', '--outdir',
        default=Path(config['languages']),
        help='absolute Path to save daily language usage dataframes'
    )

    parser.add_argument(
        '-x', '--codes',
        default=Path(config['language_codes']),
        help='path to (.json) language hashtable'
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    for p in Path(sys.argv[0]).resolve().parents:
        if str(p).endswith('tlid'):
            with open(p/'config.json', 'r') as cfg:
                config = ujson.load(cfg)
                break

    args = parse_args(args, config)

    ngrams = Path(config['ngrams'])
    out = Path(args.outdir)
    out.mkdir(parents=True, exist_ok=True)

    try:
        date = datetime.strptime(args.date, "%Y-%m-%d")
        date = date.strftime("%Y-%m-%d")
    except ValueError:
        date = Path(args.date).stem.split('.')[0]

    logdir = Path(out/'logs')
    logdir.mkdir(parents=True, exist_ok=True)
    logfile = logdir/f'{date}.log'
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO, filename=logfile)

    logging.info('Initialized logging...')
    logging.info(args)

    langdf = languages_analytics.lang_stats(
        Path(f'{ngrams}/1grams/{date}.tar.gz'),
        Path(args.codes)
    )

    for i in range(1, 4):
        logging.info(f'Processing {i}grams...')

        langdf = languages_analytics.ngrams_stats(
            langdf,
            Path(f'{ngrams}/{i}grams/{date}.tar.gz')
        )

    langdf.loc['_all'] = langdf.sum()

    langdf.index.name = 'language'
    langdf.to_csv(out/f'{date}.csv.gz')

    logging.info(f'{out/f"{date}.csv.gz"}')
    logging.info(f'Total time elapsed: {time.time() - timeit:.2f} secs.')


if __name__ == "__main__":
    main()
