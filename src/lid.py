"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""


import logging
import shutil
import sys
import tarfile
import time
from pathlib import Path

import cli
import regexr
import ujson
from utils import select_tweets


def parse_args(args, config):
    parser = cli.parser()

    parser.add_argument(
        'datapth',
        help='path to a [day] directory of compressed 15-minutes twitter files (ie. /tmp/2019-01-01)'
    )

    # optional args
    parser.add_argument(
        '-l', '--lang',
        default="en",
        type=str,
        help='desired language for tweet selection'
    )

    parser.add_argument(
        '-m', '--model',
        default=config['model'],
        help='absolute Path to FastText pre-trained model'
    )

    parser.add_argument(
        '-o', '--outdir',
        default=Path(config['languages']),
        help='absolute Path to save network predictions'
    )

    parser.add_argument(
        '-e', '--emoji',
        action='store_true',
        help='download new codes for emojis from (https://www.unicode.org/) and re-compile regex to parse out ngrams'
    )

    parser.add_argument(
        '--score',
        default=float(config['model_threshold']),
        type=float,
        help='confidence score threshold for the language identifier'
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    for p in Path(sys.argv[0]).resolve().parents:
        if str(p).endswith('tlid'):
            with open(p/'config.json', 'r') as cfg:
                config = ujson.load(cfg)
                break

    args = parse_args(args, config)

    Path(config["logs"]).mkdir(parents=True, exist_ok=True)
    logfile = f'{config["logs"]}/{args.lang}-{Path(args.datapth).stem}.log'
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO, filename=logfile)
    print = logging.info
    logging.info('Initialized logging...')
    logging.info(args)

    if args.emoji:
        codes = regexr.download_emojis()
        regexr.update_parsers(codes, config['twitterlid'])

    eparser = regexr.get_emojis_parser(config['emoji_parser'])
    indir = Path(args.datapth)
    outdir = Path(args.outdir)
    outdir.mkdir(parents=True, exist_ok=True)

    done = select_tweets(
        indir,
        str(args.model),
        eparser,
        outdir,
        args.score,
        args.lang
    )

    if done:
        print('Compressing files... ')
        with tarfile.open(f'{outdir}.tar.gz', "w:gz") as tar:
            tar.add(outdir, arcname=outdir.stem)

        print('Deleting temporary files... ')
        shutil.rmtree(outdir, ignore_errors=True)

    print(f'Total time elapsed: {time.time() - timeit:.2f} sec. ~ {args.outdir}')


if __name__ == "__main__":
    main()
