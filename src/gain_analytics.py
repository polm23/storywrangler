"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""

import consts
import fileio
import gain_vis

import joblib
import gzip
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.stats import skewnorm


def sample(ngrams, savepath, samplesize=1.0):
    """ Draw a random sample of gain values from a list of CSV files
    :param ngrams: a directory of aggregated ngrams
    :param savepath: path to save generated sample
    :param samplesize: sample size from the ngrams dataset
    """
    grams = {}
    for f in sorted(ngrams.glob('*.tsv.gz')):
        lang, year = f.stem.split('.')[0].split('_')

        if grams.get(lang) is None:
            grams[lang] = {}

        if grams[lang].get(year) is None:
            grams[lang][year] = {'gain': [], 'count': []}

        with gzip.open(f, 'rb') as file:
            header = file.readline().decode('utf-8').rstrip().split('\t')
            for row in tqdm(file, desc=f"Sampling [{lang}-{year}]", unit='ngram'):
                row = dict(zip(header, row.decode('utf-8').strip().split('\t')))
                if np.float_(row['freq_no_rt']) > 0 and np.random.random() * np.float_(row['freq']) < samplesize:
                    g = 10 * np.log10(np.float_(row['count']) / np.float_(row['count_no_rt']))
                    grams[lang][year]['count'].append(np.float_(row['count']))
                    grams[lang][year]['gain'].append(g)

    fileio.save_json(grams, savepath)


def lang_ratio(langs, savepath, hashtbl, resolution='Y'):
    """ Plot time series of language usage on Twitter
    :param langs: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    :param resolution: a time-scale to aggregate on (i.e. 'H', 'D', 'W', 'M', '6M', 'Y')
    """
    lang_ratio = langs.copy()
    lang_ratio['ft_organic'] = lang_ratio.ft_tweets + lang_ratio.ft_comments
    lang_ratio['ratio'] = lang_ratio['ft_retweets'] / lang_ratio['ft_organic']
    lang_ratio = lang_ratio.loc[:, ['language', 'ratio']]
    lang_ratio = lang_ratio.pivot(columns='language', values='ratio')
    lang_ratio = lang_ratio.resample(resolution).mean()
    lang_ratio.index = lang_ratio.index.strftime('%Y')

    heatmap = lang_ratio.loc[:, consts.langsOfInterest]
    heatmap = heatmap.replace([np.inf, -1. * np.inf, np.nan], 0)

    lang_ratio.rename(
        columns={code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )

    heatmap.rename(
        columns={code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )

    langts = langs[
        ['ft_count', 'ft_speakers', 'ft_tweets', 'ft_retweets', 'ft_comments']
    ].groupby(['date']).sum()
    langts = langts.resample('Y').mean()

    langts['ft_organic'] = langts.ft_tweets + langts.ft_comments
    langts['ft_ratio'] = langts['ft_retweets'] / langts['ft_organic']

    counts = langs.groupby(['date', 'language']).sum()
    counts['ft_organic'] = counts.ft_tweets + counts.ft_comments
    counts['ft_ratio'] = counts['ft_retweets'] / counts['ft_organic']

    gain_vis.plot_lang_ratio(
        f'{savepath}/lang_ratio',
        counts,
        lang_ratio,
        heatmap.T,
        langts,
    )
    print(f'Saved: {savepath}/lang_ratio')


def lang_gain(langs, savepath, hashtbl, models, resolution='Y'):
    """ Plot time series of language usage on Twitter
    :param langs: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    :param models: bayesian-fit models for gain and count
    :param resolution: a time-scale to aggregate on (i.e. 'H', 'D', 'W', 'M', '6M', 'Y')
    """
    lang_gain = langs.copy()
    lang_gain['organic'] = lang_gain.ft_tweets + lang_gain.ft_comments
    lang_gain['gain'] = lang_gain['ft_count'] / lang_gain['organic']
    lang_gain = lang_gain.loc[:, ['language', 'gain']]
    lang_gain = lang_gain.pivot(columns='language', values='gain')
    lang_gain = lang_gain.resample(resolution).mean()
    lang_gain.index = lang_gain.index.strftime('%Y')
    lang_gain = lang_gain.apply(lambda x: 10 * np.log10(x))
    heatmap = lang_gain.loc[:, consts.langsOfInterest]
    heatmap = heatmap.replace([np.inf, -1. * np.inf, np.nan], 0)


    lang_gain.rename(
        columns={code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )

    heatmap.rename(
        columns={code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )

    langts = langs[
        ['ft_count', 'ft_speakers', 'ft_tweets', 'ft_retweets', 'ft_comments']
    ].groupby(['date']).sum()
    langts = langts.resample('Y').mean()

    langts['ft_organic'] = langts.ft_tweets + langts.ft_comments
    langts['ft_gain'] = langts['ft_count'] / langts['ft_organic']
    langts['ft_gain'] = langts['ft_gain'].apply(lambda x: 10 * np.log10(x))

    counts = langs.groupby(['date', 'language']).sum()
    counts['ft_organic'] = counts.ft_tweets + counts.ft_comments
    counts['ft_gain'] = (counts.ft_count / counts.ft_organic).apply(lambda x: 10 * np.log10(x))
    counts['ft_gain'].replace([np.inf, -1.*np.inf, np.nan], 1., inplace=True)

    gain_vis.plot_lang_gain(
        f'{savepath}/lang_gain',
        counts,
        lang_gain,
        heatmap.T,
        langts,
        models
    )
    print(f'Saved: {savepath}/lang_gain')


def ngrams_gain(ngrams, timeseries, savepath):
    """ Plot time series of language usage on Twitter
    :param ngrams: a sample of gain values per year and language stored in json files
    :param timeseries: a directory of ngram timeseries (ie. [the.tsv.gz ... test.tsv.gz])'
    :param savepath: path to save generated plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    :param resolution: a time-scale to aggregate on (i.e. 'H', 'D', 'W', 'M', '6M', 'Y')
    """
    grams = fileio.load_json(ngrams)

    tseries = {}
    for f in sorted(timeseries.glob('*.tsv.gz')):
        name = f.stem.split('_')[0]
        tseries[name] = pd.read_csv(
            f,
            header=0,
            sep='\t',
            na_filter=False,
            compression='gzip',
            index_col=0,
        )
        c = 'freq'
        tseries[name]['gain'] = tseries[name][c] / tseries[name][f"{c}_noRT"]
        tseries[name]['gain'] = tseries[name]['gain'].apply(lambda x: 10 * np.log10(x))

        tseries[name].index = pd.to_datetime(tseries[name].index)

    gain_vis.plot_ngrams_gain(
        f'{savepath}/ngrams_gain',
        grams,
        tseries,
    )
    print(f'Saved: {savepath}/ngrams_gain')


def contagion_timeline(df, savepath, hashtbl, resolution='M', var='ratio'):
    """ Plot a timeline comparing contagion ratios among languages on twitter
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param resolution: a time-scale to aggregate on (i.e. 'W', 'M', '6M', 'Y')
    """
    langs = {code: f'{label} ({code})' for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])}
    df.language.replace(langs, inplace=True)

    clangs = df.loc[:, ['language', 'ft_count']]
    clangs = clangs.pivot(columns='language', values='ft_count')
    clangs = clangs.loc[:, clangs.any()]
    clangs = clangs.resample(resolution).sum()

    df['organic'] = df['ft_tweets'] + df['ft_comments']
    df['gain'] = df['ft_count'] / df['organic']
    df['ratio'] = df['ft_retweets'] / df['organic']

    if var == 'gain':
        cc = df.loc[:, ['language', 'gain']]
        cc = cc.pivot(columns='language', values='gain')
        cc = cc.loc[:, clangs.columns]
        cc = cc.apply(lambda x: 10 * np.log10(x))
        cc = cc.resample(resolution).mean()
    else:
        cc = df.loc[:, ['language', 'ratio']]
        cc = cc.pivot(columns='language', values='ratio')
        cc = cc.loc[:, clangs.columns]
        cc = cc.resample(resolution).mean()

    gain_vis.plot_contagion_timeline(
        f'{savepath}/{var}_timeseries.html',
        clangs,
        cc
    )

    print(f'Saved: {savepath}/{var}_timeseries.html')


def diff(df, savepath, hashtbl, resolution):
    """ Plot a timeline comparing the frequency of languages on twitter
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param resolution: a time-scale to aggregate on (i.e. 'H', 'D', 'W', 'M', '6M', 'Y')
    """
    langs = {code: f'{label} ({code})' for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])}
    df.language.replace(langs, inplace=True)

    print(resolution)

    all = df.loc[:, ['language', 'ft_count']]
    all = all.pivot(columns='language', values='ft_count')
    all = all.loc[:, all.any()]
    all = all.resample(resolution).sum()

    tweets = df.loc[:, ['language', 'ft_tweets']]
    tweets = tweets.pivot(columns='language', values='ft_tweets')
    tweets = tweets.loc[:, tweets.any()]
    tweets = tweets.resample(resolution).sum()

    comments = df.loc[:, ['language', 'ft_comments']]
    comments = comments.pivot(columns='language', values='ft_comments')
    comments = comments.loc[:, comments.any()]
    comments = comments.resample(resolution).sum()

    retweets = df.loc[:, ['language', 'ft_retweets']]
    retweets = retweets.pivot(columns='language', values='ft_retweets')
    retweets = retweets.loc[:, retweets.any()]
    retweets = retweets.resample(resolution).sum()
    retweets = retweets.div(all)

    organic = tweets + comments
    organic = organic.div(all)

    gain_vis.plot_diff(
        f'{savepath}/retweets_timeseries.html',
        organic,
        retweets
    )
    print(f'Saved: {savepath}/retweets_timeseries.html')


def gen_hskd_bayes_model(pred_params, n=250, n_gen_datapts=178, noise=False):
    # pred_params should be of shape (N, 1, D) since we are looking one year ahead
    inds = np.random.choice(pred_params.shape[0], size=n, replace=True)

    preds = np.empty((n, n_gen_datapts))
    log_counts = np.empty((n, n_gen_datapts))

    for i, ind in enumerate(inds):
        # first, generate the pretend language counts
        # this is mean field i.e. *not* particle-based --- no language2language correspondence
        log_count = skewnorm(
            pred_params[ind, 0, 1], pred_params[ind, 0, 0], 1. / np.sqrt(pred_params[ind, 0, 2])
        ).rvs(size=n_gen_datapts)
        log_count = np.sort(log_count)  # so that quantiles come out nicely later

        # now we will generate mean ratios using the linear model
        mu = np.dot(
            np.column_stack([np.ones_like(log_count), log_count]),
            pred_params[ind, 0, 3:5]  # the two beta values
        )
        # now noise this prediction
        if noise:
            pred_noise = np.random.laplace(
                loc=0.,
                scale=pred_params[ind, 0, -1],
                size=n_gen_datapts
            )
            pred = mu + pred_noise
            preds[i] = pred
        else:
            preds[i] = mu
        log_counts[i] = log_count

    return preds, log_counts


def gain_predictions(langs, savepath, hashtbl, models, log=False):
    """ Plot bayes models of language usage on Twitter
    :param langs: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    :param models: bayesian-fit models for gain and count
    """
    langs.language.replace(
        {code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )

    counts = langs.groupby(['date', 'language']).sum()
    counts['ft_organic'] = counts.ft_tweets + counts.ft_comments
    counts['ft_gain'] = counts['ft_retweets'] / counts['ft_organic']

    if log:
        counts['ft_gain'] = counts['ft_gain'].apply(lambda x: np.log10(x))
        counts['ft_gain'].replace([np.inf, -1. * np.inf, np.nan], 0, inplace=True)

    # now time to predict in the last timestep
    # what will happenin 2020 WHO REALLY KNOWS
    # to find out load the data
    pred_params = joblib.load(models/'bayesian-logcount-ratio-timeseries-preds.gz')
    pred_vals, gen_lc_years = gen_hskd_bayes_model(
        pred_params,
        n=1000,
        n_gen_datapts=178,
        noise=True
    )
    pred_regs, gen_lc_years = gen_hskd_bayes_model(
        pred_params,
        n=1000,
        n_gen_datapts=178,
        noise=False
    )

    gain_vis.plot_prediction(
        f'{savepath}/predictions',
        counts,
        models,
        pred_vals,
        pred_regs,
        gen_lc_years,
        pred_params
    )
    print(f'Saved: {savepath}/predictions')


def ratio_timeseries(df, savepath, resolution='M'):
    """ Plot time series of language usage on Twitter
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    """
    langts = df[
        ['ft_count', 'ft_speakers', 'ft_tweets', 'ft_retweets', 'ft_comments']
    ].groupby(['date']).sum()
    langts = langts.resample(resolution).mean()

    langts['ft_organic'] = langts.ft_tweets + langts.ft_comments
    langts['ft_ratio'] = langts['ft_retweets'] / langts['ft_organic']

    gain_vis.plot_ratio_timeseries(
        f'{savepath}/ratio_ts',
        langts
    )
    print(f'Saved: {savepath}/ratio')


def gain_timeseries(df, savepath, resolution='M'):
    """ Plot time series of language usage on Twitter
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    """
    langts = df[
        ['ft_count', 'ft_speakers', 'ft_tweets', 'ft_retweets', 'ft_comments']
    ].groupby(['date']).sum()
    langts = langts.resample(resolution).mean()

    langts['ft_organic'] = langts.ft_tweets + langts.ft_comments
    langts['ft_gain'] = langts['ft_count'] / langts['ft_organic']
    langts['ft_gain'] = langts['ft_gain'].apply(lambda x: 10 * np.log10(x))
    langts['ft_gain'] = langts['ft_gain'].replace([np.inf, -1. * np.inf, np.nan], 0)

    gain_vis.plot_gain_timeseries(
        f'{savepath}/gain_ts',
        langts
    )
    print(f'Saved: {savepath}/gain')


def ratio(df, savepath, hashtbl):
    """ Plot ratio of languages over time as a function of number of messages
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    """
    '''df.language.replace(
        {code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )'''

    counts = df.groupby(['date', 'language']).sum()
    counts['ft_organic'] = counts.ft_tweets + counts.ft_comments
    counts['ft_ratio'] = counts['ft_retweets'] / counts['ft_organic']

    gain_vis.plot_ratio(
        f'{savepath}/ratios',
        counts
    )
    print(f'Saved: {savepath}/ratios')


def gain(df, savepath, hashtbl):
    """ Plot gain of languages over time as a function of number of messages
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    """
    '''df.language.replace(
        {code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )'''

    counts = df.groupby(['date', 'language']).sum()
    counts['ft_organic'] = counts.ft_tweets + counts.ft_comments
    counts['ft_gain'] = (counts.ft_count / counts.ft_organic).apply(lambda x: 10 * np.log10(x))
    counts['ft_gain'].replace([np.inf, -1. * np.inf, np.nan], 1., inplace=True)

    gain_vis.plot_gain(
        f'{savepath}/gains',
        counts
    )
    print(f'Saved: {savepath}/gains')


def gain_heatmap(df, savepath, hashtbl):
    """ Plot gain of languages over time as a function of number of messages
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    """
    lang_gain = df.loc['2009':'2019']
    lang_gain['organic'] = lang_gain.ft_tweets + lang_gain.ft_comments
    lang_gain['gain'] = lang_gain['ft_count'] / lang_gain['organic']
    lang_gain = lang_gain.loc[:, ['language', 'gain']]
    lang_gain = lang_gain.pivot(columns='language', values='gain')
    lang_gain = lang_gain.resample('3M').mean()
    lang_gain.index = lang_gain.index.strftime('%b %Y')
    lang_gain = lang_gain.apply(lambda x: 10 * np.log10(x))
    heatmap = lang_gain.loc[:, consts.langsOfInterest]
    heatmap = heatmap.replace([np.inf, -1. * np.inf, np.nan], 0)

    heatmap.rename(
        columns={code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )
    heatmap = heatmap.T

    gain_vis.plot_gain_heatmap(
        f'{savepath}/gain_heatmap',
        heatmap
    )
    print(f'Saved: {savepath}/gain_heatmap')


def ratio_heatmap(df, savepath, hashtbl):
    """ Plot ratio of languages over time as a function of number of messages
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    """
    lang_ratio = df.loc['2009':'2019']
    lang_ratio['organic'] = lang_ratio.ft_tweets + lang_ratio.ft_comments
    lang_ratio['ratio'] = lang_ratio['ft_retweets'] / lang_ratio['organic']
    lang_ratio = lang_ratio.loc[:, ['language', 'ratio']]
    lang_ratio = lang_ratio.pivot(columns='language', values='ratio')
    lang_ratio = lang_ratio.resample('Y').mean()
    lang_ratio.index = lang_ratio.index.strftime('%Y')
    heatmap = lang_ratio.loc[:, consts.langsOfInterest]
    heatmap = heatmap.replace([np.inf, -1. * np.inf, np.nan], 0)

    heatmap.rename(
        columns={code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )
    heatmap = heatmap.T

    gain_vis.plot_ratio_heatmap(
        f'{savepath}/ratio_heatmap',
        heatmap
    )
    print(f'Saved: {savepath}/ratio_heatmap')
