"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""


class Storyon(dict):
    def __init__(self, count=0, count_no_rt=0):
        super().__init__()
        self.update(dict(count=count, count_no_rt=count_no_rt))

    def __repr__(self):
        return f'Storyon(count={self["count"]}, count_no_rt={self["count_no_rt"]})'

    def __gt__(self, other):
        if type(other) == int:
            other = Storyon(0, 0)

        return self["count"] > other["count"]

    def __lt__(self, other):
        if type(other) == int:
            other = Storyon(0, 0)

        return self["count"] < other["count"]

    def __eq__(self, other):
        if type(other) == int:
            other = Storyon(0, 0)

        return self["count"] == other["count"]

    def __add__(self, other):
        if type(other) == int:
            other = Storyon(0, 0)

        self["count"] += other["count"]
        self["count_no_rt"] += other["count_no_rt"]
        return self

    def __iadd__(self, other):
        if type(other) == int:
            other = Storyon(0, 0)

        self["count"] += other["count"]
        self["count_no_rt"] += other["count_no_rt"]
        return self

    def extract(self):
        return [self["count"], self["count_no_rt"]]


class NgramCounter(dict):
    def __init__(self, d):
        super(NgramCounter, self).__init__()
        d = {k: Storyon(cc["count"], cc["count_no_rt"]) for k, cc in d.items()}
        self.update(d)

    def __missing__(self, key):
        return Storyon(0, 0)

    def __add__(self, other):
        for k, s in other.items():
            self[k] += s
        return self

    def __iadd__(self, other):
        for k, s in other.items():
            self[k] += s
        return self
