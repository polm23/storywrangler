"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""

import re
import os
from datetime import datetime
from pathlib import Path

import consts
import ngrams_vis
import numpy as np
import pandas as pd
from ngrams_query import Query
import ujson
from PyPDF2 import PdfFileReader, PdfFileMerger
import regexr
import tweet_utils


def flipbook(savepath, datapath):
    """ Combine PDFs into a flipBook
    :param savepath: path to save generated pdf
    :param files: a list of files to combine
    """
    pdf = PdfFileMerger()

    for f in datapath.rglob('*.pdf'):
        print(f)
        pdf.append(PdfFileReader(str(f), 'rb'))

    pdf.write(f"{savepath}/flipbook_{datapath.stem}.pdf")
    pdf.close()

    print(f"Saved: {savepath}/flipbook_{datapath.stem}.pdf")


def collect_stats(datapath, savepath):
    """ Generate summary dataframe for ngrams
    :param datapath: path to ngrams directories
    :param savepath: path to save summary file
    """
    data = {}
    for day in datapath.rglob(f'*.tar.gz'):
        date = day.stem.split('.')[0]
        if data.get(date) is None:
            data[date] = {}

        try:
            s = tweet_utils.read_tarball(
                day,
                filename=f'summary_fastText',
                open_func=ujson.load,
            )
        except Exception:
            continue

        try:
            data[date]['num_tweets'] = s['num_tweets']
            data[date]['num_retweets'] = s['num_retweets']
            data[date]['num_comments'] = s['num_comments']
            data[date]['num_corrupted'] = s['num_corrupted']
            data[date]['num_activities'] = s['num_activities']
        except KeyError:
            pass

        for n in range(1, 4):
            try:
                data[date][f'num_{n}grams'] = s[f'num_{n}grams']
                data[date][f'num_{n}grams_no_rt'] = s[f'num_{n}grams_no_rt']
                data[date][f'unique_{n}grams'] = s[f'unique_{n}grams']
            except KeyError:
                pass

    df = pd.DataFrame.from_dict(data, orient='index')
    df.index = pd.to_datetime(df.index)
    df.to_csv(f'{savepath}/ngrams.csv')
    print(f'Saved: {savepath}/ngrams.csv')


def stats(datapath, savepath):
    """ Plot general ngram statistics overtime
    :param datapath: path to daily language usage on Twitter
    :param savepath: path to save generated plot
    """
    df = pd.read_csv(datapath, header=0, index_col=0)
    df.index = pd.to_datetime(df.index)
    df = df.drop(consts.corrupted_days)
    for n in consts.ngrams:
        df[f'num_{n}_rt'] = df[f'num_{n}'] - df[f'num_{n}_no_rt']

    for n in consts.ngrams:
        print(df.filter(
            items=[
                f'num_{n}',
                f'num_{n}_no_rt',
                f'num_{n}_rt',
                f'unique_{n}'
            ]
        ).describe())
        print('-'*75)

    ngrams_vis.plot_stats_timeseries(f'{savepath}/stats_timeseries', df)
    print(f'Saved: {savepath}/stats_timeseries')

    ngrams_vis.plot_stats_dists(f'{savepath}/stats_dists', df)
    print(f'Saved: {savepath}/stats_dists')


def aggregate_ngrams(paths):
    """ Combine a list of dataframes
    :param paths: a list of paths to csv file(s)
    :return: a dataframe
    """
    cols = ['ngram', 'count']
    df = pd.read_csv(paths[0], sep='\t', header=0, usecols=cols, memory_map=True)
    df['date'] = datetime.strptime(Path(paths[0]).stem.split('.')[0], '%Y-%m-%d')

    for p in paths[1:]:
        d = pd.read_csv(p, sep='\t', header=0, usecols=cols, memory_map=True)
        d['date'] = datetime.strptime(Path(p).stem.split('.')[0], '%Y-%m-%d')
        df = pd.concat([df, d], sort=False, ignore_index=True)

    return df.set_index('date')


def zipf(path, date, savepath, eparser, lang='en'):
    """ Plot a zipf distribution of [N]-grams for a desired language
    :param path: path to a ngrams directory
    :param date: desired date to parse out
    :param savepath: path to save plot
    :param lang: desired language
    """
    def hashtags_parser(s):
        return bool(re.search(r'\B(\#\w+\b)', s))

    def handles_parser(s):
        return bool(re.search(r'\B(\@\w+\b)', s))

    def emojis_parser(s):
        return bool(eparser.search(s))

    dfs = {
        '1grams': {},
        '2grams': {},
        '3grams': {},
    }

    sample_size = .01

    for n in dfs.keys():
        print(f"Processing {n}: ")

        df = tweet_utils.read_tarball(
            Path(f'{path}/{n}/{date}.tar.gz'),
            filename=f'{lang}_',
            kwargs=dict(
                header=0,
                memory_map=True,
                sep='\t',
                compression='gzip',
                encoding='utf-8',
            )
        )
        inds = sorted(np.random.choice(
            df.index.values,
            p=df.freq.values,
            size=int(sample_size * df.shape[0]),
        ))
        df = df.iloc[inds, :]

        print('Parsing hashtags...')
        hashtags = df[df['ngram'].astype(str).apply(hashtags_parser)]

        print('Parsing handles...')
        handles = df[df['ngram'].astype(str).apply(handles_parser)]

        print('Parsing emojis...')
        emojis = df[df['ngram'].astype(str).apply(emojis_parser)]

        dfs[n] = {
            'All': df,
            'Hashtags': hashtags,
            'Handles': handles,
            'Emojis': emojis
        }

    print('Plotting...')
    ngrams_vis.plot_zipf(
        f'{savepath}/{lang}_{path.stem.split(".")[0]}_zipf',
        dfs
    )

    print(f'Saved: {savepath}/{lang}_{path.stem.split(".")[0]}_zipf')


def load_freq_distribution(paths, languages=consts.reachedtop10):
    """
    Helper function to load frequency distribution files
    :param paths:
    :return:
    """

    print(f'The Langs {languages}')

    return tweet_utils.read_tarball_multi(paths, languages, {'sep': '\t'})


def get_counts(df, all='count', no_rt='count_no_rt'):
    """
    Get an array of just counts
    :param df: dataframe
    :param all: column name for all
    :param no_rt: column name for count_no_rt
    :return:
    """

    return df[all].values, df[no_rt].values


def get_zipf_dist(df, n_interp=None, x_min=1, x_max=None, y_min=1, y_max=None):
    """
    Get the Zipf distribution for counts of organic and all tweets
    :param df: daily Zipf DataFrame
    :type df: pd.DataFrame
    :param n_interp: optional number of points to linearly interpolate
    :type n_interp: None or int
    :return: tuple of all and no_rt Zipf
    :rtype:
    """

    counts_all, counts_no_rt = get_counts(df)

    counts_rt = np.array(sorted(counts_all - counts_no_rt, reverse=True))
    counts_no_rt = np.array(sorted(counts_no_rt, reverse=True))

    rt_zipf = (np.log10(np.arange(counts_rt.shape[0])), np.log10(counts_rt / counts_rt.sum()))
    no_rt_zipf = (np.log10(np.arange(counts_no_rt.shape[0])), np.log10(counts_no_rt / counts_no_rt.sum()))
    combo = (no_rt_zipf, rt_zipf)

    if n_interp:  # if linearly interpolate values to save space
        new_combo = []
        for (x, y) in combo:
            x, y = x[x_min:x_max], y[y_min:y_max]

            interp_x = np.linspace(x.min(), x.max(), n_interp)
            interp_y = np.interp(interp_x, x, y)
            new_combo.append((interp_x, interp_y))

        return new_combo

    else:
        return combo


def load_daily_zipfs(paths, lang_list=['tsv'], stream_function=get_counts, sf_args={}):
    """
    Load daily zipf distributions to a dict of filename2zipf distribution
    :param paths: list of paths to load from
    :param lang_list: list of languages of interest
    :param stream_function: function to use within tarball stream
    :param sf_args: arguments for stream function
    :return:
    """

    return_dict = {}

    print(f'Supported langs {lang_list}')

    for path in paths:
        print(f'Working on {path}.')
        temp_df = tweet_utils.read_tarball_multi(
            path,
            lang_list,
            {'compression': 'gzip',
             'encoding': 'utf-8',
             'sep': '\t',
             'na_filter': False
             },
            open_func=pd.read_csv,
            stream_func=stream_function,
            sfargs=sf_args,
        )

        print('temp_df shape', len(temp_df))

        return_dict.update({path: temp_df})

    return return_dict


def freq_to_zipf(paths, savepath, cachedir, languages, supported_languages, emojis=False, tarballs=True,
                 target_date=None):
    """
    take frequency distribution files and generate zipf distribution plots
    :param paths: path to frequency distribution tarball
    :param savepath: path to save location
    :param languages:  languages of interest
    :param tarballs:  True if reading tarballs, False if Zipf tsv
    :param supported_languages: list of languages to plot
    :param emojis: True if making CCDF for just emojis (will cache for future use)
    :return:
    """

    if isinstance(paths, list) and tarballs:
        languages = ['id', 'ko', 'en', 'tr', 'und']
        emojis_df = None
        # run with emojis
        if emojis:

            tweet_utils.make_save_locs(cachedir, 'emoji_zipfs')
            emoji_paths = []
            for i, p in enumerate(paths):  # generate emoji zipfs
                to_check = f'{cachedir}/emoji_zipfs/{i + 1}grams/{p.split("/")[-1]}'
                print(f'Checking {to_check} for {i + 1}grams.')

                # check if we already have emoji Zipf cached
                if not os.path.isfile(to_check):
                    saver = Path(to_check.rsplit('/', 1)[0])  # /f'{i+1}grams'
                    tweet_utils.make_save_locs(saver, '')
                    print(f'Saving to: {saver}')

                    regexr.tarball_to_emoji_tarball(
                        p,
                        saver,
                        i + 1,
                        files_contain=languages,
                        verbose=True
                    )

                emoji_paths.append(to_check)

            emojis_df = load_daily_zipfs(emoji_paths, lang_list=languages)

        main_files = load_daily_zipfs(paths, lang_list=languages)

        ngrams_vis.plot_zipf_grid(main_files, savepath, languages, supported_languages, emojis_df)

    elif tarballs:

        filename2freqdist = load_freq_distribution(paths)

        print(filename2freqdist.keys())

        ngrams_vis.plot_freq_to_zipf_grid(filename2freqdist, savepath, languages, supported_languages)

    elif not tarballs and isinstance(paths, list):

        freq_dists = {}

        for file in paths:
            this_freq = pd.read_csv(
                file,
                compression='gzip',
                encoding='utf-8',
                sep='\t',
                na_filter=False
            )

            freq_dists.update({file: this_freq})

            ngrams_vis.plot_zipf_grid(freq_dists, savepath, languages, supported_languages)


def freq_to_zipf_lite(paths, savepath, cachedir, languages, supported_languages, emojis=False, tarballs=True,
                      target_date=None):
    """
    take frequency distribution files and generate zipf distribution plots

    !! modified to linearly interpolate the CCDF and send to modified plotting script !!

    :param paths: path to frequency distribution tarball
    :param savepath: path to save location
    :param languages:  languages of interest
    :param tarball😊s:  True if reading tarballs, False if Zipf tsv
    :param supported_languages: list of languages to plot
    :param emojis: True if making CCDF for just emojis (will cache for future use)
    :return:
    """

    if isinstance(paths, list) and tarballs:
        languages = ['ko', 'en', 'de', 'und']  # ['id', 'ko', 'en', 'tr', 'und']
        emojis_df, other_dates = None, None
        # run with emojis
        if emojis:

            tweet_utils.make_save_locs(cachedir, 'emoji_zipfs')
            emoji_paths = []
            for i, p in enumerate(paths):  # generate emoji zipfs
                to_check = f'{cachedir}/emoji_zipfs/{i + 1}grams/{p.split("/")[-1]}'
                print(f'Checking {to_check} for {i + 1}grams.')

                # check if we already have emoji Zipf cached
                if not os.path.isfile(to_check):
                    saver = Path(to_check.rsplit('/', 1)[0])  # /f'{i+1}grams'
                    tweet_utils.make_save_locs(saver, '')
                    print(f'Saving to: {saver}')

                    regexr.tarball_to_emoji_tarball(
                        p,
                        saver,
                        i + 1,
                        files_contain=languages,
                        verbose=True
                    )

                emoji_paths.append(to_check)

            emojis_df = load_daily_zipfs(emoji_paths, lang_list=languages, stream_function=get_zipf_dist,
                                         sf_args={'n_interp': 10000})

        main_files = load_daily_zipfs(paths, lang_list=languages, stream_function=get_zipf_dist,
                                      sf_args={'n_interp': 10000})
        if target_date:
            other_dates = load_daily_zipfs(Path(paths[0]).expanduser().parent.rglob(f'*{target_date}*'),
                                           lang_list=['en'], stream_function=get_zipf_dist,
                                           sf_args={'n_interp': 10000})

        print(len(main_files))

        ngrams_vis.plot_zipf_assorted_lite(main_files, savepath, languages, supported_languages, emojis_df,
                                           other_dates=other_dates)

    elif tarballs:

        filename2freqdist = load_freq_distribution(paths)

        print(filename2freqdist.keys())

        ngrams_vis.plot_freq_to_zipf_grid(filename2freqdist, savepath, languages, supported_languages)

    elif not tarballs and isinstance(paths, list):

        freq_dists = {}

        for file in paths:
            this_freq = pd.read_csv(
                file,
                compression='gzip',
                encoding='utf-8',
                sep='\t',
                na_filter=False
            )

            freq_dists.update({file: this_freq})

            ngrams_vis.plot_zipf_grid(freq_dists, savepath, languages, supported_languages)


def ngrams_grid(
    savepath,
    lang_hashtbl,
    case_sensitive=True,
    start_date=datetime(2010, 1, 1)
):
    """ Plot a grid of time series
    :param savepath: path to save generated plot
    :param lang_hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    :param case_sensitive: a toggle for case_sensitive lookups
    :param start_date: starting date for the query
    """
    words = [
        ('😊', 'en'), ('trump', 'en'), ('#metoo', 'en'),
        ('#newyear', 'en'), ('2015', 'en'), ('i\'m', 'en'),
        ('virus', 'en'), ('@bts_twt', 'en'), ('thrones', 'en'),
        ('black hole', 'en'), ('Stephen Hawking', 'en'), ('avengers', 'en'),
        ('katrina', 'en'), ('9/11', 'en'), ('#lasvegasshooting', 'en'),
    ]

    ngrams = []
    for i, (w, lang) in enumerate(words):
        n = len(w.split())
        print(f"Retrieving {lang_hashtbl.get(lang)}: {n}gram -- '{w}'")

        q = Query(f'{n}grams', lang)

        if case_sensitive:
            d = q.query_timeseries(w, start_time=start_date)
        else:
            d = q.query_insensitive_timeseries(w, start_time=start_date)

        d.index.name = f"{lang_hashtbl.get(lang)}\n'{w}'"
        ngrams.append(d)

    ngrams_vis.plot_ngrams_grid(
        f'{savepath}/ngrams_grid',
        ngrams,
    )
    print(f'Saved: {savepath}/ngrams_grid')


def chart(
        savepath,
        lang_hashtbl,
        nparser,
        case_sensitive=True,
        start_date=datetime(2010, 1, 1),
):
    """ Plot a grid of ngrams timeseries
    :param savepath: path to save generated plot
    :param lang_hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    :param nparser: compiled ngram parser
    :param case_sensitive: a toggle for case_sensitive lookups
    :param start_date: starting date for the query
    """

    targets = dict(
        Years=[
            ('2012', '_all', 'C0'),
            ('2014', '_all', 'C1'),
            ('2016', '_all', 'C2'),
            ('2018', '_all', 'C3'),
        ],
        Periodic=[
            ('Christmas', 'en', 'C0'),
            ('Pasqua', 'it', 'C1'),
            ('eleição', 'pt', 'C2'),
            ('sommar', 'sv', 'C3'),
        ],
        Sports=[
            ('Olympics', 'en', 'C0'),
            ('World Cup', 'en', 'C1'),
            ('Super Bowl', 'en', 'C3'),
        ],
        Science=[
            ('Higgs', 'en', 'C0'),
            ('#AlphaGo', 'en', 'C1'),
            ('gravitational waves', 'en', 'C2'),
            ('CRISPR', 'en', 'C5'),
            ('black hole', 'en', 'C3'),
        ],
        Fame=[
            ('Cristiano Ronaldo', 'pt', 'C0'),
            ('Donald Trump', 'en', 'C1'),
            ('Papa Francesco', 'it', 'C3'),
        ],
        Outbreaks=[
            ('cholera', 'en', 'C0'),
            ('Ebola', 'en', 'C1'),
            ('Zika', 'en', 'C2'),
            ('coronavirus', 'en', 'C3'),
        ],
        Conflicts=[
            ('غزة', 'ar', 'C0'),
            ('Libye', 'fr', 'C1'),
            ('Suriye', 'tr', 'C2'),
            ('Росія', 'uk', 'C3'),
        ],
        Movements=[
            ('ثورة', 'ar', 'C0'),
            ('Occupy', 'en', 'C1'),
            ('Black Lives Matter', 'en', 'C3'),
            ('Brexit', 'en', 'C2'),
            ('#MeToo', 'en', 'C5'),
        ],
    )

    df = None
    for topic, ngrams in targets.items():
        for i, (w, lang, color) in enumerate(ngrams):
            n = len(regexr.ngrams(w, parser=nparser, n=1))
            print(f"Retrieving {lang_hashtbl.get(lang)}: {n}gram -- '{w}'")

            q = Query(f'{n}grams', lang)
            if case_sensitive:
                d = q.query_timeseries(w, start_time=start_date)
            else:
                d = q.query_insensitive_timeseries(w, start_time=start_date)

            print(f"Top rank: {d['rank'].min()} -- {d['rank'].idxmin().date()}")

            d = d.dropna(how='all')
            ts = pd.DataFrame(dict(
                ngram=w,
                topic=topic,
                date=d.index,
                val=d['rank'].fillna(10**6),
                color=color,
                lang=lang if lang_hashtbl.get(lang) is not None else 'All',
            ))

            if df is not None:
                df = df.append(ts)
            else:
                df = ts

    ngrams_vis.plot_chart(
        f'{savepath}/chart',
        df,
        shading=False
    )
    print(f'Saved: {savepath}/chart')
