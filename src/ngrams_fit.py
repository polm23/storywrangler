
from pathlib import Path
import pymc3 as pm
import cli
from pyro.infer import MCMC, NUTS
import sys
import ngrams_analytics
import tweet_utils
import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt

import pyro
import pyro.distributions as dist


def parse_args(args, config):
    description = 'Fit models to ngram data'
    parser = parser = cli.parser()

    parser.add_argument(
        'datapath',
        help='Input directory',
        type=Path
    )
    parser.add_argument(
        '-o', '--outputdir',
        required=True,
        type=Path,
        help='Where to save'
    )

    return parser.parse_args(args)


def fit_2_region_reg_pymc(filename, summ_stat_dir, plot_dir, this_date, language='en'):
    """
    Fit a two region regression to Zipf distribution using PyMC3
    :param filename:
    :type filename:
    :param outpath:
    :type outpath:
    :return:
    :rtype:
    """

    this_df = tweet_utils.read_tarball(filename, language, {'compression': 'gzip',
                                                      'encoding': 'utf-8',
                                                      'sep': '\t',
                                                      'na_filter': False
                                                      }
                                 )

    (x, y), _ = ngrams_analytics.get_zipf_dist(this_df, n_interp=100, x_min=30, x_max=10 ** 5, y_min=30,
                                               y_max=10 ** 5)  # just grab no_rt Zipfs
    (x_all, y_all), _ = ngrams_analytics.get_zipf_dist(this_df, n_interp=1000)

    plt.plot(x_all, y_all, marker='.')
    plt.plot(x, y)
    plt.savefig(str(plot_dir / this_date) + '.pdf')

    trace = pymc3_reg_model(x, y)

    summ_stats = get_summary_stats_pymc(trace)

    # if outpath:
    summ_stats.to_csv(str(summ_stat_dir / this_date) + '.txt', sep=' ', index=False)

    (t, f0, f1) = get_split_coef_pymc(trace)

    plot_fit(x_all, y_all, x, t, f0, f1, plot_dir / this_date)

    return t, f0, f1


def fit_2_region_reg_pyro(filename, summ_stat_dir, plot_dir, this_date, language='en'):
    """
    Fit a two region regression to Zipf distribution using PyMC3
    :param filename:
    :type filename:
    :param outpath:
    :type outpath:
    :return:
    :rtype:
    """

    this_df = tweet_utils.read_tarball(filename, language, {'compression': 'gzip',
                                                      'encoding': 'utf-8',
                                                      'sep': '\t',
                                                      'na_filter': False
                                                      }
                                 )

    (x, y), _ = ngrams_analytics.get_zipf_dist(this_df, n_interp=100, x_min=30, x_max=10 ** 5, y_min=30,
                                               y_max=10 ** 5)  # just grab no_rt Zipfs
    (x_all, y_all), _ = ngrams_analytics.get_zipf_dist(this_df, n_interp=1000)

    plt.plot(x_all, y_all, marker='.')
    plt.plot(x, y)
    plt.savefig(str(plot_dir / this_date) + '.pdf')

    data = torch.tensor([x, y], dtype=torch.float)
    x_data, y_data = data[0], data[1]
    x_data = np.reshape(x_data, (-1, 1))
    y_data = np.reshape(y_data, (-1, 1))

    model = pyro_model

    nuts_kernel = NUTS(model)
    # run MCMC
    mcmc = MCMC(nuts_kernel, num_samples=1000, warmup_steps=200)
    mcmc.run(x_data, y_data)

    # retrieve samples
    hmc_samples = {k: v.detach().cpu().numpy() for k, v in mcmc.get_samples().items()}
    hmc_df = this_df = pd.DataFrame(hmc_samples)

    summ_stats = get_summary_stats_pyro(hmc_df)

    summ_stats.to_csv(str(summ_stat_dir / this_date) + '.txt', sep=' ', index=False)

    (t, f0, f1) = get_split_coef_pyro(hmc_samples)

    plot_fit(x_all, y_all, x, t, f0, f1, plot_dir / this_date)

    return t, f0, f1


def pyro_model(x_m, y_m):
    """
    define a regression model in Pyro
    :param x_m:
    :type x_m:
    :param y_m:
    :type y_m:
    :return:
    :rtype:
    """

    a0 = pyro.sample("a0", dist.Normal(0., 1.))
    b_x0 = pyro.sample('b_X0', dist.Normal(0., 1.))
    sigma0 = pyro.sample('sigma0', dist.Normal(0., 1.))

    a1 = pyro.sample("a1", dist.Normal(0., 1.))
    b_x1 = pyro.sample('b_X1', dist.Normal(0., 1.))
    sigma1 = pyro.sample('sigma1', dist.Normal(0., 1.))

    tau = int(pyro.sample('tau', dist.Uniform(1, len(x_m))))

    ind = torch.range(1, 99).long()

    with pyro.plate('data0', 100, subsample=ind[:tau]):
        y_pred0 = a0 + b_x0 * x_m[ind[:tau]]
        pyro.sample('obs0', dist.Normal(y_pred0, sigma0), obs=y_m[ind[:tau]])
    with pyro.plate('data1', 100, subsample=ind[tau + 1:]):
        y_pred1 = a1 + b_x1 * x_m[ind[tau + 1:]]
        pyro.sample('obs1', dist.Normal(y_pred1, sigma1), obs=y_m[ind[tau + 1:]])


def get_summary_stats_pyro(hmc_df):
    """

    :param hmc_df:
    :type hmc_df:
    :return:
    :rtype:
    """

    pyro_summs = hmc_df.describe([.1, .25, .5, .75, .9])
    final_df = pd.DataFrame(np.concatenate([pyro_summs.values, [np.median(pyro_summs, axis=0)]]))
    final_df.columns = pyro_summs.columns
    final_df['metric'] = list(pyro_summs.index.values) + ['median']
    final_df.set_index('metric', inplace=True)

    return final_df


def plot_fit(x_all, y_all, x, t, f0, f1, outdir=None):
    """
    plot the fit with two regions
    :param x:
    :type x:
    :param y:
    :type y:
    :param t:
    :type t:
    :param f0:
    :type f0:
    :param f1:
    :type f1:
    :param outdir: path with date but no extension
    :return:
    :rtype:
    """

    # print('yargs',locals())

    t = int(t)

    plt.plot(x_all, y_all, color='grey')

    x1, y1 = np.array(x[:t - 1]), np.array(f0(x[:t - 1])) + .1
    x2, y2 = np.array(x[t + 1:]), np.array(f1(x[t + 1:])) + .1
    plt.plot(x1, y1, alpha=.5, linestyle='-', color='r')
    plt.plot(x2, y2, alpha=.5, linestyle='-', color='r')

    plt.text(np.median(x1), np.median(y1), f'{f0[1]:.2f}x+{f0[0]:.2f}')
    plt.text(np.median(x2), np.median(y2), f'{f1[1]:.2f}x+{f1[0]:.2f}')
    plt.xlabel('Rank (log$_{10}$)')
    plt.ylabel('Normalized Frequency (log$_{10}$)')

    if outdir:
        plt.savefig(str(outdir) + '.pdf')
        plt.savefig(str(outdir) + '.png', dpi=300)


def get_summary_stats_pymc(trace):
    """
    Get summary stats from PyMC3 trace object
    :param trace:
    :type trace:
    :return:
    :rtype: list
    """
    save_vals = []
    for tv in trace.varnames:
        temp_df = pd.DataFrame(trace[tv]).describe(percentiles=[.1, .25, .75, .9])
        new_list = temp_df.reset_index().values.tolist()
        new_list.append(['median', np.median])
        final_df = pd.DataFrame(new_list)
        final_df.set_index(0, inplace=True)
        final_df.columns = [tv]
        save_vals.append(pd.DataFrame(new_list))

    return pd.concat(save_vals)


def get_split_coef_pymc(trace):
    '''return coef for two regions

    '''
    tau = np.median(trace['tau'])

    # coef = np.polyfit(x,y,1)
    f0 = np.poly1d([np.mean(trace['Intercept_0']), np.mean(trace['x_0'])])
    f1 = np.poly1d([np.mean(trace['Intercept_1']), np.mean(trace['x_1'])])
    return tau, f0, f1


def get_split_coef_pyro(hmc_samples):
    """

    :param hmc_samples:
    :type hmc_samples:
    :return:
    :rtype:
    """
    f0 = np.poly1d([np.median(hmc_samples['b_X0']), np.median(hmc_samples['a0'])])
    f1 = np.poly1d([np.median(hmc_samples['b_X1']), np.median(hmc_samples['a1'])])
    tau = int(np.median(hmc_samples['tau']))
    return tau, f0, f1


def pymc3_reg_model(x, y):
    """

    :param x:
    :type x:
    :param y:
    :type y:
    :return:
    :rtype:
    """

    with pm.Model() as model:
        sigma_0 = pm.HalfCauchy('sigma_0', beta=10, testval=1.)
        intercept_0 = pm.Normal('Intercept_0', 0, sigma=20)
        x_coeff_0 = pm.Normal('x_0', 0, sigma=20)

        sigma_1 = pm.HalfCauchy('sigma_1', beta=10, testval=1.)
        intercept_1 = pm.Normal('Intercept_1', 0, sigma=20)
        x_coeff_1 = pm.Normal('x_1', 0, sigma=20)

        tau = pm.DiscreteUniform('tau', lower=0, upper=x.shape[0] - 1)

        idx = np.arange(x.shape[0])
        sigma = pm.math.switch(tau > idx, sigma_0, sigma_1)
        intercept = pm.math.switch(tau > idx, x_coeff_0, x_coeff_1)
        x_coeff = pm.math.switch(tau > idx, intercept_0, intercept_1)

        # lambda_ = pm.math.switch()

        likelihood = pm.Normal('y', mu=intercept + (x_coeff * x),
                               sigma=sigma, observed=y)

        trace = pm.sample(4000, cores=12, chains=4)

    return trace


def write_out(fname, list_):
    with open(fname, 'w') as f:
        for item in list_:
            f.write(str(item))
    return


def main(args=None):
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args, {})

    plot_dir = args.outputdir / 'zipf_regs'
    summ_dir = args.outputdir / 'summ_stats'
    coef_dir = args.outputdir / 'coefs'

    this_date = tweet_utils.regex_file_dates(args.datapath)

    for dir in (plot_dir, summ_dir, coef_dir):
        dir.mkdir(parents=True, exist_ok=True)

    t, f0, f1 = fit_2_region_reg_pyro(args.datapath, summ_dir, plot_dir, this_date)

    write_out((coef_dir / this_date).with_suffix('.txt'), (t, f0, f1))


if __name__ == "__main__":
    main()
