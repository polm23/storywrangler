
import sys
if sys.platform == 'darwin':
    import matplotlib
    matplotlib.use("TkAgg")

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

import seaborn as sns
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from mpl_toolkits.axes_grid1.colorbar import colorbar
import matplotlib.colors as mcolors
import matplotlib.colorbar as colorbar
import matplotlib.dates as mdates
import numpy as np

import consts

try:
    import fastText
except ImportError:
    import fasttext


def plot_confmat(ncm, targets, codes, savepath):
    """ Plot a confmat comparing twitter to fasttext lang. labels
    :param ncm:  normalized confusion matrix
    :param targets: np.array of all listed languages
    :param codes: np.array of all listed language iso-codes
    :param savepath: path to save confusion matrix plot
    :return: saves figure to {savepath}
    """
    plt.rcParams.update({
        'axes.titlesize': 18,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'font.family': 'serif',
    })

    fig, ax = plt.subplots(figsize=(8, 8))
    cmap = plt.cm.magma_r
    cmap.set_under('white')
    im = ax.imshow(ncm, cmap=cmap)
    cax = make_axes_locatable(ax).append_axes('top', size='4%', pad='2%')
    cbar = colorbar(im, cax=cax, ticks=np.arange(0, 1.1, .1), orientation='horizontal')
    cax.xaxis.set_ticks_position('top')
    cax.set_xlabel('Agreement', labelpad=10)
    cax.xaxis.set_label_position('top')
    im.set_clim(.01, 1)

    ax.set_xticks(range(len(targets)))
    ax.set_yticks(range(len(targets)))
    ax.set_xticklabels(targets, rotation=45, ha='right', rotation_mode='anchor')
    ax.set_yticklabels(targets, rotation=45)
    ax.set_ylabel('Predicted language (Twitter LID)')
    ax.set_xlabel('Predicted language (FastText LID)', labelpad=5)

    upper_threshold = .7
    lower_threshold = .01
    for i in range(len(codes)):
        for j in range(len(codes)):
            if ncm[i, j] > lower_threshold and i != j:
                ax.text(j, i, round(ncm[i, j], 2), va="center", ha="center", color="black")

        ax.text(i, i, round(ncm[i, i], 2), ha="center", va="center", color="white")

    plt.savefig(savepath+'.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath+'.pdf', bbox_inches='tight', pad_inches=.25)


def compare(y1, y2, languages, targets, hashtbl, savepath):
    plt.rcParams.update({
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'font.family': 'serif',
    })

    subplotlabel = (-.175, 1.01)
    callout_label_size = 22

    languages = np.array(languages, dtype=object)
    targets = np.array(targets, dtype=object)
    codes = np.array(targets, dtype=object)
    mask = np.outer(np.in1d(languages, targets), np.in1d(languages, targets).T)

    def calc(files):
        for i, lang in enumerate(languages[np.where(np.in1d(languages, targets))[0]]):
            try:
                targets[i] = hashtbl[hashtbl[:, 1] == lang, 0][0]
                codes[i] = lang
            except IndexError:
                targets[i] = hashtbl[hashtbl[:, 2] == lang, 0][0]
                codes[i] = lang

        mat = np.zeros((len(languages), len(languages)), dtype=float)
        for m in files:
            m = np.load(m).astype(float)
            mat += m

        total = np.sum(mat)
        # normalize matrix
        mat = mat / (1 + mat.sum(axis=1)[:, np.newaxis])
        mat = mat[mask].reshape((len(targets), len(targets)))
        return mat, total

    def plot(ax, cm):
        im = ax.imshow(cm, cmap=cmap)
        lower_threshold = .01
        for i in range(len(codes)):
            for j in range(len(codes)):
                if cm[i, j] > lower_threshold and i != j:
                    ax.text(j, i, round(cm[i, j], 2), va="center", ha="center", color="black", fontsize=10)

            ax.text(i, i, round(cm[i, i], 2), ha="center", va="center", color="white", fontsize=10)

        return im

    fig = plt.figure(figsize=(14, 7))
    gs = fig.add_gridspec(ncols=2, nrows=1)

    ncm1, total1 = calc(y1)
    ncm2, total2 = calc(y2)

    ax1 = fig.add_subplot(gs[:, 0])
    ax2 = fig.add_subplot(gs[:, 1], sharey=ax1)
    cbarax = inset_axes(
        ax2,
        width="5%",
        height="112%",
        bbox_to_anchor=(0.15, 0, 1, 1),
        bbox_transform=ax2.transAxes,
        borderpad=.15,
    )

    cmap = plt.cm.magma_r
    cmap.set_under('white')

    ax1.set_title(f"{y1[0].stem.split('-')[0]} ~{int(round(total1 / 10**9, 0))} Billion Messages")
    ax2.set_title(f"{y2[0].stem.split('-')[0]} ~{int(round(total2 / 10**9, 0))} Billion Messages")

    ax1.set_xticks(range(len(targets)))
    ax2.set_xticks(range(len(targets)))
    ax1.set_yticks(range(len(targets)))

    ax1.set_xticklabels(targets, rotation=45, ha='right', rotation_mode='anchor')
    ax2.set_xticklabels(targets, rotation=45, ha='right', rotation_mode='anchor')
    ax1.set_yticklabels(targets)

    ax1.set_ylabel('Predicted language (Twitter LID)')
    ax1.set_xlabel('Predicted language (FastText LID)', labelpad=5)
    ax2.set_xlabel('Predicted language (FastText LID)', labelpad=5)

    im = plot(ax1, ncm1)
    im.set_clim(.01, 1)

    im = plot(ax2, ncm2)
    im.set_clim(.01, 1)

    cbar = colorbar(im, cax=cbarax, ticks=np.arange(0, 1.1, .1))#, orientation='horizontal')
    cbarax.yaxis.set_ticks_position('right')
    cbarax.set_xlabel('Agreement', labelpad=20)
    cbarax.xaxis.set_label_position('bottom')

    ax1.annotate(
        "A", xy=subplotlabel, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
    )
    ax2.annotate(
        "B", xy=subplotlabel, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
    )

    fig.tight_layout()
    plt.savefig(savepath+'.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath+'.pdf', bbox_inches='tight', pad_inches=.25)


def plot_mismatches(mismatches, lens, savepath):
    """ Plot LID mismatches as a function of tweet length
    :param mismatches: dataframe of predictions for the top 10 languages
    :param lens: dataframe of tweet lengths
    :param savepath: path to save confusion matrix plot
    :return: saves figure to {savepath}
    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })

    fig, ax = plt.subplots(figsize=(6, 6))
    inset = fig.add_axes([0.22, 0.2, 0.35, 0.25])

    for d in mismatches.columns:
        ax.plot(
            mismatches[d],
            color='lightgrey',
            lw=1,
            zorder=0,
        )

    ax.plot(
        mismatches.mean(axis=1),
        color='k',
        lw=1,
    )

    ax.set_xlim(0, 500)
    ax.set_ylim(10**0, 10**5)
    ax.set_yscale('log')
    ax.set_xlabel('Number of characters')
    ax.set_ylabel('Number of mismatches')
    ax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')

    for d in lens.columns:
        inset.plot(
            lens[d],
            color='lightgrey',
            lw=1,
            zorder=0,
        )
        inset.fill_between(
            lens[d].index,
            0,
            lens[d],
            facecolor='lightgrey',
            hatch='//',
            zorder=0,
        )

    inset.plot(
        lens.mean(axis=1),
        color='k',
        lw=1,
    )

    inset.set_xlim(0, 350)
    inset.set_ylim(0, 3*10**5)
    inset.set_xlabel('Number of characters', fontsize=12)
    inset.set_ylabel('Number of messages', fontsize=12)
    inset.ticklabel_format(axis='y', style='sci', useMathText=True, scilimits=(0, 3))
    inset.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')

    plt.savefig(f'{savepath}.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)


def plot_agreement(df1, df2, savepath):
    """ Compare language usage across two different years
    :param df1: a tuple of (year, dataframe of twitter/fasttext labels)
    :param df2: a tuple of (year, dataframe of twitter/fasttext labels)
    :param savepath: path to save plot
    :return: saves a figure to {savepath}
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6, 8))
    width = 0.5

    for i, (y, df) in enumerate([df1, df2]):
        stack = 0
        for level, alpha in zip(['low', 'mid', 'high'], [.3, .6, 1.]):
            bar = (df[f'ft_tweets_{level}'] + df[f'ft_comments_{level}'])/df['ft_count']
            bar = -bar if i == 0 else bar

            ax.barh(
                df.index,
                bar,
                width,
                color='C0',
                alpha=alpha,
                left=stack,
                label=f'OT ({level})' if i == 0 else None
            )
            stack += bar

        for level, alpha in zip(['low', 'mid', 'high'], [.3, .6, 1.]):
            bar = (df[f'ft_retweets_{level}'] + df[f'ft_retweets_{level}']) / df['ft_count']
            bar = -bar if i == 0 else bar

            ax.barh(
                df.index,
                bar,
                width,
                color='C1',
                alpha=alpha,
                left=stack,
                label=f'RT ({level})' if i == 0 else None
            )
            stack += bar

    ax.set_yticks(range(df1[1].shape[0]))
    ax.set_yticklabels(df1[1].index)
    ax.set_xlim((-1, 1))
    ax.set_ylim((-.5, df1[1].shape[0]-.5))
    ax.set_xticks([-1, -.8, -.6, -.4, -.2, 0, .2, .6, .4, .8, 1])
    ax.set_xticklabels([1, .8, .6, .4, .2, 0, .2, .4, .6, .8, 1])
    ax.xaxis.set_ticks_position('top')
    ax.xaxis.set_label_position('top')
    ax.spines["bottom"].set_alpha(0.0)
    ax.spines["right"].set_alpha(0.0)
    ax.spines["left"].set_alpha(0.0)
    ax.axvline(0, linestyle='-', color='dimgrey')
    ax.set_xlabel(f'{df1[0]} ~~~ Relative rate of usage ~~~ {df2[0]}', labelpad=10)
    ax.invert_yaxis()
    ax.legend(loc='lower center', frameon=False, ncol=3, bbox_to_anchor=[.5, -.11])

    plt.tight_layout()
    plt.savefig(f'{savepath}.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)


def plot_usage(ftlangs, fttypes, hashtbl, savepath):
    """ Plot language counts for twitter and fastText on diverging axes
    :param ftlangs: a dataframe of twitter/fasttext labels
    :param fttypes: a dataframe of twitter/fasttext labels
    :param savepath: path to save plot
    :return: saves a figure to {savepath}
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })

    callout_label_size = 20
    legend_font_size = 12

    fig = plt.figure(figsize=(11, 7))
    gs = fig.add_gridspec(ncols=2, nrows=4)
    tags = 'A B C'.split()
    ax = fig.add_subplot(gs[:-1, 0])
    legend = fig.add_subplot(gs[-1, 0])
    legend.set_axis_off()
    tax = fig.add_subplot(gs[:2, 1])
    long_tax = fig.add_subplot(gs[2:, 1])

    colors = [consts.colors[lang] for lang in ftlangs.columns]

    ax.stackplot(
        ftlangs.index, *ftlangs.values.T,
        labels=ftlangs.columns, colors=colors, lw=2
    )

    patchs, labels = ax.get_legend_handles_labels()
    for i, lang in zip(range(len(labels)), labels):
        if lang == 'und':
            labels[i] = 'Undefined'
        elif lang == 'other':
            labels[i] = 'Other'
        elif lang == 'unknown':
            labels[i] = 'Unknown'
        else:
            try:
                labels[i] = hashtbl[hashtbl[:, 2] == lang, 0][0]
            except IndexError:
                labels[i] = hashtbl[hashtbl[:, 1] == lang, 0][0]

    patchs = patchs[::-1]
    labels = labels[::-1]

    l1 = legend.legend(
        patchs[1:], labels[1:],
        loc='center',
        bbox_to_anchor=(.5, .75), ncol=3,
        fontsize=legend_font_size, frameon=False
    )
    legend.add_artist(l1)

    ft = fttypes[[
        'ft_short_tweets',
        'ft_short_comments',
        'ft_short_retweets',
    ]]

    tax.stackplot(
        ft.index, *ft.values.T,
        labels=ft.columns, colors=['slategrey', 'teal', 'salmon'], lw=2
    )

    ft = fttypes[[
        'ft_long_tweets',
        'ft_long_comments',
        'ft_long_retweets',
    ]]

    long_tax.stackplot(
        ft.index, *ft.values.T,
        labels=ft.columns, colors=['slategrey', 'teal', 'salmon'], lw=2,
    )

    patchs, _ = long_tax.get_legend_handles_labels()
    legend.legend(
        patchs, ['Tweets', 'Comments', 'Retweets'],
        loc='center',
        bbox_to_anchor=(.5, -.25), ncol=3,
        fontsize=legend_font_size, frameon=True
    )

    ax.set_ylim(0, 1)

    tax.set_title(r'Short messages ($\leq 140$)')
    tax.set_ylim(0, 1)

    long_tax.set_title(r'Long messages ($140 <$)')
    long_tax.set_ylim(0, .25)

    tax.annotate(
        'Long messages', xy=(.5, .9), color='k',
        xycoords="axes fraction", fontsize=14,
    )

    long_tax.annotate(
        'Introduction to\n280 characters', xy=(.05, .8), color='k',
        xycoords="axes fraction", fontsize=14,
    )

    for i, ax in enumerate([ax, tax, long_tax]):
        ax.set_ylabel(f'Relative rate of usage')
        ax.set_xlabel("")
        ax.set_xlim(ftlangs.index[0], ftlangs.index[-1])
        ax.grid(True, which="both", axis='both', zorder=0, alpha=.3, lw=1, linestyle='--')
        ax.xaxis.set_major_locator(mdates.YearLocator())
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%b\n%Y'))
        ax.xaxis.set_minor_locator(mdates.MonthLocator([4, 7, 10]))
        ax.xaxis.set_minor_formatter(mdates.DateFormatter('%b'))
        plt.setp(ax.xaxis.get_majorticklabels(), ha='center')

        if i == 0:
            ax.annotate(
                tags[i], xy=(-.15, 1.03), color='k',
                xycoords="axes fraction", fontsize=callout_label_size
            )
        else:
            ax.annotate(
                tags[i], xy=(-.15, 1.05), color='k',
                xycoords="axes fraction", fontsize=callout_label_size
            )

    plt.subplots_adjust(top=0.95, right=0.95, wspace=.3, hspace=1)
    plt.savefig(f'{savepath}.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(f'{savepath}.pdf', bbox_inches='tight', pad_inches=.25)


def plot_val_heatmap(savepath, heatmap):
    """ Plot top-n ranked languages over time
    :param savepath: path to save plot
    :param heatmap: a dataframe of languages to use for the ratio heatmap
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })

    fig, heatmapax = plt.subplots(figsize=(6, 10))

    vmin = 0
    vmax = 1.3
    step = .15
    threshold = .05
    bounds = [vmin, threshold]
    bounds.extend(np.arange(.1, vmax+step, step=step))
    cmap = plt.cm.get_cmap('magma_r')
    cmaplist = [cmap(i) for i in range(cmap.N)]
    cmaplist[0] = (1, 1, 1, 1.0)  # force the first color entry to be white
    cmap = mcolors.LinearSegmentedColormap.from_list(None, cmaplist, len(bounds))
    norm = mcolors.BoundaryNorm(bounds, len(bounds))

    cbarax = inset_axes(
        heatmapax,
        width="5%",
        height="100%",
        bbox_to_anchor=(.1, -.001, 1, 1),
        bbox_transform=heatmapax.transAxes,
        borderpad=.25,
    )

    heatmapax = sns.heatmap(
        heatmap,
        ax=heatmapax,
        cbar_ax=cbarax,
        cbar=True,
        cmap=cmap,
        norm=norm,
        vmin=vmin,
        vmax=vmax,
        linewidths=0.0001,
        linecolor='grey',
    )

    cb = colorbar.ColorbarBase(
        cbarax,
        cmap=cmap,
        norm=norm,
        ticks=bounds,
        boundaries=bounds,
        format=r'$\leq$%.2f',
        extend='min',
    )

    cbarax.tick_params(labelsize=12)
    cbarax.set_xlabel(r"$\delta = |\mathsf{R} - \mathsf{R}_{\alpha}|$", fontsize=14, ha='left', labelpad=10, x=-.2)
    cbarax.yaxis.set_label_position("right")
    cbarax.yaxis.set_ticks_position('right')

    heatmapax.set_xticklabels(heatmap.columns, ha='center')
    heatmapax.tick_params(axis='x', which='major', pad=10)
    heatmapax.set_ylabel('')
    heatmapax.grid(False, which="both")

    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)

