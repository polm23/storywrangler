"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""

import argparse
import os
import shutil
import sys
import tarfile
import time
from functools import partial
from pathlib import Path

from utils import multiprocess


def move_ngrams(ipath, opath):
    lang, fname = ipath.name.split('_')
    lang = '_all' if lang == '' else lang

    out = Path(f'{opath}/{lang}/{ipath.parent.stem}')
    out.mkdir(parents=True, exist_ok=True)
    shutil.move(ipath, f'{out}/{fname}')


def move_langs(ipath, opath):
    out = Path(f'{opath}/_all/languages/')
    out.mkdir(parents=True, exist_ok=True)
    shutil.move(ipath, f"{out}/{ipath.name.split('_')[0]}.csv.gz")


def decompress(ipath, opath):
    with tarfile.open(ipath) as tar:
        tar.extractall(f'{opath}')


def process_day(ipath, opath):
    start = time.time()

    tmp = opath / 'tmp'
    decompress(ipath, tmp)
    os.remove(ipath)
    ipath = opath / 'tmp' / ipath.name.split(".")[0]

    for d in ipath.rglob('*grams'):
        for f in d.iterdir():
            move_ngrams(f, opath)

    for f in ipath.rglob('*_languages.csv.gz'):
        move_langs(f, opath)

    shutil.rmtree(ipath, ignore_errors=True)

    print(f'{time.time() - start:.2f} secs. | {ipath.name}')


def parse_args(args):
    """ Util function to parse command-line arguments """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='A simple script to re-arrange dataset directories. \
            Copyright (c) 2019 The Computational Story Lab. Licensed under the MIT License;'
    )

    parser.add_argument('indir')

    parser.add_argument(
        '-o', '--outdir',
        default=Path.cwd()/'langdb',
        help='absolute Path to save network predictions'
    )

    parser.add_argument(
        '-j', '--jobs',
        default=1,
        type=int,
        help='the number of jobs to run in parallel. (-1) means using all processors.'
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)

    data = Path(args.indir)
    out = Path(args.outdir)

    worker = partial(process_day, opath=out)
    multiprocess(worker, data.rglob('*.tar.gz'), args.jobs)

    print(f'Total time elapsed: {time.time() - timeit:.2f} secs.')


if __name__ == "__main__":
    main()
