"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""

from sys import platform as sys_pfn
if sys_pfn == 'darwin':
    import matplotlib
    matplotlib.use("TkAgg")

import numpy as np
from scipy.spatial.distance import cdist
import matplotlib.pyplot as plt

from _plotly_future_ import v4_subplots
import plotly.offline as py
from plotly.subplots import make_subplots
import plotly.graph_objs as go
import pandas as pd
import seaborn as sns

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from matplotlib.lines import Line2D
from matplotlib.transforms import Affine2D
import matplotlib.dates as mdates
import matplotlib.colors as mcolors
import matplotlib.colorbar as colorbar
import mpl_toolkits.axisartist.floating_axes as floating_axes
from mpl_toolkits.axisartist.grid_finder import DictFormatter
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, mark_inset

import warnings
warnings.simplefilter("ignore")

import consts


def plot_zipf(df, savepath):
    """ A util function to plot zipf distribution
    :param df: a dataframe of twitter/fasttext labels
    :param savepath: path to save plot
    :return: saves a two figures to {savepath}
    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })
    figsize = (8, 8)
    plt.figure(figsize=figsize)
    ax = plt.gca()
    df.sort_values(by='tw_count', ascending=False, inplace=True)
    ax.loglog(df['tw_rank'], df['tw_count'], '--', marker='.', color='grey')
    for n in list(np.logspace(-0.5, np.log10(len(df)-1), 20).astype(int)):
        ax.annotate(
            df.index[n], (df['tw_rank'].iloc[n], df['tw_count'].iloc[n]),
            (df['tw_rank'].iloc[n]+n/10, df['tw_count'].iloc[n]+n/10),
            verticalalignment="bottom", horizontalalignment="left", fontsize=9
        )

    ax.set_xlabel("Rank ($log_{10}$)")
    ax.set_ylabel("Absolute frequency ($log_{10}$)")
    ax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='--')
    ax.annotate(
        "A", xy=(0.1, 0.1), color='k',
        xycoords="axes fraction", fontsize=18,
        bbox=dict(facecolor='lightgrey', edgecolor='black', pad=8),

    )

    plt.savefig(f'{savepath}/tw_zipf.png', dpi=300)
    plt.savefig(f'{savepath}/tw_zipf.pdf')

    plt.figure(figsize=figsize)
    ax = plt.gca()

    ax.loglog(df['tw_rank'], df['tw_count'], '--', color='r', alpha=.7, label='Twitter')
    df.sort_values(by='ft_count', ascending=False, inplace=True)
    ax.loglog(df['ft_rank'], df['ft_count'], marker='.', color='grey', label='FastText')

    for n in list(np.logspace(-0.5, np.log10(len(df)-1), 19).astype(int)):
        ax.annotate(
            df.index[n], (df['ft_rank'].iloc[n], df['ft_count'].iloc[n]),
            (df['ft_rank'].iloc[n]+n/10, df['ft_count'].iloc[n]+n/10),
            verticalalignment="bottom", horizontalalignment="left", fontsize=9,
        )

    ax.set_xlabel("Rank ($log_{10}$)")
    ax.set_ylabel("Absolute frequency ($log_{10}$)")
    ax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='--')

    ax.annotate(
        "B", xy=(0.1, 0.1),  color='k',
        xycoords="axes fraction", fontsize=18,
        bbox=dict(facecolor='lightgrey', edgecolor='black', pad=8),
    )
    plt.legend()
    plt.savefig(f'{savepath}/ft_zipf.png', dpi=300)
    plt.savefig(f'{savepath}/ft_zipf.pdf')


def plot_shifts(df, savepath):
    """ Plot language counts for twitter and fastText on diverging axes
    :param df: a dataframe of twitter/fasttext labels
    :param savepath: path to save plot
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'font.family': 'serif',
    })

    cols = 4
    fig, axis = plt.subplots(nrows=1, ncols=cols, figsize=(12, 10))
    langs = np.array_split(df.index.values, cols)
    r = np.array_split(
        (df.loc[:, 'ft_count'] - df.loc[:, 'tw_count']) / (df.loc[:, 'ft_count'] + df.loc[:, 'tw_count']),
        cols
    )

    for i in range(cols):
        ax = axis[i]
        for j, (lang, x) in enumerate(zip(langs[i], r[i])):
            if x > 0:
                a = ax.barh(lang, x, height=.75, color='grey', alpha=.4)
                ax.text(x=.05, y=j+.2, s=lang, size=8, color='k', horizontalalignment='left')
                ax.text(x=-.05, y=j+.2, s=f'{abs(x):.2f}', size=8, color='grey', horizontalalignment='right')
            else:
                b = ax.barh(lang, x, height=.75, color='red', alpha=.4)
                ax.text(x=-.05, y=j+.2, s=lang, size=8, color='k', horizontalalignment='right')
                ax.text(x=.05, y=j + .2, s=f'{abs(x):.2f}', size=8, color='red', horizontalalignment='left')

        ax.axvline(0, linestyle='-', color='dimgrey')
        ax.set_yticks([])
        ax.set_xlim((-1, 1))
        ax.set_ylim((-1, len(r[0])))
        ax.set_xticks([-1, -.5, 0, .5, 1])
        ax.set_xticklabels([1, .5, 0, .5,  1])
        ax.spines["top"].set_alpha(0.75)
        ax.spines["bottom"].set_alpha(0.0)
        ax.spines["right"].set_alpha(0.0)
        ax.spines["left"].set_alpha(0.0)
        ax.xaxis.set_ticks_position('top')
        ax.xaxis.set_label_position('top')
        ax.invert_yaxis()
        ax.set_xlabel('Divergence\n')

    axis[2].legend(
        [b, a], ['Twitter LID', 'FastText LID'],
        ncol=2, bbox_to_anchor=(0.625, -.025), frameon=False
    )
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_divergence(df, savepath):
    """ Plot language counts for twitter and fastText on diverging axes
    :param df: a dataframe of twitter/fasttext labels
    :param savepath: path to save plot
    :return: saves a figure to {savepath}
    """

    def rotate45(fig, pos):
        """ Rotates a figure 45 degrees """
        transformation = Affine2D().rotate_deg(45)
        ticks = [
            (-9, r"$\emptyset$"),
            (-8, r"$-8$"),
            (-7, r"$-7$"),
            (-6, r"$-6$"),
            (-5, r"$-5$"),
            (-4, r"$-4$"),
            (-3, r"$-3$"),
            (-2, r"$-2$"),
            (-1, r"$-1$"),
            (0, r"$0$"),
        ]
        grid_helper = floating_axes.GridHelperCurveLinear(
            transformation,
            extremes=(rmin, rmax, rmin, rmax),
            tick_formatter1=DictFormatter(dict(ticks)),
            tick_formatter2=DictFormatter(dict(ticks)),
        )
        ax = floating_axes.FloatingSubplot(fig, pos, grid_helper=grid_helper)
        fig.add_subplot(ax)
        aux_ax = ax.get_aux_axes(transformation)

        bleft = ax.axis['left']
        bleft.label.set_text(
            '$\log_{10}$ Relative rate of usage'+'\n\n'+r'$[ft]\ \bf{FastText}$'+'\n'+'Language Identification Model'
        )
        bleft.label.set_rotation(360)
        bleft.major_ticklabels.set_rotation(45)
        bleft.LABELPAD += 65

        bright = ax.axis['bottom']
        bright.label.set_text(
            '$\log_{10}$ Relative rate of usage'+'\n\n'+r'$[tw]\ \bf{Twitter}$'+'\n'+'Language Identification Model'
        )
        bright.major_ticklabels.set_rotation(-45)
        bright.LABELPAD += 15

        ax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='--')
        return aux_ax

    plt.rcParams.update({
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'xtick.major.pad': 10,
        'ytick.major.pad': 10,
        'font.family': 'serif',
    })

    inf = -9
    rmin, rmax = inf-1, 0
    callout_label_size = 9

    vmin, vmax = -1, 1
    bounds = [-1, -.8, -.4, -.1, 0, .1, .4, .8, 1]
    cbarlabels = ['1', '.8', '.4', '.1', '0', '.1', '.4', '.8', '1']
    cmap = plt.cm.get_cmap('coolwarm')
    cmaplist = [cmap(i) for i in range(cmap.N)]
    cmap = mcolors.LinearSegmentedColormap.from_list(None, cmaplist, cmap.N)
    norm = mcolors.BoundaryNorm(bounds, cmap.N)

    # main plot
    fig = plt.figure(figsize=(8, 8))
    fig.subplots_adjust(wspace=0.3, left=0.1, right=0.9, top=.95, bottom=.05)
    gs = fig.add_gridspec(ncols=4, nrows=4)
    ax = rotate45(fig, gs[:, :])
    ax.annotate(
        "C", xy=(.49, .25), color='k',
        xycoords="axes fraction", fontsize=14,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=5),
    )

    ft, tw, un = [], [], []
    for ll in df.index.values:
        if df['tw_freq'].loc[ll] == inf or df['ft_freq'].loc[ll] == inf:
            un.append([ll, df['tw_freq'].loc[ll], df['ft_freq'].loc[ll], df['divergence'].loc[ll]])

        elif df['tw_freq'].loc[ll] > df['ft_freq'].loc[ll]:
            tw.append([ll, df['tw_freq'].loc[ll], df['ft_freq'].loc[ll], df['divergence'].loc[ll]])

        else:
            ft.append([ll, df['tw_freq'].loc[ll], df['ft_freq'].loc[ll], df['divergence'].loc[ll]])

    # plot twitter data points
    tw = np.array(sorted(tw, key=lambda r: r[1], reverse=True), dtype=object)
    ax.scatter(
        tw[:, 1], tw[:, 2],
        marker='o',
        c=tw[:, -1],
        s=50,
        edgecolors='grey',
        cmap=cmap,
        vmin=vmin,
        vmax=vmax,
        norm=norm
    )

    # plot fastText data points
    ft = np.array(sorted(ft, key=lambda r: r[2], reverse=True), dtype=object)
    cls = ax.scatter(
        ft[:, 1], ft[:, 2],
        marker='o',
        c=ft[:, -1],
        s=50,
        edgecolors='grey',
        cmap=cmap,
        vmin=vmin,
        vmax=vmax,
        norm=norm
    )

    # plot middle lines
    separator = range(rmin, rmax+1)
    cutoff = -8.5
    ax.plot(separator, separator, 'grey', ls='--', lw=2, alpha=.5)
    separator = np.arange(rmin-1, rmax+.5, .5)
    ax.plot(np.full_like(separator, cutoff), separator, linestyle='--', color='grey')
    ax.plot(separator, np.full_like(separator, cutoff), linestyle='--', color='grey')

    # fill in area
    separator = np.arange(rmin, rmax+.5, .5)
    ax.fill_between(
        separator, np.full_like(separator, cutoff), np.full_like(separator, rmin),
        color='lightblue',
    )
    ax.fill_betweenx(
        separator, np.full_like(separator, cutoff), np.full_like(separator, rmin),
        color='lightblue',
    )

    # plot unique languages
    un = np.array(sorted(un, key=lambda r: r[2], reverse=True), dtype=object)
    ax.scatter(
        un[un[:, 1] <= inf, 1], un[un[:, 1] <= inf, 2],
        zorder=3, marker='s', s=10, c=cmap(0)
    )
    ax.scatter(
        un[un[:, 1] > inf, 1], un[un[:, 1] > inf, 2],
        zorder=3, marker='s', s=10, c='darkred'
    )

    dists, prev = cdist(tw[:, 1:], tw[:, 1:]), 0
    for i, (lang, x, y, r) in enumerate(tw):
        if dists[i, 0] - prev > .25 or prev == 0:
            ax.annotate(lang, (x+.2, y-.2), color='dimgrey', fontsize=callout_label_size)
        prev = dists[i, 0]

    dists, prev = cdist(ft[:, 1:], ft[:, 1:]), 0
    for i, (lang, x, y, r) in enumerate(ft):
        if dists[i, 0] - prev > .25 or prev == 0:
            ax.annotate(lang, (x-.4, y+.4), color='dimgrey', fontsize=callout_label_size)
        prev = dists[i, 0]

    for i, (lang, x, y, r) in enumerate(un):
        if (i % 12 == 0) and (x == inf):
            ax.annotate(lang, (x-.6, y+.1), color='dimgrey', fontsize=callout_label_size)
        elif y == inf:
            ax.annotate(lang, (x+.1, y-.2), color='dimgrey', fontsize=callout_label_size)

    # color bar
    cbarax = inset_axes(
        ax,
        width="40%",
        height="2%",
        bbox_to_anchor=(-.295, -1.0, 1, 1),
        bbox_transform=ax.transAxes,
        borderpad=.25,
    )

    cbar = colorbar.ColorbarBase(
        cbarax,
        cmap=cmap,
        norm=norm,
        spacing='proportional',
        ticks=bounds,
        boundaries=bounds,
        orientation='horizontal',
        #extend='both',
    )

    cbarax.xaxis.set_ticks_position('bottom')
    cbarax.tick_params(labelsize=8)
    cbarax.set_xticklabels(cbarlabels)
    #cbarax.set_title('Divergence', fontsize=12, pad=10)

    subplotlabel = (0.1, 0.1)
    ftax = inset_axes(
        ax,
        width="25%",
        height="25%",
        bbox_to_anchor=(-0.65, -0.1, 1, 1),
        bbox_transform=ax.transAxes,
        borderpad=.25,
    )

    t = df[df.ft_count != 0]
    t.sort_values(by='ft_freq', ascending=False, inplace=True)
    ftax.plot(t['ft_rank'], t['ft_freq'], marker='.', color='k', lw=.1, ms=3)
    ftax.set_xlabel("Rank")
    ftax.set_ylabel("$\log_{10}$ Relative\nrate of usage")
    ftax.yaxis.set_ticks_position('left')
    ftax.yaxis.set_label_position('left')
    ftax.xaxis.set_ticks_position('top')
    ftax.xaxis.set_label_position('top')
    ftax.set_xticks(range(0, 250, 50))
    ftax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='--')
    ftax.tick_params(direction='out', pad=5)

    ftax.annotate(
        "A", xy=subplotlabel, color='k',
        xycoords="axes fraction", fontsize=14,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=5),
    )

    twax = inset_axes(
        ax,
        width="25%",
        height="25%",
        bbox_to_anchor=(-0.08, -0.1, 1, 1),
        bbox_transform=ax.transAxes,
        borderpad=.25,
    )

    t = df[df.tw_count != 0]
    t.sort_values(by='tw_freq', ascending=False, inplace=True)
    twax.plot(t['tw_rank'], t['tw_freq'], marker='.', color='k', lw=.1, ms=3)
    twax.set_xlabel("Rank")
    twax.set_ylabel("$\log_{10}$ Relative\nrate of usage")
    twax.yaxis.set_ticks_position('right')
    twax.yaxis.set_label_position('right')
    twax.xaxis.set_ticks_position('top')
    twax.xaxis.set_label_position('top')
    twax.set_xticks(range(0, 250, 50))
    twax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='--')
    twax.tick_params(direction='out', pad=5)

    twax.annotate(
        "B", xy=subplotlabel, color='k',
        xycoords="axes fraction", fontsize=14,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=5),
    )

    twax.annotate('Twitter\nLID', xy=(.5, .75), xycoords="axes fraction", color='C3', fontsize=14)
    ftax.annotate('FastText\nLID', xy=(.45, .75), xycoords="axes fraction", color='C0', fontsize=14)

    plt.subplots_adjust(top=0.97, right=0.97)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.1)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.1)


def plot_story(savepath, langs, twlangs, ftlangs, hashtbl):
    """ Plot total number of languages over time
    :param savepath: path to save plot
    :param df: a dataframe of twitter/fasttext labels
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif'
    })

    subplotlabel = (-.15, .95)

    fig = plt.figure(figsize=(5, 8))
    gs = fig.add_gridspec(ncols=1, nrows=12)
    lw = .15
    callout_label_size = 20
    legend_font_size = 12

    colors = [consts.colors[lang] for lang in twlangs.columns]

    tsax = fig.add_subplot(gs[:4, :])
    nax = fig.add_subplot(gs[4:, :])

    tsax.plot(langs.tw_count, color='r', linestyle='--')
    tsax.plot(langs.ft_count, color='grey', linestyle='-')
    tsax.set_ylabel(f"Languages")
    tsax.set_xlabel("")

    tsax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='--')

    tsax.set_xlim(langs.index[0], langs.index[-1])
    tsax.xaxis.set_major_locator(mdates.YearLocator())
    tsax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
    tsax.xaxis.set_minor_locator(mdates.MonthLocator([4, 7, 10]))
    tsax.set_xticklabels([])

    tsax.annotate(
        "A", xy=subplotlabel, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
        #bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
    )

    tsax.annotate(
        "Metadata", xy=(.7, .45), color='r',
        xycoords="axes fraction", fontsize=14,
    )

    tsax.annotate(
        "FastText", xy=(.7, .85), color='k',
        xycoords="axes fraction", fontsize=14,
    )

    ftlangs = ftlangs.divide(ftlangs.sum(axis=1), axis=0)
    nax.stackplot(
        ftlangs.index, *ftlangs.values.T,
        labels=ftlangs.columns, colors=colors, lw=lw
    )
    nax.set_ylabel(f'Relative rate of usage')
    nax.set_xlabel("")
    nax.set_ylim(0, 1)
    nax.set_xlim(ftlangs.index[0], ftlangs.index[-1])
    nax.grid(True, which="both", axis='both', zorder=0, alpha=.3, lw=1, linestyle='--')

    nax.xaxis.set_major_locator(mdates.YearLocator())
    nax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
    nax.xaxis.set_minor_locator(mdates.MonthLocator([4, 7, 10]))
    plt.setp(nax.xaxis.get_majorticklabels(), ha='center', rotation=45)

    nax.annotate(
        "B", xy=subplotlabel, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
        #bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
    )

    patchs, labels = nax.get_legend_handles_labels()
    for i, lang in zip(range(len(labels)), labels):
        if lang == 'und':
            labels[i] = 'Undefined'
        elif lang == 'other':
            labels[i] = 'Other'
        elif lang == 'unknown':
            labels[i] = 'Unknown'
        else:
            try:
                labels[i] = hashtbl[hashtbl[:, 2] == lang, 0][0]
            except IndexError:
                labels[i] = hashtbl[hashtbl[:, 1] == lang, 0][0]

    patchs = patchs[::-1]
    labels = labels[::-1]

    nax.legend(
        patchs[1:], labels[1:],
        loc='center',
        bbox_to_anchor=(.45, -.25), ncol=3,
        fontsize=legend_font_size, frameon=False
    )

    plt.subplots_adjust(top=0.97, right=0.97)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_rank(savepath, ranks, langs, n):
    """ Plot top-n ranked languages over time
    :param savepath: path to save plot
    :param df: a dataframe of label counts per languages
    :param n: number of languages to plot
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'axes.axisbelow': False,
        'font.family': 'serif',
    })

    rows, cols = 6, 6

    fig = plt.figure(figsize=(12, 8))
    gs = fig.add_gridspec(ncols=cols, nrows=rows)

    # alluvial plot
    # ------------------------------------------------------------------------------------------------- #
    ax = fig.add_subplot(gs[:, :])
    colors = [consts.colors[lang] for lang in langs]

    ax = pd.plotting.parallel_coordinates(
        ranks, ax=ax, class_column='language',
        color=colors, lw=10, axvlines=False,
    )

    ax.legend_.remove()
    lines, labels = ax.get_legend_handles_labels()
    for line, label in zip(lines, labels):
        y = line.get_ydata()

        if y[0] <= n:
            ax.annotate(
                label, xy=(0, y[0]), xytext=(-6, 0), color=line.get_color(),
                xycoords=ax.get_yaxis_transform(), textcoords="offset points",
                size=16, va="center", ha='right'
            )

        if y[-1] <= n:
            ax.annotate(
                label, xy=(1, y[-1]), xytext=(6, 0), color=line.get_color(),
                xycoords=ax.get_yaxis_transform(), textcoords="offset points",
                size=16, va="center"
            )

    # ax.set_ylabel('Rank')
    ax.set_ylim(.5, n + .5)
    ax.set_yticks(range(1, n + 1))
    ax.set_yticklabels(range(1, n + 1), ha='left')
    ax.invert_yaxis()
    ax.tick_params(axis='y', pad=-15, direction='in', zorder=2)
    for x in ax.yaxis.get_ticklabels(): x.set_bbox(dict(facecolor='whitesmoke', edgecolor='k'))

    ax2 = ax.twinx()
    # ax2.set_ylabel('Rank', rotation=-90, labelpad=20)
    ax2.set_ylim(.5, n + .5)
    ax2.set_yticks(range(1, n + 1))
    ax2.set_yticklabels(range(1, n + 1), ha='right')
    ax2.invert_yaxis()
    ax2.tick_params(axis='y', pad=-15, direction='in', zorder=2)
    for x in ax2.yaxis.get_ticklabels(): x.set_bbox(dict(facecolor='whitesmoke', edgecolor='k'))

    ax.spines["top"].set_alpha(0.0)
    ax.spines["bottom"].set_alpha(1.0)
    ax.spines["right"].set_alpha(0.0)
    ax.spines["left"].set_alpha(0.0)
    ax.grid(True, which="both", alpha=.4, lw=1, linestyle='--')
    # ------------------------------------------------------------------------------------------------- #

    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_timeline(savepath, twitter, fasttext):
    """ Produce a timeline comparing the counts of languages on twitter
    :param savepath: path to save generated plot
    :param twitter: a dataframe of twitter labels
    :param fasttext: a dataframe of fasttext labels
    :return: saves a figure to {savepath}
    """
    cols = 3
    langs = sorted(map(str, set(fasttext.columns) | set(twitter.columns)))
    print(langs)
    rows = len(langs) // cols
    fig = make_subplots(
        rows=rows, cols=cols,
        subplot_titles=langs,
    )

    i = 0
    for r in range(1, rows+1):
        for c in range(1, cols+1):
            fig.add_trace(
                go.Scatter(
                    x=fasttext.index,
                    y=fasttext.iloc[:, i],
                    fill='tozeroy',
                    name='FastText',
                    mode='lines',
                    line=dict(color='dimgrey'),
                    xaxis=f'x{i+1}',
                    yaxis=f'y{i+1}',
                ),
                row=r, col=c
            )
            fig.add_trace(
                go.Scatter(
                    x=twitter.index,
                    y=twitter.iloc[:, i],
                    fill='tozeroy',
                    name='Twitter',
                    mode='lines',
                    line=dict(color='orangered', dash='dash'),
                    xaxis=f'x{i+1}',
                    yaxis=f'y{i+1}',
                ),
                row=r, col=c
            )

            fig.update_xaxes(
                row=r,
                col=c,
                range=['2008-09-01', '2020-01-01'],
                tickformat='%b<br>%Y',
            )

            fig.update_yaxes(range=[0, 8], type='log', row=r, col=c)

            i += 1

    fig.layout.update(dict(
        height=10000,
        title='Weekly rate of usage (number of messages)',
        font=dict(size=14),
        hovermode='x',
        showlegend=False,
    ))

    py.plot(fig, filename=savepath)


def plot_compare(savepath, ftweets, fretweets, ttweets, tretweets):
    """ Produce a timeline comparing the counts of languages on twitter
    :param savepath: path to save generated plot
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'axes.axisbelow': False,
        'font.family': 'serif',
    })
    cmap = plt.get_cmap('tab20')

    fig, axes = plt.subplots(figsize=(14, 14), nrows=5, ncols=4, sharey=True, sharex=True)

    labels = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z'.split(' ')

    for i, ax in enumerate(axes.flatten()):
        ax.set_title(ftweets.columns[i])

        ax.plot(ttweets.iloc[:, i], color=cmap(6), lw=2)
        ax.plot(tretweets.iloc[:, i], color=cmap(7), ls=':', marker='o', markevery=12, lw=2)

        ax.fill_between(
            ftweets.index[:50],
            0,
            ftweets.iloc[:50, i],
            color=cmap(1),
            alpha=.5,
        )

        ax.plot(ftweets.iloc[:, i], color='k', lw=2)
        ax.plot(fretweets.iloc[:, i], color='dimgrey', ls=':', marker='o', markevery=12, lw=2)

        ax.set_yscale('log')
        ax.set_ylim(10 ** 0, 10 ** 10)
        ax.set_xlim('2009', '2020')
        ax.xaxis.set_major_locator(mdates.YearLocator(2))
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
        ax.xaxis.set_minor_locator(mdates.YearLocator())
        ax.tick_params(axis='x', which='major', rotation=45)
        ax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='--')

        ax.annotate(
            labels[i], xy=(0.8, .2), color='k',
            xycoords="axes fraction", fontsize=20,
            bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
        )

    for i in range(5):
        axes[i, 0].set_ylabel('Number of messages')

    fig.legend(
        handles=[
            Line2D([0], [0], color='dimgrey', lw=12, linestyle='-', label='FastText'),
            Line2D([0], [0], color=cmap(6), lw=12, linestyle='-', label='Twitter'),
            Line2D([0], [0], color='k', lw=2, linestyle='-', label='Tweets'),
            Line2D([0], [0], color='grey', lw=2, linestyle=':', marker='o', label='Retweets'),
        ],
        bbox_to_anchor=(.85, 1.05), ncol=4,
        fontsize=18, frameon=False
    )

    plt.tight_layout()
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_summary(savepath, ftlangs, tweets, retweets, langs, hashtbl):
    """ Plot top-n ranked languages over time
    :param savepath: path to save plot
    :param df: a dataframe of label counts per languages
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'axes.axisbelow': False,
    })

    rows, cols = 5, 6

    fig = plt.figure(figsize=(11, 11))
    gs = fig.add_gridspec(ncols=cols, nrows=rows)
    lw = .15
    colors = [consts.colors[lang] for lang in ftlangs.columns]

    # ------------------------------------------------------------------------------------------------- #
    lax = fig.add_subplot(gs[:2, :])

    lax.stackplot(
        ftlangs.index,
        *ftlangs.values.T,
        labels=ftlangs.columns,
        colors=colors,
        lw=lw,
        baseline='sym',
    )
    lax.set_ylabel(f"Number of messages\n(Tweets + Retweets)")
    lax.set_xlabel("")
    lax.set_xlim(ftlangs.index[0], ftlangs.index[-1])
    lax.grid(True, which="both", axis='both', zorder=0, alpha=.3, lw=1, linestyle='--')

    lax.xaxis.set_major_locator(mdates.YearLocator())
    lax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
    lax.xaxis.set_minor_locator(mdates.MonthLocator([4, 7, 10]))
    plt.setp(lax.xaxis.get_majorticklabels(), ha='center')
    lax.spines['right'].set_visible(False)
    lax.spines['top'].set_visible(False)
    lax.spines['left'].set_position(('axes', -.01))
    lax.spines['bottom'].set_position(('axes', -.1))

    # ------------------------------------------------------------------------------------------------- #

    # Time series
    # ------------------------------------------------------------------------------------------------- #
    i = 0
    for r in range(2, rows):
        for c in range(cols):
            ax = fig.add_subplot(gs[r, c])
            ax.set_title(tweets.columns[i])

            tw = tweets.iloc[:, i]
            rt = retweets.iloc[:, i]

            ax.plot(tw, color=consts.types_colors['OT'], lw=1.5)
            ax.plot(rt, color=consts.types_colors['RT'], lw=1.5)

            if r == 2 and c == 0:
                ax.legend(
                    handles=[
                        Line2D([0], [0], color=consts.types_colors['OT'], lw=2, label='OT'),
                        Line2D([0], [0], color=consts.types_colors['RT'], lw=2, label='RT'),
                    ],
                    loc='center left',
                    bbox_to_anchor=(-.1, .5),
                    ncol=1,
                    frameon=False,
                    fontsize=10,
                )

            idx = np.argwhere((rt - tw) > 0).flatten()
            if len(idx) > 0:
                roi = rt[idx]
                dates = np.split(roi.index, np.where(np.diff(roi.index) != 1)[0] + 1)
                for roi in dates:
                    if len(roi) == 1:
                        ax.axvline(roi[0], color='red', alpha=0.1)
                    else:
                        ax.axvspan(roi[0], roi[-1], facecolor='red', alpha=0.1)

            ax.set_ylim(0, 1)
            ax.set_yticks([0, .3, .6, 1])

            if c == 0:
                ax.set_yticklabels(['0', '.3', '.6', '1'], fontsize=11)
                ax.set_ylabel(f"OT/RT\nBalance")
            else:
                ax.set_yticklabels([])

            ax.set_xlim('2009', '2020')
            ax.xaxis.set_major_locator(mdates.YearLocator(3))
            ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
            ax.xaxis.set_minor_locator(mdates.YearLocator())
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.spines['left'].set_position(('axes', -.1))
            ax.spines['bottom'].set_position(('axes', -.1))
            ax.tick_params(axis='x', which='major', rotation=45)

            ax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='--')

            i += 1
    # ------------------------------------------------------------------------------------------------- #

    patchs, labels = lax.get_legend_handles_labels()
    patchs = patchs[::-1]
    labels = labels[::-1]
    for i, lang in zip(range(len(labels)), labels):
        if lang == 'und':
            labels[i] = 'Undefined'
        elif lang == 'other':
            labels[i] = 'Other'
        elif lang == 'unknown':
            labels[i] = 'Unknown'
        else:
            labels[i] = hashtbl[hashtbl[:, 1] == lang, 0][0]

    lax.legend(
        patchs[1:], labels[1:],
        bbox_to_anchor=(.29, .33), ncol=2,
        fontsize=10, frameon=False
    )

    fig.tight_layout()
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_ratio(savepath, ftweets, fretweets):
    """ Produce a timeline comparing the counts of languages on twitter
    :param savepath: path to save generated plot
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'axes.axisbelow': False,
        'font.family': 'serif',
    })

    fig, axes = plt.subplots(figsize=(14, 14), nrows=6, ncols=5, sharey=True, sharex=True)

    for i, ax in enumerate(axes.flatten()):
        if i == 0:
            ax.legend(
                handles=[
                    Line2D([0], [0], color='C0', lw=3, linestyle='-', label=r'$\mathcal{O}$'),
                    Line2D([0], [0], color='C1', lw=3, linestyle=':', label=r'$\mathcal{R}$'),
                ],
                loc='center left',
                fontsize=14,
                ncol=1,
                frameon=False
            )

        ax.set_title(f'{ftweets.columns[i]}')

        tw = ftweets.iloc[:, i]
        rt = fretweets.iloc[:, i]

        ax.plot(tw, lw=1.5)
        ax.plot(rt, ls=':', lw=1.5)

        ax.set_ylim(0, 1)
        ax.set_xlim('2009', '2020')
        ax.xaxis.set_major_locator(mdates.YearLocator(2))
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
        ax.xaxis.set_minor_locator(mdates.YearLocator())
        ax.tick_params(axis='x', which='major', rotation=45)
        ax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='--')

        try:
            idx = np.argwhere((rt - tw) > 0).flatten()
            if len(idx) > 0:
                for d in rt[idx].index:
                    ax.axvline(d, color='grey', alpha=0.1)

        except IndexError:
            pass

    for i in range(6):
        axes[i, 0].set_ylabel(f'Relative\nrate of usage')

    plt.tight_layout()
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_stats(savepath, table):
    """ Plot a table of total number of messages captured by language
    :param savepath: path to save generated plot
    :param table: a dataframe of total number of messages by language
    :return: saves a figure to {savepath}
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })

    cols = 4
    vmin = 0
    vmax = 5*10**10
    bounds = [0, 10**5, 5*10**5, 10**6, 6*10**6, 10**7, 5*10**7, 10**8, 5*10**8, 10**9, 5*10**9, 10**10, 5*10**10]
    xlabels = [
        '100\nThousand', '500\nThousand',
        '1\nMillion', '5\nMillion',
        '10\nMillion', '50\nMillion',
        '100\nMillion', '500\nMillion',
        '1\nBillion', '5\nBillion', '10\nBillion',
    ]

    cmap = plt.cm.get_cmap('magma_r')
    cmaplist = [cmap(i) for i in range(cmap.N)]
    cmaplist[0] = (1, 1, 1, 1.0)  # force the first color entry to be white
    cmap = mcolors.LinearSegmentedColormap.from_list(None, cmaplist, cmap.N)
    norm = mcolors.BoundaryNorm(bounds, cmap.N)

    fig, axes = plt.subplots(figsize=(12, 14), ncols=cols)
    dfs = np.array_split(table, cols)

    cbarax = inset_axes(
        axes[3],
        width="2000%",
        height="2%",
        bbox_to_anchor=(.4, .03, 1, 1),
        bbox_transform=axes[3].transAxes,
        borderpad=.25,
    )

    for df, ax in zip(dfs, axes):
        ax = sns.heatmap(
            df,
            ax=ax,
            cbar_ax=cbarax,
            cbar=True,
            cmap=cmap,
            norm=norm,
            vmin=vmin,
            vmax=vmax,
            linewidths=0.1,
            linecolor='k',
        )

        ax.tick_params(
            axis='x',
            which='both',
            bottom=False,
            labelbottom=False
        )
        ax.set_ylabel('')
        ax.grid(False, which="both")

    cb = colorbar.ColorbarBase(
        cbarax,
        cmap=cmap,
        norm=norm,
        ticks=bounds,
        boundaries=bounds,
        orientation='horizontal',
        extend='both'
    )

    cbarax.tick_params(labelsize=14)
    cbarax.set_xticklabels(xlabels)
    cbarax.xaxis.set_ticks_position('top')
    cbarax.yaxis.set_label_position('left')

    plt.tight_layout()
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_contagiograms(savepath, langs, rolling_avg=True):
    """ Plot a grid of contagiograms
    :param savepath: path to save plot
    :param ngrams: a 2D-list of langs to plot
    :param rolling_avg: a toggle for plotting a rolling average of the timeseries
    :return: saves a figure to {savepath}
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    rows, cols = 16, 3
    fig = plt.figure(figsize=(12, 14))
    gs = fig.add_gridspec(ncols=cols, nrows=rows)
    at_color = 'k'
    ot_color = 'C0'
    rt_color = 'C1'
    labels = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z'.split(' ')
    window_size = 7
    metric = 'count'
    df = langs[0].dropna(how='all')
    start_date = df.index[0]
    end_date = df.index[-1]
    diff = end_date - start_date
    print(f'Last updated: {end_date}')

    if diff.days < 365:
        major_date_format = '%b'
        minor_date_format = ''
        major_locator = mdates.MonthLocator(range(1, int(np.ceil(diff.days/30) + 1)))
        minor_locator = mdates.MonthLocator()
        contagion_resolution = 'D'
    elif diff.days < 365 * 2:
        major_date_format = '%b\n%Y'
        minor_date_format = '%b'
        major_locator = mdates.YearLocator()
        minor_locator = mdates.MonthLocator([4, 7, 10])
        contagion_resolution = 'D'
    elif diff.days < 365 * 4:
        major_date_format = '%Y'
        minor_date_format = ''
        major_locator = mdates.YearLocator()
        minor_locator = mdates.AutoDateLocator()
        contagion_resolution = 'W'
    else:
        major_date_format = '%Y'
        minor_date_format = ''
        major_locator = mdates.YearLocator(2)
        minor_locator = mdates.YearLocator()
        contagion_resolution = 'M'

    i = 0
    for r in np.arange(0, rows, step=4):
        for c in np.arange(cols):

            ax = fig.add_subplot(gs[r + 1:r + 3, c])
            cax = fig.add_subplot(gs[r, c])

            df = langs[i]
            df.index = pd.to_datetime(df.index)
            df = df.dropna(how='all')

            start_date = df.index[0]
            end_date = df.index[-1]
            ax.set_xlim(start_date, end_date)
            cax.set_xlim(start_date, end_date)

            ax.xaxis.set_major_locator(major_locator)
            ax.xaxis.set_major_formatter(mdates.DateFormatter(major_date_format))
            ax.xaxis.set_minor_locator(minor_locator)
            ax.xaxis.set_minor_formatter(mdates.DateFormatter(minor_date_format))

            cax.xaxis.set_major_locator(major_locator)
            cax.xaxis.set_major_formatter(mdates.DateFormatter(major_date_format))
            cax.xaxis.set_minor_locator(minor_locator)
            cax.xaxis.set_minor_formatter(mdates.DateFormatter(minor_date_format))

            at = df['count'].resample(contagion_resolution).mean()
            ot = df['count_no_rt'].resample(contagion_resolution).mean()
            rt = at - ot

            df['count'] = df['count'].apply(np.log10).fillna(0)
            df['count_no_rt'] = df['count_no_rt'].apply(np.log10).fillna(0)

            df['freq'] = df['freq'].apply(np.log10).fillna(0)
            df['freq_no_rt'] = df['freq_no_rt'].apply(np.log10).fillna(0)

            df['rank'] = df['rank'].apply(np.log10).fillna(6)
            df['rank_no_rt'] = df['rank_no_rt'].apply(np.log10).fillna(6)

            cax.annotate(
                labels[i], xy=(-.16, 1.2), color='k', weight='bold',
                xycoords="axes fraction", fontsize=16,
            )
            cax.set_title(df.index.name)

            try:
                # plot contagion fraction
                try:
                    idx = np.argwhere((rt - ot) > 0).flatten()
                    if len(idx) > 0:
                        for d in rt[idx].index:
                            cax.axvline(d, color='grey', alpha=.25)

                except IndexError:
                    pass

                cax.plot(
                    ot / at,
                    lw=1,
                    color=ot_color
                )
                cax.plot(
                    rt / at,
                    lw=1,
                    color=rt_color
                )

                # plot timeseries
                ax.plot(
                    df[metric],
                    color='lightgrey',
                    lw=1,
                    zorder=0,
                )

                ax.plot(
                    df[metric].idxmax(), df[metric].max(),
                    'o', ms=15, color='orangered', alpha=0.5
                )
                ax.plot(
                    df[metric].idxmax(), df[metric].max(),
                    'o', ms=1,
                    color='k',
                    mfc='k',
                    mec='k',
                )

                if rolling_avg:
                    ts = df[metric].rolling(window_size, center=True).mean()
                    ax.plot(
                        ts,
                        color=at_color,
                        lw=1,
                    )

                vmax = np.ceil(df[metric].max()+.25)
                ax.set_ylim(None, vmax)

            except ValueError as e:
                print(f'Value error for {df.index.name}: {e}.')
                pass

            ax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')
            cax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')

            cax.set_xticklabels([], minor=False)
            cax.set_xticklabels([], minor=True)

            cax.set_ylim(0, 1)
            cax.set_yticks([0, .5, 1])
            cax.set_yticklabels(['0', '.5', '1'])
            cax.axhline(.5, color='k', lw=1)

            ax.spines['right'].set_visible(False)
            ax.spines['left'].set_visible(False)
            ax.spines['top'].set_visible(False)

            cax.spines['right'].set_visible(False)
            cax.spines['left'].set_visible(False)

            ax.text(
                df[metric].idxmax(),
                df[metric].max() + .2,
                df[metric].idxmax().strftime('%Y/%m/%d'),
                ha='center',
                verticalalignment='center',
                # transform=ax.transAxes,
                color='grey'
            )

            i += 1

            if c == 0:
                cax.text(
                    -0.22, 0.5, f"OT/RT\nBalance", ha='center',
                    verticalalignment='center', transform=cax.transAxes
                )

                ax.text(
                    -0.22, 0.5, f"AT", ha='center',
                    verticalalignment='center', transform=ax.transAxes
                )

                ax.text(
                    -0.22, 0.2, "Less\n↓", ha='center',
                    verticalalignment='center', transform=ax.transAxes, color='grey'
                )
                ax.text(
                    -0.22, 0.8, "↑\nMore", ha='center',
                    verticalalignment='center', transform=ax.transAxes, color='grey'
                )

        plt.subplots_adjust(top=0.97, right=0.97, hspace=0.25)
        plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
        plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_lang_usage(savepath, df):
    """ Plot a grid of contagiograms
    :param savepath: path to save plot
    :param df: a pandas dataframe from the database
    :return: saves a figure to {savepath}
    """

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    rows, cols = 4, 6
    fig = plt.figure(figsize=(10, 11))
    gs = fig.add_gridspec(ncols=cols, nrows=rows)
    aax = fig.add_subplot(gs[0, :])
    fax = fig.add_subplot(gs[1, :3])
    rax = fig.add_subplot(gs[1, 3:])
    uax = fig.add_subplot(gs[2, :])
    nax1 = fig.add_subplot(gs[3, :2])
    nax2 = fig.add_subplot(gs[3, 2:4])
    nax3 = fig.add_subplot(gs[3, -2:])

    window_size = 7
    start_date = df.index[0]
    end_date = df.index[-1]
    diff = end_date - start_date
    print(f'Last updated: {end_date}')

    if diff.days < 365:
        major_date_format = '%b'
        minor_date_format = ''
        major_locator = mdates.MonthLocator(range(1, int(np.ceil(diff.days/30) + 1)))
        minor_locator = mdates.MonthLocator()
    elif diff.days < 365 * 2:
        major_date_format = '%b\n%Y'
        minor_date_format = '%b'
        major_locator = mdates.YearLocator()
        minor_locator = mdates.MonthLocator([3, 5, 7, 9, 11])
    elif diff.days < 365 * 4:
        major_date_format = '%Y'
        minor_date_format = ''
        major_locator = mdates.YearLocator()
        minor_locator = mdates.AutoDateLocator()
    else:
        major_date_format = '%Y'
        minor_date_format = ''
        major_locator = mdates.YearLocator(2)
        minor_locator = mdates.YearLocator()

    for ax in [aax, fax, rax, uax, nax1, nax2, nax3]:
        ax.set_xlim(start_date, end_date)
        ax.xaxis.set_major_locator(major_locator)
        ax.xaxis.set_major_formatter(mdates.DateFormatter(major_date_format))
        ax.xaxis.set_minor_locator(minor_locator)
        ax.xaxis.set_minor_formatter(mdates.DateFormatter(minor_date_format))
        ax.grid(True, which="both", axis='both', alpha=.3, lw=1, linestyle='-')
        ax.ticklabel_format(axis='y', style='sci', useMathText=True, scilimits=(0, 3))
    rax.ticklabel_format(axis='y', style='plain')

    def plot(ax, ts, color, label):
        ax.plot(
            ts,
            color='lightgrey',
            zorder=0,
        )
        ax.plot(
            ts.rolling(window_size, center=True).mean(),
            label=label,
            color=color,
        )

    plot(
        aax,
        df['count'],
        color='dimgrey',
        label='AT'
    )

    plot(
        fax,
        (df['count'] - df['count_no_rt']),
        color=consts.types_colors['RT'],
        label='RT'
    )

    plot(
        fax,
        df['count_no_rt'],
        color=consts.types_colors['OT'],
        label='OT'
    )

    plot(
        rax,
        (df['count'] - df['count_no_rt']) / df['count'],
        color=consts.types_colors['RT'],
        label='RT'
    )

    plot(
        rax,
        df['count_no_rt'] / df['count'],
        color=consts.types_colors['OT'],
        label='OT'
    )

    for n in consts.ngrams:
        plot(
            uax,
            df[f'unique_{n}'],
            color=consts.ngrams_colors[n],
            label=n
        )

    for n, stream in zip(consts.ngrams, [nax1, nax2, nax3]):
        d = df.loc[:, [f'num_{n}', f'num_{n}_rt', f'num_{n}_no_rt']]
        d = d.resample('W').mean()
        stream.stackplot(
            d.index,
            d.values.T[::-1],
            baseline='sym',
            labels=list(consts.types_colors.keys())[::-1],
            colors=[consts.types_colors[t] for t in list(consts.types_colors.keys())[::-1]],
        )
        stream.set_title(n)
        stream.set_xlabel("")

    aax.set_ylabel('Number of tweets')
    fax.set_ylabel('Number of tweets')
    rax.set_ylabel('RT/OT Balance')
    uax.set_ylabel('Unique $n$-grams')
    nax1.set_ylabel(r'Volume of $n$-grams')

    aax.legend(frameon=False, loc='upper left')
    fax.legend(frameon=False, loc='upper left')
    uax.legend(frameon=False, loc='upper left')

    plt.tight_layout()
    sns.despine(offset=5)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_stats_timeseries(savepath, df):
    """ Plot ngram statistics overtime
    :param savepath: path to save generated plot
    :param df: a dataframe of ngram statistics
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'axes.facecolor': (0, 0, 0, 0),
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    fig = plt.figure(figsize=(12, 10))
    cols, rows = 3, 3
    gs = fig.add_gridspec(ncols=cols, nrows=rows)
    tags = 'A B C D E F G'.split()

    res = 'M'
    window_size = 30
    ax = fig.add_subplot(gs[0, :])
    for i, n in enumerate(consts.ngrams):
        ts = f'unique_{n}'
        ax.plot(
            df[ts],
            color='lightgrey',
            zorder=0,
        )

        ax.plot(
            df[ts].rolling(window_size, center=True).mean(),
            label=f'Unique {n}',
            color=consts.ngrams_colors[n],
            lw=3,
        )

        ax.xaxis.set_major_locator(mdates.YearLocator())
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
        ax.xaxis.set_minor_locator(mdates.MonthLocator())
        ax.set_xlim(df.index[0], df.index[-1])
        ax.grid(True, which="major", axis='both', zorder=0, alpha=.3, linestyle='-')
        ax.ticklabel_format(axis='y', style='sci', useMathText=True)

        ax.annotate(
            tags[0], xy=(-.03, 1.05), color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        stream = fig.add_subplot(gs[1, i])
        d = df.loc[:, [f'num_{n}', f'num_{n}_rt', f'num_{n}_no_rt']]
        d = d.resample(res).mean()
        x = d.index
        y = d.values.T

        stream.set_title(n)
        stream.stackplot(
            x,
            y[::-1],
            baseline='sym',
            labels=list(consts.types_colors.keys())[::-1],
            colors=[consts.types_colors[t] for t in list(consts.types_colors.keys())[::-1]],
        )

        stream.set_ylim(-8*10**8, 8*10**8)
        stream.ticklabel_format(axis='y', style='sci', useMathText=True)

        stream.set_xlabel("")
        stream.xaxis.set_major_locator(mdates.YearLocator(2))
        stream.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
        stream.xaxis.set_minor_locator(mdates.YearLocator())
        stream.set_xlim(df.index[0], df.index[-1])

        stream.annotate(
            tags[1 + i], xy=(-.1, 1.05), color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        ratio = fig.add_subplot(gs[2, i])
        y = y[1:] / y[0]

        ratio.stackplot(
            x,
            y,
            baseline='zero',
            labels=list(consts.types_colors.keys())[1:],
            colors=[consts.types_colors[t] for t in list(consts.types_colors.keys())[1:]],
        )

        ratio.set_xlabel("")
        ratio.set_ylim(0, 1)
        ratio.xaxis.set_major_locator(mdates.YearLocator(2))
        ratio.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
        ratio.xaxis.set_minor_locator(mdates.YearLocator())
        ratio.set_xlim(df.index[0], df.index[-1])

        ratio.annotate(
            tags[4 + i], xy=(-.1, 1.05), color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        if i % 3 != 0:
            stream.set_yticklabels([])
            ratio.set_yticklabels([])

        if i == 0:
            patchs, labels = stream.get_legend_handles_labels()
            stream.legend(patchs[::-1], labels[::-1], frameon=False, loc='upper left', ncol=3)

    patchs, labels = ax.get_legend_handles_labels()
    ax.legend(patchs[::-1], labels[::-1], frameon=False, loc='upper left')

    sns.despine(offset=5)
    plt.tight_layout()
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_stats_dists(savepath, df):
    """ Plot ngram distributions
    :param savepath: path to save generated plot
    :param df: a dataframe of ngram statistics
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'axes.facecolor': (0, 0, 0, 0),
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    def dist(ax, t, c='grey', ll='', xlim=(0, None), ylim=(0, None)):
        sns.distplot(
            df[t],
            ax=ax,
            kde=True,
            bins=100,
            color=c,
            kde_kws={"lw": 3, "label": "KDE"},
        )

        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.axvline(df[t].median(), ls='--', lw=2, color='k', label='median')
        ax.ticklabel_format(axis='both', style='sci', useMathText=True)
        ax.legend().remove()
        ax.set_xlabel('')
        ax.set_title(ll)

    fig = plt.figure(figsize=(10, 12))
    cols, rows = 3, 4
    gs = fig.add_gridspec(ncols=cols, nrows=rows)
    tags = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z'.split(' ')
    tag_pos = (-.25, 1.075)

    for i, n in enumerate(consts.ngrams):

        unique = fig.add_subplot(gs[0, i])
        dist(
            ax=unique,
            t=f'unique_{n}',
            c=consts.ngrams_colors[n],
            ll=f'{n}\n\nUnique',
            xlim=(0, 3 * 10**7) if i == 0 else (0, 2 * 10**8),
            ylim=(0, 2*10 ** -7) if i == 0 else (0, 3*10 ** -8),
        )
        unique.annotate(
            tags[i], xy=tag_pos, color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        at = fig.add_subplot(gs[1, i])
        dist(
            ax=at,
            t=f'num_{n}',
            c=consts.types_colors['AT'],
            ll=f'ATs',
            xlim=(0, 8 * 10 ** 8),
            ylim=(0, 5 * 10 ** -9),
        )
        at.annotate(
            tags[i+4], xy=tag_pos, color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        rt = fig.add_subplot(gs[2, i])
        dist(
            ax=rt,
            t=f'num_{n}_rt',
            c=consts.types_colors['RT'],
            ll=f'RTs',
            xlim=(0, 8 * 10 ** 8),
            ylim=(0, 6 * 10 ** -9),
        )
        rt.annotate(
            tags[i+8], xy=tag_pos, color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        ot = fig.add_subplot(gs[3, i])
        dist(
            ax=ot,
            t=f'num_{n}_no_rt',
            c=consts.types_colors['OT'],
            ll=f'OTs',
            xlim=(0, 8 * 10 ** 8),
            ylim=(0, 9 * 10 ** -9),
        )
        ot.annotate(
            tags[i+12], xy=tag_pos, color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        if i == 0:
            unique.legend(frameon=False)
            unique.set_ylabel('Density')
            at.set_ylabel('Density')
            ot.set_ylabel('Density')
            rt.set_ylabel('Density')

    sns.despine(offset=5)
    plt.tight_layout()
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
