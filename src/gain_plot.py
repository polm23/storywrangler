"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""

import sys
import time
from pathlib import Path

import cli
import gain_analytics
import pandas as pd
import ujson


def parse_args(args, config):
    parser = cli.parser()

    # optional subparsers
    subparsers = parser.add_subparsers(help='Arguments for specific action.', dest='dtype')
    subparsers.required = True

    lang_gain_parser = subparsers.add_parser(
        'gain_complex',
        help='Compare growth of sharing among languages'
    )
    lang_gain_parser.add_argument(
        'langpath',
        help='path to CSV file of language statistics (ie. /tmp/languages.csv.gz)'
    )

    lang_ratio_parser = subparsers.add_parser(
        'ratio_complex',
        help='Compare growth of sharing among languages'
    )
    lang_ratio_parser.add_argument(
        'langpath',
        help='path to CSV file of language statistics (ie. /tmp/languages.csv.gz)'
    )

    gain_parser = subparsers.add_parser(
        'gain',
        help='Compare gain among languages'
    )
    gain_parser.add_argument(
        'langpath',
        help='path to CSV file of language statistics (ie. /tmp/languages.csv.gz)'
    )

    ratio_parser = subparsers.add_parser(
        'ratio',
        help='Compare ratio among languages'
    )
    ratio_parser.add_argument(
        'langpath',
        help='path to CSV file of language statistics (ie. /tmp/languages.csv.gz)'
    )

    gain_heatmap_parser = subparsers.add_parser(
        'gain_heatmap',
        help='Compare gain among languages'
    )
    gain_heatmap_parser.add_argument(
        'langpath',
        help='path to CSV file of language statistics (ie. /tmp/languages.csv.gz)'
    )

    ratio_heatmap_parser = subparsers.add_parser(
        'ratio_heatmap',
        help='Compare ratio among languages'
    )
    ratio_heatmap_parser.add_argument(
        'langpath',
        help='path to CSV file of language statistics (ie. /tmp/languages.csv.gz)'
    )

    gain_timeseries_parser = subparsers.add_parser(
        'gaints',
        help='Plot a time series of gain'
    )
    gain_timeseries_parser.add_argument(
        'langpath',
        help='path to CSV file of language statistics (ie. /tmp/languages.csv.gz)'
    )

    ratio_timeseries_parser = subparsers.add_parser(
        'ratiots',
        help='Plot a time series of ratio'
    )
    ratio_timeseries_parser.add_argument(
        'langpath',
        help='path to CSV file of language statistics (ie. /tmp/languages.csv.gz)'
    )

    pred_gain_parser = subparsers.add_parser(
        'pred',
        help='Compare growth of sharing among languages'
    )
    pred_gain_parser.add_argument(
        'langpath',
        help='path to CSV file of language statistics (ie. /tmp/languages.csv.gz)'
    )

    timeline_parser = subparsers.add_parser(
        'contagion',
        help='Plot a timeline comparing comparing contagion ratios among languages on twitter'
    )
    timeline_parser.add_argument(
        'langpath',
        help='path to CSV file of language statistics (ie. /tmp/languages.csv.gz)'
    )

    diff_parser = subparsers.add_parser(
        'diff',
        help='Plot growth of retweet-ing by language'
    )
    diff_parser.add_argument(
        'langpath',
        help='path to CSV file of language statistics (ie. /tmp/languages.csv.gz)'
    )

    ngrams_gain_parser = subparsers.add_parser(
        'ngrams',
        help='Compare growth of sharing among ngrams'
    )

    ngrams_gain_parser.add_argument(
        'ngramspath',
        help='path to a directory of ngram samples or a sample file /tmp/sample.json'
    )
    ngrams_gain_parser.add_argument(
        'tspath',
        help='path to a directory of ngram timeseries file(s) (ie. /tmp/timeseries/[the.tsv.gz ... test.tsv.gz])'
    )

    # optional args
    parser.add_argument(
        '-r', '--resolution',
        default='W',
        help="group days based on a given timescale [i.e. 'D', 'W', 'M', '6M', 'Y']"
    )

    parser.add_argument(
        '-s', '--sample',
        action='store_true',
        help='get a random sample of ngrams and save it to a json file'
    )

    parser.add_argument(
        '-o', '--outdir',
        default=Path(config['plots'])/'gain',
        help='absolute Path to save figures'
    )

    parser.add_argument(
        '-m', '--models',
        default=Path(config['models']),
        help='absolute Path to bayesian fit models'
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    for p in Path(sys.argv[0]).resolve().parents:
        if str(p).endswith('tlid'):
            with open(p/'config.json', 'r') as cfg:
                config = ujson.load(cfg)
                break

    args = parse_args(args, config)
    out = Path(args.outdir)
    out.mkdir(parents=True, exist_ok=True)

    if args.dtype != 'ngrams':
        try:
            df = pd.read_csv(
                Path(args.langpath),
                sep=',',
                header=0,
                usecols=[
                    'date', 'language',
                    'ft_count', 'ft_speakers',
                    'ft_tweets', 'ft_retweets', 'ft_comments',
                ],
                memory_map=True,
                index_col='date'
            )
            df.language.fillna('unknown', inplace=True)
            df.index = pd.to_datetime(df.index)

        except:
            raise Exception('Error: wrong file format!')

        # VERY special data cleaning for the paper...
        # ---------------------------------------------------------------------------------------------------------------- #
        df = df.groupby(['date', 'language']).sum().reset_index()
        df = df.set_index('date')
        df = df.loc['2009':'2019']

        df['ft_freq'] = df['ft_count'] / df['ft_count'].sum()
        df['ft_rank'] = df['ft_count'].rank(method='average', ascending=False)
        # ---------------------------------------------------------------------------------------------------------------- #

    supported_languages = pd.read_csv(config['supported_languages'], header=0).fillna(value='unknown')

    if args.dtype == 'ratio_complex':
        gain_analytics.lang_ratio(df, out, hashtbl=supported_languages.values)

    elif args.dtype == 'gain_complex':
        gain_analytics.lang_gain(df, out, hashtbl=supported_languages.values, models=args.models)

    elif args.dtype == 'ratio':
        gain_analytics.ratio(df, out, hashtbl=supported_languages.values)

    elif args.dtype == 'gain':
        gain_analytics.gain(df, out, hashtbl=supported_languages.values)

    elif args.dtype == 'ratio_heatmap':
        gain_analytics.ratio_heatmap(df, out, hashtbl=supported_languages.values)

    elif args.dtype == 'gain_heatmap':
        gain_analytics.gain_heatmap(df, out, hashtbl=supported_languages.values)

    elif args.dtype == 'gaints':
        gain_analytics.gain_timeseries(df, out)

    elif args.dtype == 'ratiots':
        gain_analytics.ratio_timeseries(df, out)

    elif args.dtype == 'pred':
        gain_analytics.gain_predictions(df, out, hashtbl=supported_languages.values, models=args.models)

    elif args.dtype == 'contagion':
        gain_analytics.contagion_timeline(df, out, hashtbl=supported_languages.values, resolution=args.resolution)

    elif args.dtype == 'diff':
        gain_analytics.diff(df, out, hashtbl=supported_languages.values, resolution=args.resolution)

    elif args.dtype == 'ngrams':
        ngrams = Path(args.ngramspath)
        tseries = Path(args.tspath)

        if args.sample:
            gain_analytics.sample(ngrams, savepath=ngrams / 'sample.json')
            ngrams = ngrams / 'sample.json'

        gain_analytics.ngrams_gain(ngrams, tseries, out)

    else:
        print('Error: unknown action!')

    print(f'Total time elapsed: {time.time() - timeit:.2f} secs.')


if __name__ == "__main__":
    main()
