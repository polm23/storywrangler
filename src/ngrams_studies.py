"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""

import os
from datetime import datetime, timedelta
from pathlib import Path

import dill
import ngrams_vis
import ngrams_risk
import ngrams_movies
import numpy as np
import pandas as pd
from ngrams_query import Query
import regexr


def query_lang_array(
        save_path,
        lang,
        database,
        ngrams,
        rt=True,
        start_date=datetime(2009, 1, 1)
):
    """Query a given language collection in the database

    Args:
        save_path (pathlib.Path): path to save generated timeseries
        lang (string): language collection
        database (string): database codename
        ngrams (list): a list of ngrams to query
        rt (bool): a toggle to include retweets
        start_date (datetime): starting date for the query
    """
    if rt:
        dfs = {
            'count': None,
            'rank': None,
            'freq': None
        }
    else:
        dfs = {
            'count_no_rt': None,
            'rank_no_rt': None,
            'freq_no_rt': None
        }

    q = Query(database, lang)
    print(f'Starting date: {start_date.date()}')
    d_arr = q.query_timeseries_array(list(ngrams), start_time=start_date)

    for k in dfs.keys():
        to_update = d_arr.pivot(index='time', columns='word', values=k)
        to_update = to_update[to_update.index == to_update.index]  # remove NaTs in index
        dfs[k] = to_update
        dfs[k].index.name = k
        file = Path(f'{save_path}/{k}.tsv.gz')

        if file.exists():
            old = pd.read_csv(file, header=0, index_col=0, na_filter=False, sep='\t')
            old[old == ''] = np.nan

            old = old.combine_first(dfs.get(k))
            old.to_csv(file, sep='\t')
        else:
            dfs.get(k).to_csv(file, sep='\t')


def pantheon(pantheon, ngrams):
    """ parse out ngram timeseries for the pantheon case study
    :param pantheon: path to pantheon dataset
    :param ngrams: path to ngram timeseries
    """
    figures = pd.read_csv(
        pantheon, sep='\t', header=0, index_col=0,
        usecols=[
            'name', 'countryName', 'birthyear',
            'occupation', 'gender', 'industry',
        ]
    )
    countries = ['united states']
    occupations = [
        'politician', 'businessperson', 'writer', 'artist'
        'computer scientist', 'physicist', 'inventor',
        'social activist', 'extremist',
        'actor', 'film director', 'singer', 'musician',
        'american football player', 'soccer player'
    ]

    figures = figures.apply(lambda x: x.astype(str).str.lower())
    figures["birthyear"] = pd.to_numeric(figures["birthyear"], errors='coerce')
    names = figures.index.str.split()
    figures['firstname'] = names.str[0]
    figures['lastname'] = names.str[-1]
    figures['2grams'] = figures['firstname'].str.strip('123.!? \n\t') + " " + \
                        figures['lastname'].str.strip('123.!? \n\t')

    figures = figures[figures['birthyear'] > 1900]
    figures = figures[figures['countryName'].isin(countries)]
    figures = figures[figures['occupation'].isin(occupations)]
    targets = figures['2grams'].unique()

    print(f"Retrieving: {len(targets)} timeseries ...")
    query_lang_array(ngrams, 'en', '2grams', targets, rt=False)
    query_lang_array(ngrams, 'en', '2grams', targets)

    df = pd.read_csv(f'{ngrams}/rank.tsv.gz', sep='\t', header=0, index_col=0)
    df.index = pd.to_datetime(df.index)
    df = df.apply(np.log10).fillna(6)

    timeseries = None
    for name in targets:
        person = figures.loc[figures['2grams'] == name].iloc[0].to_dict()
        ts = pd.DataFrame(dict(
            ngram=name,
            occupation=person['occupation'],
            age=2020 - person['birthyear'],
            gender=person['gender'],
            industry=person['industry'],
            date=df.index,
            val=df[name].values
        ))

        timeseries = timeseries.append(ts) if timeseries is not None else ts

    timeseries.to_csv(Path(f'{ngrams}/figures.tsv.gz'), sep='\t')


def pantheon_figures(pantheon, savepath):
    """ Plot time series of famous individuals on Twitter
    :param pantheon: path to pantheon dataset (see pantheon())
    :param savepath: path to save generated plot
    """
    pantheon = pd.read_csv(pantheon, sep='\t', header=0)
    pantheon.loc[pantheon['ngram'] == 'Donald Trump', 'occupation'] = 'politician'
    pantheon.loc[pantheon['ngram'] == 'Taylor Swift', 'occupation'] = 'singer'

    ngrams_vis.plot_pantheon_figures(
        f'{savepath}/pantheon_figures',
        pantheon,
    )
    print(f'Saved: {savepath}/pantheon_figures')


def pantheon_age(pantheon, savepath):
    """ Plot KDE of rank and age of famous individuals on twitter
    :param pantheon: path to pantheon dataset (see pantheon())
    :param savepath: path to save generated plot
    """
    pantheon = pd.read_csv(pantheon, sep='\t', header=0)
    pantheon.loc[pantheon['ngram'] == 'Donald Trump', 'occupation'] = 'politician'
    pantheon.loc[pantheon['ngram'] == 'Taylor Swift', 'occupation'] = 'singer'

    ngrams_vis.plot_pantheon_age(
        f'{savepath}/pantheon_age',
        pantheon,
    )
    print(f'Saved: {savepath}/pantheon_age')


def studies(pantheon, risk, movies, nparser, savepath):
    """ Plot a grid of case studies
    :param pantheon: path to pantheon dataset (see pantheon())
    :param risk: path to risk data
    :param movies: path to movies data
    :param savepath: path to save generated plot
    """
    pantheon = pd.read_csv(pantheon, sep='\t', header=0)
    pantheon.loc[pantheon['ngram'] == 'Donald Trump', 'occupation'] = 'politician'
    pantheon.loc[pantheon['ngram'] == 'Taylor Swift', 'occupation'] = 'singer'

    risk_data_files = [x for x in os.listdir(risk) if x.endswith('json')]
    risk_dfs = ngrams_risk.get_word_dfs(risk, risk_data_files)
    gpr_betas = pd.read_csv(f'{risk}/gpr_betas.csv', header=0)
    gpr = ngrams_risk.get_monthly_gpr_data(risk, pct_change=True)

    print('Retrieving timeseries')
    start_date = datetime(2010, 1, 1)
    targets = dict(
        tv=[
            ('Game of Thrones', 'en', 'C0'),
            ('The Walking Dead', 'en', 'C1'),
            ('Black Mirror', 'en', 'C3'),
        ],
        movies=[
            ('Inception', 'en', 'C0'),
            ('Iron Man 3', 'en', 'C1'),
            ('Interstellar', 'en', 'C2'),
            ('Star Wars', 'en', 'C4'),
            ('Black Panther', 'en', 'C3'),
        ],
    )

    tv_movies_timeseries = None
    for topic, ngrams in targets.items():
        for i, (w, lang, color) in enumerate(ngrams):
            n = len(regexr.ngrams(w, parser=nparser, n=1))
            print(f"{n}gram -- '{w}'")

            q = Query(f'{n}grams', lang)
            d = q.query_timeseries(w, start_time=start_date)

            ts = pd.DataFrame(dict(
                ngram=w,
                topic=topic,
                date=d.index,
                val=d['rank'].apply(np.log10).fillna(6),
                color=color,
                lang=lang,
            ))

            if tv_movies_timeseries is not None:
                tv_movies_timeseries = tv_movies_timeseries.append(ts)
            else:
                tv_movies_timeseries = ts

    meta_data = 'the-movies-dataset/movies_metadata.csv'
    movie_ts = 'movie_timeseries_large_hashtag.dkl'
    movie2tsdf = dill.load(open(movies / movie_ts, 'rb'))

    movie2year, movie2date, movie2shortnames, movies = ngrams_movies.load_movies(
        movies / meta_data
    )

    dec_inc_df = ngrams_movies.get_dec_inc_df(
        movie2shortnames, movie2year, movie2date, movie2tsdf, movies, savepath
    )

    movie2dub = {}
    dubs = []
    halfs = []
    for key, val in movie2shortnames.items():
        print(key, end='\t \r')
        try:
            base_data = movie2tsdf[val].rolling(14, center=True).mean()
        except AttributeError:
            continue
        if not bool(all(base_data.mean() == base_data.mean())):
            continue
        dub = ngrams_movies.growth_decay_finder(base_data, starting_val=.5, increasing=True)
        half = ngrams_movies.growth_decay_finder(base_data, starting_val=.5, increasing=False)
        target = base_data.loc[dub[1] - timedelta(days=3):half[1] + timedelta(days=3)]['freq']

        target /= target.max()

        split_range = np.arange(-dub[0] - 3, half[0] + 4, dtype=int)
        temp_df = pd.DataFrame([split_range, target.values]).T
        temp_df.set_index(0, drop=True, inplace=True)
        movie2dub.update({key: temp_df})
        dubs.append(dub[0])
        halfs.append(dub[0])

    all_traj = pd.concat(list(movie2dub.values()), axis=1)
    med = np.nanmedian(all_traj.loc[-15.0:15.0], axis=1)
    med = med[med >= .5]
    half_med = med.shape[0] // 2

    ngrams_vis.plot_studies(
        f'{savepath}/studies',
        pantheon,
        risk_dfs,
        gpr, gpr_betas,
        tv_movies_timeseries,
        movie2dub,
        half_med,
        med,
        dec_inc_df
    )
    print(f'Saved: {savepath}/studies')

