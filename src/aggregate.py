"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""


import logging
import shutil
import sys
import tarfile
import time
from pathlib import Path

import cli
import ujson
from utils import aggregate_ngrams


def parse_args(args, config):
    parser = cli.parser()

    parser.add_argument(
        'datapth',
        help='path to a [day] directory of ngrams (see `collect_ngrams.py`)'
    )

    # optional args
    parser.add_argument(
        '-o', '--outdir',
        default=Path(config['ngrams']),
        help='absolute Path to save network predictions'
    )

    parser.add_argument(
        '-n', '--ngrams',
        default=1,
        type=int,
        help='n-grams scheme to use'
    )

    parser.add_argument(
        '-x', '--codes',
        default=Path(config['language_codes']),
        help='path to (.json) language hashtable'
    )

    parser.add_argument(
        '-l', '--languages',
        action='store_true',
        help='collect language statistics and save to _languages.csv.gz'
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    for p in Path(sys.argv[0]).resolve().parents:
        if str(p).endswith('tlid'):
            with open(p/'config.json', 'r') as cfg:
                config = ujson.load(cfg)
                break

    args = parse_args(args, config)

    Path(config["logs"]).mkdir(parents=True, exist_ok=True)
    logfile = f'{config["logs"]}/n{args.ngrams}{Path(args.datapth).stem}.log'
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO, filename=logfile)
    print = logging.info
    logging.info('Initialized logging...')
    logging.info(args)

    indir = Path(args.datapth)
    outdir = Path(args.outdir) if args.outdir else Path(args.datapth)

    done = aggregate_ngrams(
        indir,
        outdir,
        int(args.ngrams),
        args.codes,
        args.languages,
    )

    if done:
        print('Compressing files... ')
        with tarfile.open(f'{outdir}.tar.gz', "w:gz") as tar:
            tar.add(outdir, arcname=outdir.stem)

        print('Deleting temporary files... ')
        shutil.rmtree(outdir, ignore_errors=True)

    print(f'Total time elapsed: {time.time() - timeit:.2f} sec. ~ {outdir}')


if __name__ == "__main__":
    main()
