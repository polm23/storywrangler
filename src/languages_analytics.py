"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""

import re
import time
import tarfile
from datetime import datetime
from pathlib import Path

import consts
import fileio
import languages_vis
import numpy as np
import pandas as pd
import tweet_utils

from ngrams_query import Query

import logging
#print = logging.info


def lang_stats(path, p_language_codes):
    """ Create a dataframe of language statistics for a given day
    :param path: path to predictions (.csv/.tar) file
    :param p_language_hashtable: path to a dict of language codes
    :return: a dataframe of language usage
        ['language', 'ft_count', 'tw_count', 'ft_rank', 'tw_rank', 'ft_freq', 'tw_freq']
    """
    path = Path(path)
    language_codes = fileio.load_json(p_language_codes)
    cols = ['twitter_label', 'fastText_label', 'fastText_conf_score', 'user_id', 'rt', 'length']

    df = tweet_utils.read_tarball(
        path,
        filename='predictions',
        kwargs=dict(
            header=0,
            compression='gzip',
            encoding='utf-8',
            sep=',',
            usecols=cols
        )
    )

    tw = df.twitter_label.value_counts().to_frame(name='count')
    tw['speakers'] = df.groupby(['user_id', 'twitter_label'], as_index=False) \
        .size().reset_index()['twitter_label'].value_counts()

    try:
        tw['tweets'] = df.groupby(['rt', 'twitter_label'], as_index=False) \
            .size().to_frame(name='count').loc[consts.tweet_types['tweet']]
    except KeyError:
        tw['tweets'] = 0
    try:
        tw['retweets'] = df.groupby(['rt', 'twitter_label'], as_index=False) \
            .size().to_frame(name='count').loc[consts.tweet_types['retweet']]
    except KeyError:
        tw['retweets'] = 0
    try:
        tw['comments'] = df.groupby(['rt', 'twitter_label'], as_index=False) \
            .size().to_frame(name='count').loc[consts.tweet_types['comment']]
    except KeyError:
        tw['comments'] = 0

    tw['language'] = tw.index.map({v: k for k, v in language_codes.items()})
    tw = tw.set_index('language')

    ft = df.fastText_label.value_counts().to_frame(name='count')
    ft['speakers'] = df.groupby(['user_id', 'fastText_label'], as_index=False) \
        .size().reset_index()['fastText_label'].value_counts()
    try:
        ft['tweets'] = df.groupby(['rt', 'fastText_label'], as_index=False) \
            .size().to_frame(name='count').loc[consts.tweet_types['tweet']]
    except KeyError:
        ft['tweets'] = 0
    try:
        ft['retweets'] = df.groupby(['rt', 'fastText_label'], as_index=False) \
            .size().to_frame(name='count').loc[consts.tweet_types['retweet']]
    except KeyError:
        ft['retweets'] = 0
    try:
        ft['comments'] = df.groupby(['rt', 'fastText_label'], as_index=False) \
            .size().to_frame(name='count').loc[consts.tweet_types['comment']]
    except KeyError:
        ft['comments'] = 0
    ft['language'] = ft.index.map({v: k for k, v in language_codes.items()})
    ft = ft.set_index('language')

    del df

    if len(tw) > 0:
        tw['rank'] = tw['count'].rank(method='average', ascending=False)
        total = tw['count'].sum()
        tw['freq'] = tw['count'] / total
    else:
        print('Warning: no twitter labels!')

    if len(ft) > 0:
        ft['rank'] = ft['count'].rank(method='average', ascending=False)
        total = ft['count'].sum()
        ft['freq'] = ft['count'] / total
    else:
        print('Warning: no fastText labels!')

    ft = ft.add_prefix('ft_')
    tw = tw.add_prefix('tw_')
    df = pd.concat(
        [ft, tw], axis=1, join='outer', sort=False
    ).sort_values('ft_count', ascending=False)
    df.index.name = 'language'
    return df.fillna(value=0)


def ngrams_stats(df, datapath):
    """ Collect ngrams-related language statistics for a given day
    :param df: a dataframe of language usage
    :param datapath: path to an ngrams tarball (see parser.py)
    :return: updated dataframe
    """
    try:
        scheme = datapath.parent.stem
        languages = {lang: {} for lang in df.index.values}

        with tarfile.open(datapath) as obj:
            filenames = obj.getnames()
            for i, lang in enumerate(languages):
                t = time.time()
                files = [x for x in filenames if re.search(f"^{lang}_", Path(x).stem)]
                try:
                    ngrams = pd.read_csv(
                        obj.extractfile(files[0]),
                        header=0,
                        compression='gzip',
                        encoding='utf-8',
                        sep='\t',
                        usecols=['count', 'count_no_rt']
                    )

                    languages[lang][f'num_{scheme}'] = int(ngrams['count'].sum())
                    languages[lang][f'num_{scheme}_no_rt'] = int(ngrams['count_no_rt'].sum())
                    languages[lang][f'unique_{scheme}'] = int((ngrams['count'] != 0).sum())
                    languages[lang][f'unique_{scheme}_no_rt'] = int((ngrams['count_no_rt'] != 0).sum())

                    print(f'Loaded {files[0]} ~ {time.time() - t:.2f} secs.')

                except IndexError:
                    languages[lang][f'num_{scheme}'] = 0
                    languages[lang][f'num_{scheme}_no_rt'] = 0
                    languages[lang][f'unique_{scheme}'] = 0
                    languages[lang][f'unique_{scheme}_no_rt'] = 0

        d = pd.DataFrame.from_dict(
            languages,
            orient='index',
            columns=[
                f'num_{scheme}',
                f'num_{scheme}_no_rt',
                f'unique_{scheme}',
                f'unique_{scheme}_no_rt'
            ]
        )
        return df.combine_first(d)

    except OSError:
        print(f"File not found: {datapath}")


def aggregate_counts(paths, savepath, vacc=False):
    """ Combine a list of dataframes
    :param paths: a list of paths to csv file(s)
    :param vacc: option to process compressed flat files on the VACC
    :param savepath: path to dataframe
    :return: a dataframe
    """
    cols = [
        "ft_count", "ft_speakers", "ft_tweets", "ft_retweets", "ft_comments", "ft_rank", "ft_freq",
        "tw_count", "tw_speakers", "tw_tweets", "tw_retweets", "tw_comments", "tw_rank", "tw_freq"
    ]

    def load_tbl(path):
        path = Path(path)
        print(f'Processing: {path}')
        if vacc:
            df = tweet_utils.read_tarball(
                path, filename='languages',
                kwargs=dict(
                    header=0,
                    memory_map=True,
                    compression='gzip',
                    encoding='utf-8',
                    sep=',',
                    names=cols,
                )
            )
            df.index.name = 'language'
            df.reset_index(inplace=True)
        else:
            df = pd.read_csv(path, sep=',', header=0, usecols=cols, memory_map=True)

        try:
            df['date'] = datetime.strptime(path.stem.split('_')[0], '%Y-%m-%d')
        except ValueError:
            df['date'] = datetime.strptime(path.stem.split('.')[0], '%Y-%m-%d')
        return df

    df = load_tbl(paths[0])
    for p in paths[1:]:
        try:
            d = load_tbl(p)
            df = df.merge(d, how='outer')
        except Exception:
            continue

    df = df.set_index('date')
    df.to_csv(savepath)
    print(f'Saved: {savepath}')
    return df


def zipf(df, hashtbl, savepath):
    """ Plot a zipf distribution of twitter/fasttext lang. labels
    :param df: a dataframe of daily language usage on Twitter
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    :param savepath: path to save plot
    """
    df = df.loc['2014':'2020']

    df = pd.DataFrame(df.groupby(['language'])['tw_count', 'ft_count'].agg("sum"))

    df['tw_freq'] = df['tw_count'] / df['tw_count'].sum()
    df['ft_freq'] = df['ft_count'] / df['ft_count'].sum()

    df['tw_rank'] = df['tw_count'].rank(method='average', ascending=False)
    df['ft_rank'] = df['ft_count'].rank(method='average', ascending=False)

    df.rename(  # FastText codes
        index={code: f'{label}' for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )

    df.rename(  # Twitter codes
        index={code: f'{label}' for label, code in zip(hashtbl[:, 0], hashtbl[:, 2])},
        inplace=True
    )

    languages_vis.plot_zipf(df, savepath)
    print(f'Saved: {savepath}/tw_zipf')
    print(f'Saved: {savepath}/ft_zipf')


def divergence(df, savepath, hashtbl):
    """ Saves a divergence plot comparing twitter to fasttext lang. labels
    :param df: a dataframe of daily language usage on Twitter
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    :param savepath: path to save plot
    """
    df = df.loc['2014':'2020']

    df = pd.DataFrame(df.groupby(['language'])['tw_count', 'ft_count'].agg("sum"))

    df['tw_freq'] = df['tw_count'] / df['tw_count'].sum()
    df['ft_freq'] = df['ft_count'] / df['ft_count'].sum()

    df['tw_rank'] = df['tw_count'].rank(method='average', ascending=False)
    df['ft_rank'] = df['ft_count'].rank(method='average', ascending=False)

    df['divergence'] = (df.loc[:, 'tw_count'] - df.loc[:, 'ft_count']) / (df.loc[:, 'ft_count'] + df.loc[:, 'tw_count'])

    df = df.sort_values('ft_freq', ascending=False)
    df[['tw_freq', 'ft_freq']] = df[['tw_freq', 'ft_freq']].apply(np.log10)
    df[['tw_freq', 'ft_freq']] = df[['tw_freq', 'ft_freq']].replace([np.inf, -np.inf, np.nan], -9)

    languages_vis.plot_divergence(df, f'{savepath}/divergence')
    print(f'Saved: {savepath}/divergence')


def shift(df, savepath, hashtbl):
    """ Saves a shift plot comparing twitter to fasttext lang. labels
    :param df: a dataframe of daily language usage on Twitter
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    :param savepath: path to save plot
    """
    df = df.loc['2014':'2020']

    df = pd.DataFrame(df.groupby(['language'])['tw_count', 'ft_count'].agg("sum"))

    df['tw_freq'] = df['tw_count'] / df['tw_count'].sum()
    df['ft_freq'] = df['ft_count'] / df['ft_count'].sum()

    df['tw_rank'] = df['tw_count'].rank(method='average', ascending=False)
    df['ft_rank'] = df['ft_count'].rank(method='average', ascending=False)

    df.rename(  # FastText codes
        index={code: f'{label}' for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )

    df.rename(  # Twitter codes
        index={code: f'{label}' for label, code in zip(hashtbl[:, 0], hashtbl[:, 2])},
        inplace=True
    )

    languages_vis.plot_shifts(df, f'{savepath}/shifts')
    print(f'Saved: {savepath}/shifts')


def story(df, savepath, targets, hashtbl, resolution='W'):
    """ Plot a normalized timeline comparing the frequency of languages on Twitter
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param targets: a list of languages to plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    :param resolution: a time-scale to aggregate on (i.e. 'H', 'D', 'W', 'M', '6M', 'Y')
    """
    langs = pd.DataFrame(
        df.fillna(value=0).groupby(['date'])['tw_count', 'ft_count'].apply(
            lambda col: col.astype(bool).sum(axis=0)
        )
    )
    print(langs[langs.ft_count < 100])

    langs = langs.resample(resolution).mean()

    twlangs = df.loc[:, ['language', 'tw_count']]
    twlangs['language'] = twlangs.language.apply(lambda x: x if x in targets else 'other')
    twlangs = twlangs.groupby(['date', 'language']).sum().reset_index()
    twlangs.set_index('date', inplace=True)
    twlangs = twlangs.pivot(columns='language', values='tw_count').fillna(value=0)
    twlangs = twlangs.loc[:, targets]
    twlangs = twlangs.resample(resolution).mean()

    ftlangs = df.loc[:, ['language', 'ft_count']]
    ftlangs['language'] = ftlangs.language.apply(lambda x: x if x in targets else 'other')
    ftlangs = ftlangs.groupby(['date', 'language']).sum().reset_index()
    ftlangs.set_index('date', inplace=True)
    ftlangs = ftlangs.pivot(columns='language', values='ft_count').fillna(value=0)
    ftlangs = ftlangs.loc[:, targets]
    ftlangs = ftlangs.resample(resolution).mean()

    languages_vis.plot_story(
        f'{savepath}/story',
        langs,
        twlangs.loc[:, targets],
        ftlangs.loc[:, targets],
        hashtbl
    )
    print(f'Saved: {savepath}/story')


def timeline(df, savepath, hashtbl, resolution='W'):
    """ Plot a timeline comparing the frequency of languages on twitter
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param resolution: a time-scale to aggregate on (i.e. 'H', 'D', 'W', 'M', '6M', 'Y')
    """
    langs = {code: f'{label} ({code})' for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])}
    langs.update({code: f'{label} ({code})' for label, code in zip(hashtbl[:, 0], hashtbl[:, 2])})
    df.language.replace(langs, inplace=True)

    ftlangs = df.loc[:, ['language', 'ft_count']]
    ftlangs = ftlangs.pivot(columns='language', values='ft_count').fillna(value=0)
    ftlangs = ftlangs.resample(resolution).sum()

    twlangs = df.loc[:, ['language', 'tw_count']]
    twlangs = twlangs.pivot(columns='language', values='tw_count').fillna(value=0)
    twlangs = twlangs.resample(resolution).sum()

    languages_vis.plot_timeline(
        f'{savepath}/fasttext_twitter_timeseries.html',
        twlangs,
        ftlangs
    )
    print(f'Saved: {savepath}/fasttext_twitter_timeseries.html')


def rank(df, savepath, hashtbl, n):
    """ Plot time series of language usage on Twitter
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    :param n: number of languages to plot
    """
    df = df.loc['2009':'2019']
    tweets = df.loc[:, ['language', 'ft_count']]
    tweets = tweets.pivot(columns='language', values='ft_count').fillna(value=0)
    ranks = tweets.resample('Y').sum()
    ranks.index = ranks.index.strftime('%Y')
    last = ranks.index[-1]
    ranks = ranks.T.reset_index()

    for col in ranks.loc[:, ranks.columns != 'language']:
        ranks[col] = ranks[col].rank(ascending=False)
        ranks[col] = ranks[col].loc[ranks[col] <= n]

    ranks = ranks.dropna(thresh=2).fillna(n+1)
    ranks = ranks.sort_values(last)
    langs = ranks.language.to_list()

    ranks.language.replace(
        {code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )

    languages_vis.plot_rank(f'{savepath}/rank', ranks, langs, n)
    print(f'Saved: {savepath}/rank')


def compare(df, savepath, hashtbl, resolution='M'):
    """ Plot time series of language usage on Twitter
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    :param resolution: a time-scale to aggregate on (i.e. 'H', 'D', 'W', 'M', '6M', 'Y')
    """
    langs = {code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])}
    langs.update({code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 2])})
    df.language.replace(langs, inplace=True)

    loi = [
        'English', 'Japanese', 'Spanish',
        'Undefined', 'Portuguese', 'Arabic', 'Thai',
        'Korean', 'French', 'Turkish', 'Indonesian',
        'German', 'Italian', 'Chinese', 'Russian',
        'Finnish', 'Swedish', 'Hindi', 'Italian',
        'Unknown'
    ]

    ftweets = df.loc[:, ['language', 'ft_tweets']]
    ftweets = ftweets.pivot(columns='language', values='ft_tweets').fillna(value=0)
    ftweets = ftweets.resample(resolution).sum()
    ftweets['Overall'] = ftweets.sum(axis=1)
    ftweets = ftweets.loc[:, ['Overall']+loi]

    fretweets = df.loc[:, ['language', 'ft_retweets']]
    fretweets = fretweets.pivot(columns='language', values='ft_retweets').fillna(value=0)
    fretweets = fretweets.resample(resolution).sum()
    fretweets['Overall'] = fretweets.sum(axis=1)
    fretweets = fretweets.loc[:, ['Overall']+loi]

    ttweets = df.loc[:, ['language', 'tw_tweets']]
    ttweets = ttweets.pivot(columns='language', values='tw_tweets').fillna(value=0)
    ttweets = ttweets.resample(resolution).sum()
    ttweets['Overall'] = ttweets.sum(axis=1)
    ttweets = ttweets.loc[:, ['Overall']+loi]

    tretweets = df.loc[:, ['language', 'tw_retweets']]
    tretweets = tretweets.pivot(columns='language', values='tw_retweets').fillna(value=0)
    tretweets = tretweets.resample(resolution).sum()
    tretweets['Overall'] = tretweets.sum(axis=1)
    tretweets = tretweets.loc[:, ['Overall']+loi]

    languages_vis.plot_compare(
        f'{savepath}/compare',
        ftweets, fretweets,
        ttweets, tretweets
    )
    print(f'Saved: {savepath}/compare')


def summary(df, savepath, hashtbl, resolution='M'):
    """ Plot time series of language usage on Twitter
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    """
    df = df.loc['2009':'2020']
    ftlangs = df.loc[:, ['language', 'ft_count']]
    ftlangs['language'] = ftlangs.language.apply(lambda x: x if x in consts.defaultLangs else 'other')
    ftlangs = ftlangs.groupby(['date', 'language']).sum().reset_index()
    ftlangs.set_index('date', inplace=True)
    ftlangs = ftlangs.pivot(columns='language', values='ft_count').fillna(value=0)
    ftlangs = ftlangs.loc[:, consts.defaultLangs]
    ftlangs = ftlangs.resample(resolution).sum()

    langs = {code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])}
    langs.update({code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 2])})
    df.language.replace(langs, inplace=True)

    at = df.loc[:, ['language', 'ft_count']]
    at = at.pivot(columns='language', values='ft_count').fillna(value=0)
    at = at.resample(resolution).sum()
    ranks = at.resample('Y').sum()
    ranks.index = ranks.index.strftime('%Y')
    last = ranks.index[-1]
    ranks = ranks.T.reset_index()

    for col in ranks.loc[:, ranks.columns != 'language']:
        ranks[col] = ranks[col].rank(ascending=False)
        ranks[col] = ranks[col].loc[ranks[col] <= 15]

    ranks = ranks.dropna(thresh=2).fillna(16)
    ranks = ranks.sort_values(last)
    langs = ranks.language.to_list()
    langs = ['Overall'] + langs

    at['Overall'] = at.sum(axis=1)
    ftweets = df.loc[:, ['language', 'ft_tweets']]
    ftweets = ftweets.pivot(columns='language', values='ft_tweets').fillna(value=0)
    ftweets = ftweets.resample(resolution).sum()

    fcomments = df.loc[:, ['language', 'ft_comments']]
    fcomments = fcomments.pivot(columns='language', values='ft_comments').fillna(value=0)
    fcomments = fcomments.resample(resolution).sum()

    ot = ftweets + fcomments
    ot['Overall'] = ot.sum(axis=1)
    ot = ot.loc[:, langs].div(at.loc[:, langs])

    rt = df.loc[:, ['language', 'ft_retweets']]
    rt = rt.pivot(columns='language', values='ft_retweets').fillna(value=0)
    rt = rt.resample(resolution).sum()
    rt['Overall'] = rt.sum(axis=1)
    rt = rt.loc[:, langs].div(at.loc[:, langs])

    languages_vis.plot_summary(f'{savepath}/summary', ftlangs, ot, rt, langs, hashtbl)
    print(f'Saved: {savepath}/summary')


def ratio(df, savepath, hashtbl, resolution='W'):
    """ Plot time series of language usage on Twitter
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save generated plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    """
    langs = {code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])}
    langs.update({code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 2])})
    df.language.replace(langs, inplace=True)

    loi = [
        'English', 'Japanese', 'Spanish', 'Undefined', 'Portuguese',
        'Thai', 'Arabic', 'Korean', 'French', 'Indonesian',
        'Turkish', 'German', 'Chinese', 'Italian', 'Russian',
        'Tagalog', 'Hindi', 'Persian', 'Urdu', 'Polish',
        'Catalan', 'Dutch', 'Tamil', 'Greek', 'Swedish',
        'Serbian', 'Finnish', 'Cebuano', 'Ukrainian', 'Esperanto'
    ]
    all = df.loc[:, ['language', 'ft_count']]
    all = all.pivot(columns='language', values='ft_count').fillna(value=0)
    all = all.resample(resolution).sum()

    ftweets = df.loc[:, ['language', 'ft_tweets']]
    ftweets = ftweets.pivot(columns='language', values='ft_tweets').fillna(value=0)
    ftweets = ftweets.resample(resolution).sum()

    fcomments = df.loc[:, ['language', 'ft_comments']]
    fcomments = fcomments.pivot(columns='language', values='ft_comments').fillna(value=0)
    fcomments = fcomments.resample(resolution).sum()

    fretweets = df.loc[:, ['language', 'ft_retweets']]
    fretweets = fretweets.pivot(columns='language', values='ft_retweets').fillna(value=0)
    fretweets = fretweets.resample(resolution).sum()
    fretweets = fretweets.loc[:, loi].div(all.loc[:, loi])

    ffresh = ftweets + fcomments
    ffresh = ffresh.loc[:, loi].div(all.loc[:, loi])

    languages_vis.plot_ratio(
        f'{savepath}/ratio_lang_ts',
        ffresh, fretweets
    )
    print(f'Saved: {savepath}/ratio_lang_ts')

def contagiograms(
        savepath,
        lang_hashtbl,
        start_date=datetime(2020, 1, 1)
):
    """ Plot a grid of contagiograms
    :param savepath: path to save generated plot
    :param lang_hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    :param start_date: starting date for the query
    """

    loi = [
        "en", "ja", "und", "es",
        "pt", "ar", "th", "ko",
        "fr", "tr", "de", "it",
    ]

    languages = []
    for i, lang in enumerate(loi):
        print(f"Retrieving: {lang_hashtbl.get(lang)}")
        q = Query('languages', 'languages')
        d = q.query_languages(lang, start_time=start_date)

        print(f"Highest: {int(d['count'].max())} -- {d['count'].idxmax()}")
        print(f"Lowest: {int(d['count'].min())} -- {d['count'].idxmin()}")

        d.index.name = lang_hashtbl.get(lang)
        languages.append(d)

    languages_vis.plot_contagiograms(
        f'{savepath}/contagiograms',
        languages
    )
    print(f'Saved: {savepath}/contagiograms')


def stats(df, savepath, hashtbl):
    """ Plot a table of total number of messages captured by language
    :param df: a dataframe of daily language usage on Twitter
    :param savepath: path to save plot
    :param hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
    """
    df = df.loc['2008':'2019']
    df = pd.DataFrame(df.groupby(['language'])['ft_count'].agg("sum"))
    df = df[(df.T != 0).any()]
    df.sort_values(by=['ft_count'], ascending=False, inplace=True)

    df.rename(  # FastText codes
        index={code: f'{label} ({code})' for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )

    df.rename(  # Twitter codes
        index={code: f'{label} ({code})' for label, code in zip(hashtbl[:, 0], hashtbl[:, 2])},
        inplace=True
    )

    languages_vis.plot_stats(
        f'{savepath}/stats',
        df
    )
    print(f'Saved: {savepath}/stats')


def statsdf(lang='_all', start_date=datetime(2010, 1, 1)):
    """ Get ngram statistics overtime across all languages
    :param lang: target language
    :param start_date: starting date for the query
    :return: a dataframe of ngram stats
    """
    def printstats(s):
        stats = {}
        for c in s.columns:
            stats[c] = {
                'max': int(s[c].max()),
                'max_date': s[c].idxmax().date(),
                'min': int(s[c].min()),
                'min_date': s[c].idxmin().date()
            }
        print(pd.DataFrame.from_dict(stats, orient='index'))
        print('-'*65)

    q = Query('languages', 'languages')
    d = q.query_languages(lang, start_time=start_date)
    d.index = pd.to_datetime(d.index)

    print(r'$n$-grams statistics')
    for n in consts.ngrams:
        d[f'num_{n}_rt'] = d[f'num_{n}'] - d[f'num_{n}_no_rt']
        d[f'unique_{n}_rt'] = d[f'unique_{n}'] - d[f'unique_{n}_no_rt']

        s = d.filter(
            items=[
                f'num_{n}',
                f'unique_{n}',
                f'num_{n}_no_rt',
                f'unique_{n}_no_rt',
            ]
        )
        printstats(s)
        print(s.describe())
        print('-'*75)

    print('\nMessages statistics')
    s = d.filter(
        items=[
            f'count',
            f'ft_tweets',
            f'ft_comments',
            f'ft_retweets',
            f'ft_speakers',
        ]
    )
    printstats(s)
    print(s.describe())
    print('-'*75)

    return d
