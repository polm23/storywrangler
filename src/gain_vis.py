"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""

import joblib
from _plotly_future_ import v4_subplots
import matplotlib.pyplot as plt
import numpy as np
import plotly.graph_objs as go
import plotly.offline as py
import seaborn as sns
from pandas.plotting import register_matplotlib_converters
from plotly.subplots import make_subplots

register_matplotlib_converters()
from matplotlib.transforms import Affine2D
import matplotlib.dates as mdates
import matplotlib.colors as mcolors
import matplotlib.colorbar as colorbar
import mpl_toolkits.axisartist.floating_axes as floating_axes
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axisartist.grid_finder import FixedLocator, DictFormatter

import warnings
warnings.simplefilter("ignore")

import consts


def colormap(cmap=plt.cm.viridis, vmin=0, vmid=0.5, vmax=1.0):
    ''' Create a custom matplotlib cmap
    :param cmap: original cmap to use
    :param vmin: minimum value
    :param vmid: mid point
    :param vmax: maximum value
    :return: a matplotlib cmap
    '''
    cc = {'red': [], 'green': [], 'blue': [], 'alpha': []}
    idx = np.hstack([
        np.linspace(0.0, vmid, 128, endpoint=False),
        np.linspace(vmid, 1.0, 129, endpoint=True)
    ])

    for prev, new in zip(np.linspace(vmin, vmax, 257), idx):
        r, g, b, a = cmap(prev)
        cc['red'].append((new, r, r))
        cc['green'].append((new, g, g))
        cc['blue'].append((new, b, b))
        cc['alpha'].append((new, a, a))

    return mcolors.LinearSegmentedColormap('cmap', cc)


def plot_gain(savepath, counts):
    """ Plot gain of languages over time as a function of number of messages
    :param savepath: path to save plot
    :param counts: a dataframe of language counts
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })

    fig, axes = plt.subplots(figsize=(10, 10), nrows=3, ncols=2)

    years = np.arange(2009, 2020, 2)
    thresholds = dict(zip(years, [.5, 1.5, 1.9, 2.6, 3.6, 3.8]))

    for i, scale in enumerate(axes.flatten()):
        langs = counts.ft_count.loc[str(years[i])] \
            .groupby('language') \
            .sum()

        xx = langs.apply(np.log10) \
            .replace([np.inf, -1. * np.inf, np.nan], -1) \
            .values \
            .reshape(-1, 1)

        yy = counts.ft_gain.loc[str(years[i])] \
            .groupby('language') \
            .mean() \
            .values

        scale.scatter(
            xx, yy, c=yy,
            cmap='magma',
            vmin=0,
            vmax=10,
            marker='o',
            s=25
        )

        scale.set_xticks(range(0, 12, 2))

        for lang, cc, g in zip(langs.index, xx.reshape(-1, ), yy):
            if g >= thresholds[years[i]]:
                scale.annotate(lang, (cc - .15, g + .25), color='dimgrey', fontsize=10)

        scale.set_xlim(0, 10)
        scale.set_ylim(0, 10)
        scale.grid(True, which="both", axis='both', alpha=.2, lw=2, linestyle='--')

        scale.set_title(years[i])
        scale.set_xlabel(r'$\log_{10}$ Number of messages')
        scale.set_ylabel('$G_{messages}^{(c)}$')

    plt.tight_layout()
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_ratio(savepath, counts):
    """ Plot gain of languages over time as a function of number of messages
    :param savepath: path to save plot
    :param counts: a dataframe of language counts
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })

    fig, axes = plt.subplots(figsize=(10, 4), nrows=1, ncols=3)

    years = [2015, 2017, 2019]
    thresholds = dict(zip(years, [1, 1.65, 1.5]))

    for i, scale in enumerate(axes.flatten()):
        scale.axhline(1, ls='--', color='r', lw=2)

        langs = counts.ft_count.loc[str(years[i])] \
            .groupby('language') \
            .sum()

        xx = langs.apply(np.log10) \
            .replace([np.inf, -1. * np.inf, np.nan], -1) \
            .values \
            .reshape(-1, 1)

        yy = counts.ft_ratio.loc[str(years[i])] \
            .groupby('language') \
            .mean() \
            .values

        scale.scatter(
            xx, yy, c='k',
            marker='o',
            edgecolors='grey',
            s=25
        )

        scale.set_xticks(range(0, 12, 2))

        for lang, cc, g in zip(langs.index, xx.reshape(-1, ), yy):
            if g >= thresholds[years[i]]:
                scale.annotate(lang, (cc - .25, g + .25), color='dimgrey', fontsize=11)

        scale.set_xlim(0, 10)
        scale.set_ylim(0, 8)
        scale.grid(True, which="both", axis='both', alpha=.2, lw=2, linestyle='--')

        scale.set_title(years[i])
        scale.set_xlabel(r'$\log_{10}$ Number of messages')
        scale.set_ylabel(r'$R_{messages}^{(c)}$')

        scale.axhspan(1, 8, facecolor='red', alpha=0.1)

    plt.tight_layout()
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_gain_extra(savepath, counts, langs, heatmap, langts, ngrams, series):
    """ Plot top-n ranked languages over time
    :param savepath: path to save plot
    :param counts: a dataframe of language counts
    :param langs: a dataframe of language gain
    :param heatmap: a dataframe of languages to use for the gain heatmap
    :param langts: a dataframe of language gain timeseries
    :param ngrams: a dict of ngram sampled by year and language
    :param series: a dict of ngram dataframes
    :return: saves a figure to {savepath}
    """
    def rotate45(fig, pos, limits, left_label, right_label):
        """ Rotates a figure 45 degrees """
        transformation = Affine2D().rotate_deg(45)
        ticks = [
            (-1, r''),
            (0, r'$0$'),
            (1, r'$1$'),
            (2, r'$2$'),
            (3, r'$3$'),
        ]
        locator = FixedLocator([v for v, s in ticks])
        grid_helper = floating_axes.GridHelperCurveLinear(
            transformation,
            extremes=(limits[0], limits[1], limits[0], limits[1]),
            grid_locator1=locator,
            grid_locator2=locator,
            tick_formatter1=DictFormatter(dict(ticks)),
            tick_formatter2=DictFormatter(dict(ticks)),
        )
        ax = floating_axes.FloatingSubplot(fig, pos, grid_helper=grid_helper)
        ax = fig.add_subplot(ax)
        aux_ax = ax.get_aux_axes(transformation)

        ax.set_title('$G_{ngrams}^{(f)}$' + f'({lang})')

        bleft = ax.axis['left']
        bleft.label.set_text(left_label)
        bleft.label.set_rotation(360)
        bleft.major_ticklabels.set_rotation(45)
        bleft.major_ticklabels.set_pad(10)
        bleft.LABELPAD += 20
        bleft.major_ticks.set_tick_out(True)

        bright = ax.axis['bottom']
        bright.label.set_text(right_label)
        bright.major_ticklabels.set_rotation(-45)
        bright.major_ticklabels.set_pad(10)
        bright.LABELPAD += 10
        bright.major_ticks.set_tick_out(True)

        return aux_ax

    def dist(xx, yy, samples=10**6):
        """ Compute a 2D histogram """
        d1 = np.random.choice(xx, samples)
        d2 = np.random.choice(yy, samples)

        hist, xedges, yedges = np.histogram2d(
            d1, d2,
            bins=(
                np.logspace(-1, 3, 10),
                np.logspace(-1, 3, 10),
            ),
        )

        x, y = np.meshgrid(xedges, yedges)
        hist = np.log10(hist)
        hist = np.where(hist == -np.inf, np.min(hist[hist != -np.inf]), hist)
        return x, y, hist

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })
    callout_label_size = 20
    subplotlabel1 = (.05, .8)
    subplotlabel2 = (.03, .8)
    subplotlabel3 = (.1, .9)
    subplotlabel4 = (.1, .8)
    subplotlabel5 = (0.85, .125)

    fig = plt.figure(figsize=(15, 20), constrained_layout=True)
    gs = fig.add_gridspec(ncols=6, nrows=5)

    ts = fig.add_subplot(gs[0, :3])

    colors = ['royalblue', 'red', 'black']
    for i, (ngram, df) in enumerate(series.items()):
        ts.plot(df['gain'], label=ngram, color=colors[i], lw=2, alpha=.75)

    ts.set_ylim(-1, 1)
    ts.set_xlim('2009', '2020')
    ts.xaxis.set_major_locator(mdates.YearLocator())
    ts.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
    ts.xaxis.set_minor_locator(mdates.MonthLocator([4, 7, 10]))
    ts.set_xticklabels(langs.index, rotation=30, ha='center', rotation_mode='anchor')
    ts.tick_params(axis='x', which='major', pad=10)

    ts.set_ylabel('$G_{ngrams}^{(f)}$')
    ts.legend(ncol=len(colors), frameon=False, loc='upper center', bbox_to_anchor=(0.5, .25))
    ts.grid(True, which="both", axis='both', alpha=.3, lw=2, linestyle='--')

    ts.set_title(
       r'$G_{ngrams}^{(f)} = \log_{10} \left[ \dfrac{f(all~ngrams)}{f(organic~ngrams)} \right]$',
        pad=25
    )

    ts.annotate(
        "A", xy=subplotlabel1, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
    )

    loi = ['Indonesian', 'Thai', 'Korean', 'English', 'Arabic', 'Russian']
    styles = ['--', '-.', '-', ':', '--', '-']
    markers = ['', '>', 's', '^', 'o', 'd']
    colors = [
        consts.colors['id'],
        consts.colors['th'],
        consts.colors['ko'],
        consts.colors['en'],
        consts.colors['ar'],
        consts.colors['ru']
    ]
    gain = fig.add_subplot(gs[0, 3:])
    gain.set_title(
       r'$G_{messages}^{(c)} = \log_{10} \left[ \dfrac{c(all~messages)}{c(organic~messages)} \right]$',
        pad=25
    )

    for l, s, m, c in zip(loi, styles, markers, colors):
        gain.plot(langs[l].T, label=l, linewidth=3, linestyle=s, marker=m, color=c)

    gain.grid(True, which="both", axis='both', alpha=.2, lw=2, linestyle='--')
    gain.legend(ncol=2, frameon=False, loc='upper center', bbox_to_anchor=(0.4, .99))
    gain.set_xlim(langs.index[0], langs.index[-1])
    gain.set_xticklabels(langs.index, rotation=30, ha='center', rotation_mode='anchor')
    gain.tick_params(axis='x', which='major', pad=10)
    gain.set_ylabel('$G_{messages}^{(c)}$')

    gain.annotate(
        "B", xy=subplotlabel1, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
    )

    lang_grid = [('de', 'German'), ('en', 'English'), ('hi', 'Hindi')]
    labels = ['C', 'D', 'E', 'F', 'G', 'H']
    for i, (cc, lang) in enumerate(lang_grid):
        gain_prev = rotate45(
            fig, gs[i+1, :2],
            limits=(-1, 3),
            left_label='2009',
            right_label='2019'
        )

        x, y, hist = dist(
            ngrams[cc]['2009'],
            ngrams[cc]['2019']
        )

        gain_prev.annotate(
            labels[i], xy=subplotlabel2, color='k',
            xycoords="axes fraction", fontsize=callout_label_size,
            bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
        )

        hb = gain_prev.pcolormesh(np.log10(x), np.log10(y), hist, vmin=0, vmax=6, cmap='viridis')

        if i == 1:
            cbarax = inset_axes(
                gain_prev,
                width="10%",
                height="350%",
                bbox_to_anchor=(-1.25, 1.3, 1, 1),
                bbox_transform=gain_prev.transAxes,
                borderpad=.25,
            )

            plt.colorbar(hb, cax=cbarax, ticks=np.arange(0, 7, step=1))
            cbarax.tick_params(labelsize=12)
            cbarax.set_ylabel(r'Number of samples ($log_{10}$)')
            cbarax.yaxis.set_label_position("left")
            cbarax.yaxis.set_ticks_position('right')

        gain_next = rotate45(
            fig, gs[i+1, 2:4],
            limits=(-1, 3),
            left_label='2017',
            right_label='2019'
        )

        x, y, hist = dist(
            ngrams[cc]['2017'],
            ngrams[cc]['2019']
        )
        hb = gain_next.pcolormesh(np.log10(x), np.log10(y), hist, vmin=0, vmax=6, cmap='viridis')

        gain_next.annotate(
            labels[i+3], xy=subplotlabel2, color='k',
            xycoords="axes fraction", fontsize=callout_label_size,
            bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
        )

    messages = fig.add_subplot(gs[1, -2:])
    messages.semilogy(langts['ft_speakers'], label='Speakers', lw=2, color='hotpink')
    messages.semilogy(langts['ft_tweets'], label='Tweets', ls='--', marker='o', lw=2, markevery=12, color='dimgray')
    messages.semilogy(langts['ft_retweets'], label='Retweets', ls=':', marker='^', lw=2, markevery=12, color='seagreen')
    messages.set_ylabel('Number of messages')

    gmessages = messages.twinx()
    gmessages.plot(langts['ft_gain'], lw=2, color='r')
    gmessages.tick_params(axis='y', colors='r', color='r', labelcolor='r')
    gmessages.set_ylabel('Average $G_{messages}^{(c)}$', color='r')
    gmessages.spines['right'].set_color('r')
    gmessages.set_ylim(0, .5)

    messages.axvline(np.argmax(langts['ft_tweets']), color='k', lw=5)

    messages.legend(ncol=3, frameon=False, loc='upper center', bbox_to_anchor=(0.5, 1.2))
    messages.grid(True, which="both", alpha=.1, lw=1, linestyle='--')
    messages.set_xlim('2009', '2019')
    messages.set_ylim(10 ** 0, 10 ** 10)
    messages.xaxis.set_major_locator(mdates.YearLocator())
    messages.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
    messages.xaxis.set_minor_locator(mdates.MonthLocator([4, 7, 10]))
    plt.setp(messages.xaxis.get_majorticklabels(), ha='center', rotation=30, rotation_mode='anchor')
    messages.tick_params(axis='x', which='major', pad=10)
    messages.annotate(
        "I", xy=subplotlabel5, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
    )

    heatmapax = fig.add_subplot(gs[2:-1, -2:])
    cbarax = inset_axes(
        heatmapax,
        width="5%",
        height="158%",
        bbox_to_anchor=(.15, .01, 1, 1),
        bbox_transform=heatmapax.transAxes,
        borderpad=.25,
    )
    heatmapax = sns.heatmap(
        heatmap,
        ax=heatmapax,
        cbar_ax=cbarax,
        cbar=True,
        cmap='magma',
        vmin=0,
        vmax=1,
        linewidths=0.0001,
        linecolor='grey'
    )

    cbarax.tick_params(labelsize=12)
    cbarax.set_xlabel(r'$G_{messages}^{(c)}$', fontsize=16, ha='left', labelpad=10)
    cbarax.yaxis.set_label_position("left")
    cbarax.yaxis.set_ticks_position('right')

    heatmapax.set_xticklabels(heatmap.columns, rotation=30, ha='center', rotation_mode='anchor')
    heatmapax.tick_params(axis='x', which='major', pad=10)
    heatmapax.set_ylabel('')
    heatmapax.grid(False, which="both")
    heatmapax.annotate(
        "J", xy=subplotlabel3, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
    )

    years = ['2009', '2014', '2019']
    labels = ['K', 'L', 'M']
    for i, y in enumerate(years):
        scale = fig.add_subplot(gs[-1, i*2:(i*2)+2])
        yy = langs.loc[y]
        yy = yy.replace([np.inf, -np.inf, np.nan], 1)
        xx = counts.loc[y]

        scale.scatter(xx, yy, c=yy, cmap='magma', vmin=0, vmax=1, marker='o', alpha=.25, s=150)
        scale.scatter(xx, yy, c=yy, cmap='magma', vmin=0, vmax=1, marker='o', s=50)

        scale.set_xlim(10**0, 10**10)
        scale.set_ylim(-.1, 1)
        scale.set_xscale('log')
        scale.set_xlabel(f'Number of messages\n({y})')
        scale.set_ylabel('$G_{messages}^{(c)}$')
        scale.grid(True, which="both", axis='both', alpha=.2, lw=2, linestyle='--')

        scale.annotate(
            labels[i], xy=subplotlabel4, color='k',
            xycoords="axes fraction", fontsize=callout_label_size,
            bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
        )

    plt.tight_layout()
    fig.subplots_adjust(wspace=0.4, left=0.01, right=0.99, top=.95, bottom=.05)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_lang_ratio(savepath, counts, langs, heatmap, langts):
    """ Plot top-n ranked languages over time
    :param savepath: path to save plot
    :param counts: a dataframe of language counts
    :param langs: a dataframe of language ratios
    :param heatmap: a dataframe of languages to use for the ratio heatmap
    :param langts: a dataframe of language ratio timeseries
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })
    callout_label_size = 20
    subplotlabel1 = (.03, .85)
    subplotlabel2 = (.07, .85)
    subplotlabel3 = (1.2, .9)
    subplotlabel4 = (1.15, -.05)

    colormap_vmin = 0
    colormap_vmax = 8
    cmap = colormap(
        plt.cm.coolwarm,
        vmin=colormap_vmin,
        vmid=1,
        vmax=colormap_vmax,
    )
    cmap = 'magma'

    fig = plt.figure(figsize=(14, 18), constrained_layout=True)
    gs = fig.add_gridspec(ncols=3, nrows=4)

    loi = ['Indonesian', 'Thai', 'Hindi', 'Korean', 'English', 'Arabic']
    styles = ['--', '-.', '-', ':', '--', '-']
    markers = ['', '>', 's', '^', 'o', 'd']
    colors = [
        consts.colors['id'],
        consts.colors['th'],
        consts.colors['hi'],
        consts.colors['ko'],
        consts.colors['en'],
        consts.colors['ar'],
        consts.colors['ru']
    ]
    ratio = fig.add_subplot(gs[0, :-1])

    for l, s, m, c in zip(loi, styles, markers, colors):
        ratio.plot(langs[l].T, label=l, linewidth=3, linestyle=s, marker=m, color=c)

    ratio.grid(True, which="both", axis='both', alpha=.2, lw=2, linestyle='--')
    ratio.legend(ncol=2, frameon=False, loc='upper center', bbox_to_anchor=(0.4, .99))
    ratio.set_xlim(langs.index[0], langs.index[-1])
    ratio.set_ylim(colormap_vmin - .1, colormap_vmax)
    ratio.tick_params(axis='x', which='major', pad=10)
    ratio.set_ylabel('Average $R_{messages}^{(c)}$')

    ratio.annotate(
        "A", xy=subplotlabel1, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
    )

    years = np.arange(2009, 2020, 2)
    labels = ['C', 'D', 'E', 'F', 'G', 'H']
    thresholds = dict(zip(years, [.5, .6, .7, 1, 1.6, 2]))

    def scale(scale, y, label):
        langs = counts.ft_count.loc[y] \
            .groupby('language') \
            .sum()

        xx = langs.apply(np.log10) \
            .replace([np.inf, -1. * np.inf, np.nan], -1) \
            .values \
            .reshape(-1, 1)

        yy = counts.ft_ratio.loc[y] \
            .groupby('language') \
            .mean() \
            .values

        scale.scatter(
            xx, yy, c=yy,
            cmap=cmap,
            vmin=colormap_vmin,
            vmax=colormap_vmax,
            marker='o',
            edgecolors='grey',
            s=25
        )

        for lang, cc, g in zip(langs.index, xx.reshape(-1, ), yy):
            if g >= thresholds[int(y)]:
                ax.annotate(lang, (cc - .35, g + .35), color='dimgrey', fontsize=10)

        scale.axhline(1, color='k', lw=2)

        scale.set_xlim(0, 10)
        scale.set_ylim(colormap_vmin, colormap_vmax)
        scale.grid(True, which="both", axis='both', alpha=.2, lw=2, linestyle='--')

        scale.set_title(f'~ {y} ~', y=.85)
        scale.annotate(
            label, xy=subplotlabel2, color='k',
            xycoords="axes fraction", fontsize=callout_label_size,
            bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
        )

    for i, j in enumerate(range(0, len(labels), 2)):
        ax = fig.add_subplot(gs[i + 1, 0])
        scale(ax, str(years[j]), labels[j])
        ax.set_ylabel('$R_{messages}^{(c)}$')

        if i == 2:
            ax.set_xlabel(r'$\log_{10}$ Number of messages')

        ax = fig.add_subplot(gs[i + 1, 1])
        scale(ax, str(years[j + 1]), labels[j + 1])

        if i == 2:
            ax.set_xlabel(r'$\log_{10}$ Number of messages')

    messages = fig.add_subplot(gs[0, -1])
    messages.plot(langts['ft_fresh'], label='Organic', ls='-', marker='o', lw=2, markevery=1, color='dimgray')
    messages.plot(langts['ft_speakers'], label='Users', ls=':', marker='s', lw=2, markevery=1, color='hotpink')
    messages.plot(langts['ft_retweets'], label='Retweets', ls='--', marker='^', lw=2, markevery=1, color='seagreen')

    gmessages = messages.twinx()
    gmessages.plot(langts['ft_ratio'], lw=2, color='r')
    gmessages.tick_params(axis='y', colors='r', color='r', labelcolor='r')
    gmessages.set_ylabel('$R_{messages}^{(c)}$', color='r')
    gmessages.spines['right'].set_color('r')
    gmessages.set_ylim(colormap_vmin, colormap_vmax / 2)

    messages.axvline(np.argmax(langts['ft_count']), color='k', lw=2)
    messages.axvspan(langts['ft_count'].index[0], np.argmax(langts['ft_count']), facecolor='grey', alpha=0.1)

    messages.legend(ncol=1, frameon=False, loc='upper right', fontsize=10)
    messages.grid(True, which="both", alpha=.1, lw=1, linestyle='--')
    messages.set_xlim('2010', '2019')
    messages.xaxis.set_major_locator(mdates.YearLocator())
    messages.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
    plt.setp(messages.xaxis.get_majorticklabels(), ha='center', rotation=45, rotation_mode='anchor')
    messages.tick_params(axis='x', which='major', pad=15)
    messages.annotate(
        "B", xy=subplotlabel3, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
    )

    heatmapax = fig.add_subplot(gs[1:, -1])
    cbarax = inset_axes(
        heatmapax,
        width="340%",
        height="3%",
        bbox_to_anchor=(0.01, -1.06, 1, 1),
        bbox_transform=heatmapax.transAxes,
        borderpad=.25,
    )
    heatmapax = sns.heatmap(
        heatmap,
        ax=heatmapax,
        cbar_ax=cbarax,
        cbar_kws={"orientation": "horizontal"},
        cbar=True,
        cmap=cmap,
        vmin=colormap_vmin,
        vmax=colormap_vmax,
        linewidths=0.0001,
        linecolor='grey'
    )

    cbarax.tick_params(labelsize=12)
    #cbarax.set_ylabel(r'$R_{messages}^{(c)}$', fontsize=16, ha='left', labelpad=30, rotation=0, rotation_mode='anchor')
    cbarax.yaxis.set_label_position("left")
    cbarax.yaxis.set_ticks_position('right')
    cbarax.set_xlabel(
        r'$R_{messages}^{(c)} = Count(retweeted~messages) ~/~ Count(organic~messages)$',
        labelpad=5
    )

    heatmapax.set_xticklabels(heatmap.columns, rotation=45, ha='center', rotation_mode='anchor')
    heatmapax.tick_params(axis='x', which='major', pad=15)
    heatmapax.set_ylabel('')
    heatmapax.yaxis.tick_right()
    heatmapax.set_yticklabels(heatmap.index, rotation=0, ha='left', rotation_mode='anchor')
    heatmapax.grid(False, which="both")
    heatmapax.annotate(
        "I", xy=subplotlabel4, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
    )

    plt.tight_layout()
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_lang_gain(savepath, counts, langs, heatmap, langts, models):
    """ Plot top-n ranked languages over time
    :param savepath: path to save plot
    :param counts: a dataframe of language counts
    :param langs: a dataframe of language gain
    :param heatmap: a dataframe of languages to use for the gain heatmap
    :param langts: a dataframe of language gain timeseries
    :param models: bayesian-fit models for gain and count
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })
    callout_label_size = 20
    subplotlabel1 = (.03, .85)
    subplotlabel2 = (.07, .85)
    subplotlabel3 = (1.2, .9)
    subplotlabel4 = (1.15, -.05)

    colormap_vmin = 0
    colormap_vmax = 10

    fig = plt.figure(figsize=(14, 18), constrained_layout=True)
    gs = fig.add_gridspec(ncols=3, nrows=4)

    loi = ['Indonesian', 'Thai', 'Hindi', 'Korean', 'English', 'Arabic']
    styles = ['--', '-.', '-', ':', '--', '-']
    markers = ['', '>', 's', '^', 'o', 'd']
    colors = [
        consts.colors['id'],
        consts.colors['th'],
        consts.colors['hi'],
        consts.colors['ko'],
        consts.colors['en'],
        consts.colors['ar'],
        consts.colors['ru']
    ]
    gain = fig.add_subplot(gs[0, :-1])

    for l, s, m, c in zip(loi, styles, markers, colors):
        gain.plot(langs[l].T, label=l, linewidth=3, linestyle=s, marker=m, color=c)

    gain.grid(True, which="both", axis='both', alpha=.2, lw=2, linestyle='--')
    gain.legend(ncol=2, frameon=False, loc='upper center', bbox_to_anchor=(0.4, .99))
    gain.set_xlim(langs.index[0], langs.index[-1])
    gain.set_ylim(colormap_vmin-.1, colormap_vmax)
    gain.tick_params(axis='x', which='major', pad=10)
    gain.set_ylabel('Average $G_{messages}^{(c)}$')

    gain.annotate(
        "A", xy=subplotlabel1, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
    )

    years = np.arange(2009, 2020, 2)
    labels = ['C', 'D', 'E', 'F', 'G', 'H']
    thresholds = dict(zip(years, [.5, 2, 2, 3, 4, 4]))

    def scale(scale, y, label):
        labels = counts.ft_count.loc[y]\
            .groupby('language')\
            .sum()

        xx = labels.apply(np.log10)\
            .replace([np.inf, -1. * np.inf, np.nan], -1)\
            .values\
            .reshape(-1, 1)

        yy = counts.ft_gain.loc[y]\
            .groupby('language')\
            .mean()\
            .values

        scale.scatter(
            xx, yy, c=yy,
            cmap='magma',
            vmin=colormap_vmin,
            vmax=colormap_vmax,
            marker='o',
            edgecolors='grey',
            s=25
        )

        for lang, cc, g in zip(labels.index, xx.reshape(-1,), yy):
            if g >= thresholds[int(y)]:
                ax.annotate(lang, (cc-.2, g+.2), color='dimgrey', fontsize=10)

        scale.set_xlim(0, 10)
        scale.set_ylim(colormap_vmin, colormap_vmax)
        scale.grid(True, which="both", axis='both', alpha=.2, lw=2, linestyle='--')

        plot_vals = np.linspace(min(xx), max(xx), 100)
        laplace_results = joblib.load(models/f'bayesian-linear-logcount-gain-{y}.gz')
        l_prior, l_trace, l_ppc = laplace_results
        inds = np.random.choice(l_trace['beta'].shape[0], 100, replace=True)
        l_models = np.empty((inds.shape[0], plot_vals.shape[0]))

        for i, ind in enumerate(inds):
            l_models[i] = np.dot(
                np.column_stack([np.ones_like(plot_vals), plot_vals]),
                l_trace['beta'][ind]
            )

        # demonstrate variability in fits
        c = 'crimson'
        percentiles = [25, 50, 75]
        pct_l = np.percentile(l_models, percentiles, axis=0)
        scale.plot(plot_vals.reshape(-1,), pct_l[1], c)
        scale.fill_between(plot_vals.reshape(-1), pct_l[0], y2=pct_l[-1], color=c, alpha=0.5)

        # demonstrate the full posterior predictive region now
        l_ppc_pct = np.percentile(l_ppc['obs'], percentiles, axis=0)
        ax.fill_between(
            np.sort(xx.reshape(-1,)),
            np.sort(l_ppc_pct[0]),
            y2=np.sort(l_ppc_pct[-1]),
            color=c,
            alpha=0.3
        )

        scale.set_title(f'~ {y} ~', y=.85)
        scale.annotate(
            label, xy=subplotlabel2, color='k',
            xycoords="axes fraction", fontsize=callout_label_size,
            bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
        )

        l_mean_b1 = np.mean(l_trace['beta'][:, -1])
        ax.text(
            0.05, 0.65,
            f'$E[\\beta_1^{{(\ell)}}] = $ {round(l_mean_b1, 3)}',
            transform=ax.transAxes,
            fontsize=14,
            color='crimson'
        )

    for i, j in enumerate(range(0, len(labels), 2)):
        ax = fig.add_subplot(gs[i + 1, 0])
        scale(ax, str(years[j]), labels[j])
        ax.set_ylabel('$G_{messages}^{(c)}$')

        if i == 2:
            ax.set_xlabel(r'$\log_{10}$ Number of messages')

        ax = fig.add_subplot(gs[i + 1, 1])
        scale(ax, str(years[j+1]), labels[j+1])

        if i == 2:
            ax.set_xlabel(r'$\log_{10}$ Number of messages')

    messages = fig.add_subplot(gs[0, -1])
    messages.plot(langts['ft_fresh'], label='Organic', ls='-', marker='o', lw=2, markevery=1, color='dimgray')
    messages.plot(langts['ft_speakers'], label='Users', ls=':', marker='s', lw=2, markevery=1, color='hotpink')
    messages.plot(langts['ft_retweets'], label='Retweets', ls='--', marker='^', lw=2, markevery=1, color='seagreen')

    gmessages = messages.twinx()
    gmessages.plot(langts['ft_gain'], lw=2, color='r')
    gmessages.tick_params(axis='y', colors='r', color='r', labelcolor='r')
    gmessages.set_ylabel('$G_{messages}^{(c)}$', color='r')
    gmessages.spines['right'].set_color('r')
    gmessages.set_ylim(colormap_vmin, colormap_vmax/2)

    messages.axvline(np.argmax(langts['ft_count']), color='k', lw=2)
    messages.axvspan(langts['ft_count'].index[0], np.argmax(langts['ft_count']), facecolor='grey', alpha=0.1)

    messages.legend(ncol=1, frameon=False, loc='lower right')
    messages.grid(True, which="both", alpha=.1, lw=1, linestyle='--')
    messages.set_xlim('2010', '2019')
    messages.xaxis.set_major_locator(mdates.YearLocator())
    messages.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
    plt.setp(messages.xaxis.get_majorticklabels(), ha='center', rotation=45, rotation_mode='anchor')
    messages.tick_params(axis='x', which='major', pad=15)
    messages.annotate(
        "B", xy=subplotlabel3, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
    )

    heatmapax = fig.add_subplot(gs[1:, -1])
    cbarax = inset_axes(
        heatmapax,
        width="340%",
        height="3%",
        bbox_to_anchor=(0.01, -1.06, 1, 1),
        bbox_transform=heatmapax.transAxes,
        borderpad=.25,
    )
    heatmapax = sns.heatmap(
        heatmap,
        ax=heatmapax,
        cbar_ax=cbarax,
        cbar_kws={"orientation": "horizontal"},
        cbar=True,
        cmap='magma',
        vmin=colormap_vmin,
        vmax=colormap_vmax,
        linewidths=0.0001,
        linecolor='grey'
    )

    cbarax.tick_params(labelsize=12)
    #cbarax.set_ylabel(r'$G_{messages}^{(c)}$', fontsize=16, ha='left', labelpad=30, rotation=0, rotation_mode='anchor')
    cbarax.yaxis.set_label_position("left")
    cbarax.yaxis.set_ticks_position('right')
    cbarax.set_xlabel(
       r'$G_{messages}^{(c)} = 10 \cdot \log_{10} \left[ Count(all~messages) ~/~ Count(organic~messages) \right]$',
        labelpad=5
    )

    heatmapax.set_xticklabels(heatmap.columns, rotation=45, ha='center', rotation_mode='anchor')
    heatmapax.tick_params(axis='x', which='major', pad=15)
    heatmapax.set_ylabel('')
    heatmapax.yaxis.tick_right()
    heatmapax.set_yticklabels(heatmap.index, rotation=0, ha='left', rotation_mode='anchor')
    heatmapax.grid(False, which="both")
    heatmapax.annotate(
        "I", xy=subplotlabel4, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
    )

    plt.tight_layout()
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_ngrams_gain(savepath, ngrams, series):
    """ Plot top-n ranked languages over time
    :param savepath: path to save plot
    :param ngrams: a dict of ngram sampled by year and language
    :param series: a dict of ngram dataframes
    :return: saves a figure to {savepath}
    """
    vmin, vmax = (0, 6)

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })

    callout_label_size = 18
    subplotlabel1 = (.03, .8)
    subplotlabel2 = (.8, .8)
    subplotlabel3 = (.6, .8)

    fig = plt.figure(figsize=(8, 12), constrained_layout=True)
    gs = fig.add_gridspec(ncols=6, nrows=4)

    ts = fig.add_subplot(gs[0, :])
    gain_vmin, gain_vmax = 0, 30

    colors = ['royalblue', 'red', 'black']
    cmaps = ['Blues', 'Reds', 'Greys']
    for i, (ngram, df) in enumerate(series.items()):
        ts.scatter(df.index, df['gain'], c=df['gain'], cmap=cmaps[i], vmin=-10, vmax=10, marker='o', alpha=.4, s=25)
        ts.scatter(df.index, df['gain'], c=df['gain'], cmap=cmaps[i], vmin=-10, vmax=10, marker='o', s=5)
        ts.plot(df['gain'].rolling(7).mean(), label=ngram, color=colors[i], lw=2, alpha=.75)

    ts.xaxis.set_major_locator(mdates.YearLocator())
    ts.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
    ts.xaxis.set_minor_locator(mdates.MonthLocator([4, 7, 10]))
    ts.tick_params(axis='x', which='major', pad=10)

    ts.set_xlim('2009', '2019')
    ts.set_ylabel('$f_{all} ~ / ~ f_{organic}$')
    ts.legend(ncol=len(colors), frameon=False, loc='upper center', bbox_to_anchor=(0.5, .95))
    ts.grid(True, which="both", axis='both', alpha=.3, lw=2, linestyle='--')

    ts.annotate(
        "A", xy=subplotlabel1, color='k',
        xycoords="axes fraction", fontsize=callout_label_size,
        bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
    )

    lang_grid = [
        [('en', 'English'), ('hi', 'Hindi')],
        [('de', 'German'), ('ko', 'Korean')],
    ]
    labels = [
        [('B', 'C'), ('D', 'E')],
        [('F', 'G'), ('H', 'I')],
    ]

    for i in range(2):
        for j in range(2):
            cc, lang = lang_grid[i][j]
            col = j*3
            scale = fig.add_subplot(gs[i+1, col:col+2])

            inds = np.random.choice(len(ngrams[cc]['2019']['gain']), size=10**vmax)
            yy = np.array(ngrams[cc]['2019']['gain'])[inds]
            xx = np.array(ngrams[cc]['2019']['count'])[inds]
            scale.scatter(xx, yy, c=yy, cmap='Oranges', vmin=-30, vmax=30, marker='o', s=25)

            inds = np.random.choice(len(ngrams[cc]['2013']['gain']), size=10**vmax)
            yy = np.array(ngrams[cc]['2013']['gain'])[inds]
            xx = np.array(ngrams[cc]['2013']['count'])[inds]
            scale.scatter(xx, yy, c=yy, cmap='Blues', vmin=-gain_vmax, vmax=gain_vmax, marker='o', s=25)

            if j == 0:
                scale.set_ylabel('$G_{ngrams}^{(c)}$')
            else:
                scale.set_yticklabels([])

            if i == 1:
                scale.set_xlabel('Number of observations')
            else:
                scale.set_xticklabels([])

            scale.set_xscale('log')
            scale.set_xlim(10**0, 10**(vmax+2))
            scale.set_ylim(gain_vmin, gain_vmax)
            scale.set_title('$G_{ngrams}^{(c)}$' + f'({lang})')
            scale.grid(True, which="both", axis='both', alpha=.3, lw=2, linestyle='--')

            scale.annotate(
                labels[i][j][0], xy=subplotlabel2, color='k',
                xycoords="axes fraction", fontsize=callout_label_size,
                bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
            )

            hist = fig.add_subplot(gs[i+1, col+2], yticklabels=[], xticklabels=[])

            hist.hist(
                ngrams[cc]['2019']['gain'],
                bins=np.linspace(gain_vmin, gain_vmax, 100),
                histtype='stepfilled',
                orientation='horizontal',
                alpha=.75,
                color='C1',
                label='2019'
            )
            hist.hist(
                ngrams[cc]['2013']['gain'],
                bins=np.linspace(gain_vmin, gain_vmax, 100),
                histtype='stepfilled',
                orientation='horizontal',
                alpha=.8,
                color='C0',
                label='2013'
            )

            hist.set_ylim(gain_vmin, gain_vmax)
            hist.set_xscale('log')
            hist.set_xticks([])
            hist.spines['right'].set_visible(False)
            hist.spines['top'].set_visible(False)
            hist.spines['bottom'].set_visible(False)

            if i == 1 and j == 1:
                hist.legend(ncol=1, frameon=False, loc='upper center', bbox_to_anchor=(0.5, 1.35))

            hist.annotate(
                labels[i][j][1], xy=subplotlabel3, color='k',
                xycoords="axes fraction", fontsize=callout_label_size,
                bbox=dict(facecolor='whitesmoke', edgecolor='black', pad=8),
            )

    plt.tight_layout()
    fig.subplots_adjust(wspace=0.2, left=0.01, right=0.99, top=.95, bottom=.05)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_contagion_timeline(savepath, languages, gain):
    """ Produce a timeline comparing the counts of languages on twitter
    :param savepath: path to save generated plot
    :param languages: a dataframe of number of messages per language
    :param gain: a dataframe of gain values per language
    :return: saves a figure to {savepath}
    """
    cols = 3
    langs = sorted(languages.columns.values)
    print(langs)
    rows = len(langs) // cols
    fig = make_subplots(
        rows=rows, cols=cols,
        subplot_titles=langs,
        specs=[[{"secondary_y": True} for c in range(cols)] for r in range(rows)]
    )

    i = 0
    for r in range(1, rows+1):
        for c in range(1, cols+1):
            fig.add_trace(
                go.Scatter(
                    x=languages.index,
                    y=languages.iloc[:, i],
                    name="Messages",
                    mode='lines',
                    fill='tozeroy',
                    line=dict(color='dimgrey'),
                    xaxis=f'x{i + 1}',
                    yaxis=f'y{i + 1}',
                ),
                row=r, col=c,
                secondary_y=False,
            )
            fig.add_trace(
                go.Scatter(
                    x=gain.index,
                    y=gain.iloc[:, i],
                    mode='lines',
                    name="Ratio",
                    #name="Gain",
                    line=dict(color='orangered'),
                    xaxis=f'x{i + 1}',
                    yaxis=f'y{i + 1}',
                ),
                row=r, col=c,
                secondary_y=True,
            )

            fig.update_xaxes(
                row=r,
                col=c,
                range=['2009-01-01', '2020-01-01'],
                tickformat='%b<br>%Y',
            )

            fig.update_yaxes(
                row=r,
                col=c,
                range=[0, 8],
                type="log",
                secondary_y=False,
                tickfont=dict(
                    color="dimgrey"
                ),
            )

            fig.update_yaxes(
                row=r,
                col=c,
                range=[0, 16],
                tickmode='array',
                tickvals=np.arange(0, 20, step=4),
                secondary_y=True,
                tickfont=dict(
                    color="orangered"
                ),
            )

            i += 1

    fig.layout.update(dict(
        height=10000,
        title=r"$\text{Weekly Contagion Ratios } ~ \left( R_{messages}^{(c)} \right) "
              r"= Count(RT) ~/~ Count(OT)$",
        #title=r"$\text{Weekly Contagion Gains } ~ \left( G_{messages}^{(c)} \right) "
        #      r"= 10 \cdot \log_{10} \left[ Count(AT) ~/~ Count(OT) \right]$",
        font=dict(size=14),
        hovermode='x',
        showlegend=False,
    ))

    py.plot(fig, filename=savepath, include_mathjax='cdn')


def plot_diff(savepath, tweets, retweets):
    """ Compare growth of retweets among languages
    :param savepath: path to save generated plot
    :param tweets: a dataframe of organic messages
    :param retweets: a dataframe of retweeted messages
    :return: saves a figure to {savepath}
    """
    cols = 3
    langs = sorted(map(str, set(tweets.columns) | set(retweets.columns)))
    print(langs)
    rows = len(langs) // cols
    fig = make_subplots(
        rows=rows, cols=cols,
        subplot_titles=langs,
    )

    i = 0
    shapes = []
    for r in range(1, rows+1):
        for c in range(1, cols+1):
            idx = np.argwhere((retweets.iloc[:, i] - tweets.iloc[:, i]) > 0).flatten()
            if len(idx) > 0:
                for d in retweets.iloc[idx, i].index:
                    shapes.append(go.layout.Shape(
                        type="line",
                        layer="below",
                        x0=d,
                        x1=d,
                        y0=0,
                        y1=1,
                        xref=f'x{i + 1}',
                        yref=f'y{i + 1}',
                        opacity=.25,
                        line=dict(
                            color="gray",
                            width=1,
                        )
                    ))

            fig.add_trace(
                go.Scatter(
                    x=tweets.index,
                    y=tweets.iloc[:, i],
                    name='OT',
                    mode='lines',
                    line=dict(color='royalblue'),
                    xaxis=f'x{i+1}',
                    yaxis=f'y{i+1}',
                ),
                row=r, col=c
            )
            fig.add_trace(
                go.Scatter(
                    x=retweets.index,
                    y=retweets.iloc[:, i],
                    name='RT',
                    mode='lines',
                    line=dict(color='darkorange', dash='dash'),
                    xaxis=f'x{i+1}',
                    yaxis=f'y{i+1}',
                ),
                row=r, col=c
            )

            fig.update_xaxes(
                row=r,
                col=c,
                range=['2009-01-01', '2020-01-01'],
                tickformat='%b<br>%Y',
            )

            fig.update_yaxes(range=[0, 1], row=r, col=c)

            i += 1

    fig.layout.update(dict(
        height=10000,
        title='Weekly relative rate of usage',
        font=dict(size=14),
        hovermode='x',
        showlegend=False,
        shapes=shapes
    ))

    py.plot(fig, filename=savepath)


def plot_prediction(savepath, counts, models, pred_vals, pred_regs, gen_lc_years, pred_params):
    plt.rcParams.update({
        'font.size': 11,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })

    percentiles = [25, 50, 75]

    figshape = (4, 3)
    fig, axes = plt.subplots(*figshape, figsize=(11, 11))
    axes = axes.flatten()
    years = np.arange(2009, 2020)

    for year, ax in zip(years, axes):
        year = str(year)
        lc_year = counts.ft_count.loc[year] \
            .groupby('language') \
            .sum() \
            .apply(np.log10) \
            .replace([np.inf, -1. * np.inf, np.nan], 1.) \
            .values \
            .reshape(-1, 1)

        r_year = counts.ft_gain.loc[year] \
            .groupby('language') \
            .mean() \
            .values

        plot_vals = np.linspace(min(lc_year), max(lc_year), 100)

        laplace_results = joblib.load(models/f'bayesian-linear-logcount-ratio-{year}.gz')
        l_prior, l_trace, l_ppc = laplace_results

        ax.scatter(lc_year, r_year, c='k', s=25)

        # now demonstrate the linear fits
        # make and plot some random models
        inds = np.random.choice(l_trace['beta'].shape[0], 100, replace=True)
        l_models = np.empty(
            (inds.shape[0], plot_vals.shape[0])
        )

        for i, ind in enumerate(inds):
            l_models[i] = np.dot(
                np.column_stack([np.ones_like(plot_vals), plot_vals]),
                l_trace['beta'][ind]
            )

        # demonstrate variability in fits
        pct_l = np.percentile(l_models, percentiles, axis=0)

        ax.plot(plot_vals, pct_l[1], 'crimson')
        ax.fill_between(plot_vals.reshape(-1, ), pct_l[0], y2=pct_l[-1], color='crimson', alpha=0.25)

        # demonstrate the full posterior predictive region now
        l_ppc_pct = np.percentile(l_ppc['obs'], percentiles, axis=0)
        ax.fill_between(
            np.sort(lc_year.reshape(-1, )),
            np.sort(l_ppc_pct[0]),
            y2=np.sort(l_ppc_pct[-1]),
            color='crimson',
            alpha=0.2
        )

        ax.set_title(year)
        ax.set_ylim(0, 1)
        ax.set_xlim(2, 10)

        l_mean_b1 = np.mean(l_trace['beta'][:, -1])
        ax.text(
            0.05, 0.9,
            f'$E[\\beta_1] = $ {round(l_mean_b1, 3)}',
            transform=ax.transAxes,
            color='crimson'
        )

    pred_val_pcts = np.percentile(pred_vals, percentiles, axis=0)
    pred_reg_pcts = np.percentile(pred_regs, percentiles, axis=0)
    lc_year_pct = np.percentile(gen_lc_years, 50, axis=0)

    # plot the predicted regression lines
    axes[-1].plot(lc_year_pct, pred_reg_pcts[1], 'crimson')
    axes[-1].fill_between(
        lc_year_pct,
        pred_reg_pcts[0],
        y2=pred_reg_pcts[-1],
        color='crimson',
        alpha=0.25
    )

    axes[-1].fill_between(
        lc_year_pct,
        np.sort(pred_val_pcts[0]),
        y2=np.sort(pred_val_pcts[-1]),
        color='crimson',
        alpha=0.2
    )

    # final cleaning
    axes[-1].set_title('2020 (predicted)')
    axes[-1].set_ylim(0, 1)
    axes[-1].set_xlim(2, 10)

    axes[-1].text(
        0.05, 0.9,
        f'$E[\\beta_1] = $ {round(np.mean(pred_params[:, :, -2], axis=1)[0], 3)}',
        transform=axes[-1].transAxes,
        color='crimson'
    )

    for ax in axes:
        ax.set_xlabel(r'$\log_{10}$ Number of messages')
        ax.set_ylabel(r'$R_{messages}^{(c)}$')
        ax.grid(True, which="both", axis='both', alpha=.2, lw=2, linestyle='--')

    plt.tight_layout()
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_ratio_timeseries(savepath, langts):
    """ Plot a time series of ratio
    :param savepath: path to save generated plot
    :param langts: dataframe of the ratio time series
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'font.size': 12,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })
    fig, messages = plt.subplots(figsize=(12, 8))
    gmessages = messages.twinx()
    gmessages.plot(langts['ft_ratio'], lw=2, color='r')
    gmessages.tick_params(axis='y', colors='r', color='r', labelcolor='r')
    gmessages.set_ylabel('$R_{messages}^{(c)}$', color='r')
    gmessages.spines['right'].set_color('r')

    langts['rel_ft_fresh'] = langts['ft_fresh'] / langts['ft_count']
    langts['rel_ft_retweets'] = langts['ft_retweets'] / langts['ft_count']

    messages.plot(
        langts['rel_ft_fresh'],
        label=r'Organic messages',
        ls=':',
        marker='o',
        markevery=4,
        color='C0',
        lw=2
    )

    messages.plot(
        langts['rel_ft_retweets'],
        label=r'Retweeted messages',
        ls='--',
        marker='s',
        markevery=4,
        color='C1',
        lw=2
    )
    # messages.plot(langts['ft_speakers'], label='Users', ls=':', marker='^', lw=2, markevery=3, color='hotpink')

    messages.set_ylabel('Relative rate of usage (monthly average)')
    messages.legend(ncol=2, frameon=False, loc='upper center', fontsize=16, bbox_to_anchor=(.45, 1.0))
    messages.grid(True, which="both", alpha=.1, lw=1, linestyle='--')
    messages.set_xlim('2009', '2020')
    messages.xaxis.set_major_locator(mdates.YearLocator())
    messages.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
    messages.xaxis.set_minor_locator(mdates.MonthLocator([4, 7, 10]))

    try:
        idx = np.argwhere((langts['ft_retweets'] - langts['ft_fresh']) > 0).flatten()
        if len(idx) > 0:
            messages.axvspan(
                langts['ft_retweets'][idx].index[0],
                langts['ft_retweets'][idx].index[-1],
                alpha=0.25,
                color='grey'
            )
    except IndexError:
        pass

    inset = fig.add_axes([.175, .4, .25, .25])

    inset.plot(
        langts['ft_fresh'],
        label=r'Organic',
        ls=':',
        marker='o',
        markevery=12,
        color='C0',
        lw=2
    )

    inset.plot(
        langts['ft_retweets'],
        label=r'Retweets',
        ls='--',
        marker='s',
        markevery=12,
        color='C1',
        lw=2
    )

    inset.set_xlim('2009', '2019')
    inset.xaxis.set_major_locator(mdates.YearLocator(2))
    inset.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
    inset.grid(True, which="both", axis='both', zorder=0, alpha=.3, lw=1, linestyle='--')
    inset.set_ylabel('Number of messages')

    try:
        idx = np.argwhere((langts['ft_retweets'] - langts['ft_fresh']) > 0).flatten()
        if len(idx) > 0:
            inset.axvspan(
                langts['ft_retweets'][idx].index[0],
                langts['ft_retweets'][idx].index[-1],
                alpha=0.25,
                color='grey'
            )

    except IndexError:
        pass

    messages.annotate(
        "A", xy=(-.075, .99), color='k',
        xycoords="axes fraction", fontsize=22,
    )

    inset.annotate(
        "B", xy=(-.075, 1.1), color='k',
        xycoords="axes fraction", fontsize=22,
    )

    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_gain_timeseries(savepath, langts):
    """ Plot a time series of gain
    :param savepath: path to save generated plot
    :param langts: dataframe of the ratio time series
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'font.size': 12,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })


    fig, messages = plt.subplots(figsize=(12, 8))

    langts['rel_ft_fresh'] = langts['ft_fresh'] / langts['ft_count']
    langts['rel_ft_retweets'] = langts['ft_retweets'] / langts['ft_count']

    messages.plot(
        langts['rel_ft_fresh'],
        label=r'Organic messages',
        ls=':',
        marker='o',
        markevery=4,
        color='C0',
        lw=2
    )

    messages.plot(
        langts['rel_ft_retweets'],
        label=r'Retweeted messages',
        ls='--', marker='s',
        markevery=4,
        color='C1',
        lw=2
    )
    # messages.plot(langts['ft_speakers'], label='Users', ls=':', marker='^', lw=2, markevery=3, color='hotpink')

    gmessages = messages.twinx()
    gmessages.plot(langts['ft_gain'], lw=2, color='r')
    gmessages.tick_params(axis='y', colors='r', color='r', labelcolor='r')
    gmessages.set_ylabel('$G_{messages}^{(c)}$', color='r')
    gmessages.spines['right'].set_color('r')

    messages.set_ylabel('Relative rate of usage (monthly average)')
    messages.legend(ncol=2, frameon=False, loc='upper center', fontsize=16, bbox_to_anchor=(.45, 1.0))
    messages.grid(True, which="both", alpha=.1, lw=1, linestyle='--')
    messages.set_xlim('2009', '2020')
    messages.xaxis.set_major_locator(mdates.YearLocator())
    messages.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
    messages.xaxis.set_minor_locator(mdates.MonthLocator([4, 7, 10]))

    try:
        idx = np.argwhere((langts['ft_retweets'] - langts['ft_fresh']) > 0).flatten()
        if len(idx) > 0:
            messages.axvspan(
                langts['ft_retweets'][idx].index[0],
                langts['ft_retweets'][idx].index[-1],
                alpha=0.25,
                color='grey'
            )
            for d in langts['ft_retweets'][idx].index:
                messages.axvline(d, color='grey', alpha=0.25)

    except IndexError:
        pass


    inset = fig.add_axes([.175, .45, .3, .2])

    inset.plot(langts['ft_fresh'], label=r'Organic', ls=':', marker='o', markevery=12, color='C0', lw=2)
    inset.plot(langts['ft_retweets'], label=r'Retweets', ls='--', marker='s', markevery=12, color='C1', lw=2)

    inset.set_xlim('2009', '2019')
    inset.xaxis.set_major_locator(mdates.YearLocator(2))
    inset.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
    inset.grid(True, which="both", axis='both', zorder=0, alpha=.3, lw=1, linestyle='--')
    inset.set_ylabel('Number of messages')

    try:
        idx = np.argwhere((langts['ft_retweets'] - langts['ft_fresh']) > 0).flatten()
        if len(idx) > 0:
            inset.axvspan(
                langts['ft_retweets'][idx].index[0],
                langts['ft_retweets'][idx].index[-1],
                alpha=0.25,
                color='grey'
            )
            for d in langts['ft_retweets'][idx].index:
                inset.axvline(d, color='grey', alpha=0.25)

    except IndexError:
        pass

    messages.annotate(
        "A", xy=(-.075, .99), color='k',
        xycoords="axes fraction", fontsize=22,
    )

    inset.annotate(
        "B", xy=(-.075, 1.1), color='k',
        xycoords="axes fraction", fontsize=22,
    )

    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_gain_heatmap(savepath, heatmap):
    """ Plot top-n ranked languages over time
    :param savepath: path to save plot
    :param heatmap: a dataframe of languages to use for the ratio heatmap
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })

    fig, heatmapax = plt.subplots(figsize=(12, 10))

    vmin = 0
    vmax = 10
    bounds = np.linspace(vmin, vmax, 11)
    cmap = plt.cm.get_cmap('magma_r')
    cmaplist = [cmap(i) for i in range(cmap.N)]
    cmaplist[0] = (1, 1, 1, 1.0)  # force the first color entry to be white
    cmap = mcolors.LinearSegmentedColormap.from_list(None, cmaplist, cmap.N)
    norm = mcolors.BoundaryNorm(bounds, cmap.N)

    cbarax = inset_axes(
        heatmapax,
        width="2%",
        height="100%",
        bbox_to_anchor=(.05, -.001, 1, 1),
        bbox_transform=heatmapax.transAxes,
        borderpad=.25,
    )
    heatmapax = sns.heatmap(
        heatmap,
        ax=heatmapax,
        cbar_ax=cbarax,
        cbar=True,
        cmap=cmap,
        norm=norm,
        vmin=vmin,
        vmax=vmax,
        linewidths=0.0001,
        linecolor='grey'
    )
    cb = colorbar.ColorbarBase(
        cbarax,
        cmap=cmap,
        norm=norm,
        spacing='proportional',
        ticks=bounds,
        boundaries=bounds,
        format=r'$\leq$%.1f',
        extend='min',
    )

    cbarax.tick_params(labelsize=12)
    cbarax.set_xlabel(r'$G_{messages}^{(c)}$', fontsize=16, ha='left', labelpad=10, x=-.3)
    cbarax.yaxis.set_label_position("right")
    cbarax.yaxis.set_ticks_position('right')

    heatmapax.set_xticklabels(heatmap.columns, rotation=60, ha='right', rotation_mode='anchor')
    heatmapax.tick_params(axis='x', which='major', pad=5)
    heatmapax.set_ylabel('')
    heatmapax.grid(False, which="both")

    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_ratio_heatmap(savepath, heatmap):
    """ Plot top-n ranked languages over time
    :param savepath: path to save plot
    :param heatmap: a dataframe of languages to use for the ratio heatmap
    :return: saves a figure to {savepath}
    """
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif',
    })

    print(heatmap.sort_values('2019').head(10).round(2).to_latex())
    print(heatmap.sort_values('2019').tail(10).round(2).to_latex())

    fig, heatmapax = plt.subplots(figsize=(6, 10))

    vmin = 0
    vmax = 8
    bounds = [vmin, vmin+.5]
    bounds.extend(np.arange(vmin+1, vmax+1))
    cmap = plt.cm.get_cmap('magma_r')
    cmaplist = [cmap(i) for i in range(cmap.N)]
    cmaplist[0] = (1, 1, 1, 1.0)  # force the first color entry to be white
    cmap = mcolors.LinearSegmentedColormap.from_list(None, cmaplist, cmap.N)
    norm = mcolors.BoundaryNorm(bounds, cmap.N)

    cbarax = inset_axes(
        heatmapax,
        width="5%",
        height="100%",
        bbox_to_anchor=(.1, -.001, 1, 1),
        bbox_transform=heatmapax.transAxes,
        borderpad=.25,
    )

    heatmapax = sns.heatmap(
        heatmap,
        ax=heatmapax,
        cbar_ax=cbarax,
        cbar=True,
        cmap=cmap,
        norm=norm,
        vmin=vmin,
        vmax=vmax,
        linewidths=0.0001,
        linecolor='grey',
    )

    cb = colorbar.ColorbarBase(
        cbarax,
        cmap=cmap,
        norm=norm,
        spacing='proportional',
        ticks=bounds,
        boundaries=bounds,
        format=r'$\leq$%.1f',
        extend='min',
    )

    cbarax.tick_params(labelsize=12)
    cbarax.set_yticks(range(0, 12, 2))
    cbarax.set_xlabel(r'$R_{messages}^{(c)}$', fontsize=14, ha='left', labelpad=10, x=-.1)
    cbarax.yaxis.set_label_position("right")
    cbarax.yaxis.set_ticks_position('right')

    heatmapax.set_xticklabels(heatmap.columns, rotation=30, ha='center', rotation_mode='anchor')
    heatmapax.tick_params(axis='x', which='major', pad=10)
    heatmapax.set_ylabel('')
    heatmapax.grid(False, which="both")

    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


