#!/usr/bin/env bash

cwd=$(pwd)
main="$cwd/storywrangler"
ngrams="$main/ngrams"
languages="$main/languages"
plots="$main/plots"
datacache="$main/datacache"
logs="$main/logs"
models="$main/models"
mconda="$main/env.sh"
opt="$main/opt"
tlid="$opt/tlid"
utils="$opt/tweet_utils"
fasttext="$opt/fasttext"
threshold=.25
model="$tlid/resources/lid.176.bin"
eparser="$tlid/resources/emojis.bin"
nparser="$tlid/resources/ngrams.bin"
langs="$tlid/resources/iso-codes.csv"
codes="$tlid/resources/language_codes.json"
hashtbl="$tlid/resources/language_hashtbl.json"
config="$tlid/config.json"
reqs="$tlid/requirements.yml"

echo "Installing StoryWrangler ..."
mv -f storywrangler "$cwd/tmp"
mkdir "$main"
mkdir "$opt"
mkdir "$ngrams"
mkdir "$plots"
mkdir "$datacache"
mkdir "$logs"
mkdir "$models"
mv -f "$cwd/tmp" "$tlid"

echo -e "{
    \"twitterlid\":\""$tlid"\",
    \"fasttext-version\":\"v0.9.1\",
    \"fasttext\":\""$fasttext"\",
    \"tweet_utils\":\""$utils"\",
    \"emoji_parser\":\""$eparser\"",
    \"ngrams_parser\":\""$nparser\"",
    \"model\":\""$model\"",
    \"supported_languages\":\""$langs\"",
    \"language_hashtbl\":\""$hashtbl\"",
    \"language_codes\":\""$codes\"",
    \"model_threshold\":\""$threshold\"",
    \"ngrams\":\""$ngrams"\",
    \"languages\":\""$languages"\",
    \"plots\":\""$plots"\",
    \"datacache\":\""$datacache"\",
    \"logs\":\""$logs"\",
    \"models\":\""$models"\",
}" > "$config"


if [[ $"conda --version" == "conda*" ]]; then
    env="$opt/env"
    echo "Setup Miniconda3..."
    curl https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -o "$mconda"
    bash $mconda -u -b -p "$env"
    rm -f "$mconda"
    source ~/.bashrc
fi

echo "Creating new conda env. (storywrangler)..."
conda env create -q --file "$reqs"

echo "Installing VACC Utils..."
git clone -b 'v0.4.0' https://gitlab.com/compstorylab/tweet_utils.git "$utils"

echo "Installing FastText..."
git clone -b 'v0.9.1' https://github.com/facebookresearch/fastText.git "$fasttext"

env="$(conda env list | grep storywrangler | xargs echo | cut -d ' ' -f 2)"
env="$env/bin/python"
cd $fasttext
"$env" setup.py -q install

echo -e "Successfully installed storywrangler.\n\n"
echo -e "Run the following commands to get started:"
echo -e "source activate storywrangler"

